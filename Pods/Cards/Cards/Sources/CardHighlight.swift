//
//  CardHighlight.swift
//  Cards
//
//  Created by Paolo on 07/10/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

@IBDesignable open class CardHighlight: Card {

    /**
     Text of the title label.
     */
   
    private var lightColor = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1)
   
    
    // View Life Cycle
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override  func initialize() {
        super.initialize()
    }
    
  
    override open func draw(_ rect: CGRect) {
        super.draw(rect)
        
    }
   
    //Actions
    
    @objc  func buttonTapped(){
        UIView.animate(withDuration: 0.2, animations: {
//            self.actionBtn.transform = CGAffineTransform(scaleX: 0.90, y: 0.90)
        }) { _ in
            UIView.animate(withDuration: 0.1, animations: {
//                self.actionBtn.transform = CGAffineTransform.identity
            })
        }
        delegate?.cardHighlightDidTapButton?(card: self, button: UIButton()) 
    }
}



