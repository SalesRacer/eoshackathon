
import WatchKit

class TokensController: WKInterfaceController {
    
    var loaded = [String]()
    
    override func willActivate() {
        let count = WatchManager.℠.symbols?.count ?? 0
        table.setNumberOfRows(count, withRowType: "TokensRowController")
        for i in 0 ..< count {
            let row = self.table.rowController(at: i) as! TokensRowController
            let keys = WatchManager.℠.symbols!.keys
            let key = keys.sorted()[i]
            if let item = WatchManager.℠.symbols?[key] { 
                let sym = key
                row.token = sym
                row.symbol.setText(sym)
                if let symbols = WatchManager.℠.symbols?[sym] {
                    row.tokenName.setText(symbols["name"] as? String)
                    if let link = symbols["image"] as? Data {
                        row.image.setImage(UIImage.init(data: link))
                    }
                }
            }
        }
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        let row = self.table.rowController(at: rowIndex) as! TokensRowController 
        updateUserActivity("com.coinswipe.openToken", userInfo: ["symbol" : row.token!], webpageURL: nil)

    }
    
    @IBOutlet var table: WKInterfaceTable!
    
}

class TokensRowController: NSObject {
    var token:String!
    @IBOutlet var symbol: WKInterfaceLabel!
    @IBOutlet var image: WKInterfaceImage!
    @IBOutlet var tokenName: WKInterfaceLabel!
    @IBOutlet var points: WKInterfaceLabel!
    @IBOutlet var date: WKInterfaceLabel!
}

