
import Foundation
import WatchConnectivity
//import Alamofire

final class WatchManager: NSObject, WCSessionDelegate {
    static let ℠ = WatchManager()
    
    var playSession: String?
    var history: [[String : Any]]?
    var symbols: [String : [String : Any]]?
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        let dict = WCSession.default.receivedApplicationContext
        save(dict)
    }
    
    func save(_ message: [String : Any]) {
        if let dict = message["PointsHistory"] {
            history = dict as? [[String : Any]]
        }
        if let ps = message["playsession"] {
            self.playSession = ps as? String
        }
        if let ps = message["Symbols"] {
            self.symbols = ps as? [String : [String : Any]]
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
    }
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        let message = applicationContext
        save(message)
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        save(message)
    }
    
    enum JSONError: String, Error {
        case NoData = "ERROR: no data"
        case ConversionFailed = "ERROR: conversion from JSON failed"
    }
    
//    func jsonParser(urlPath:String) {
//        guard let endpoint = URL(string: urlPath) else {
//            print("Error creating endpoint")
//            return
//        }
//        URLSession.shared.dataTask(with: endpoint) { (data, response, error) in
//            do {
//                guard let data = data else {
//                    throw JSONError.NoData
//                }
//                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
//                    throw JSONError.ConversionFailed
//                }
//                print(json)
//            } catch let error as JSONError {
//                print(error.rawValue)
//            } catch let error as NSError {
//                print(error.debugDescription)
//            }
//            }.resume()
//    }
    
//    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
//        URLSession.shared.dataTask(with: url) { data, response, error in
//            completion(data, response, error)
//            }.resume()
//    }
//    
//    func downloadImage(url: URL) {
//        print("Download Started")
//        getDataFromUrl(url: url) { data, response, error in
//            guard let data = data, error == nil else { return }
//            print(response?.suggestedFilename ?? url.lastPathComponent)
//            print("Download Finished")
//            DispatchQueue.main.async() {
////                self.imageView.image = UIImage(data: data)
//            }
//        }
//    }
    
}
