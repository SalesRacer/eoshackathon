

import WatchKit
import UIKit

class HistoryController: WKInterfaceController {
    
    var loaded = [String]()
    
    override func willActivate() {
        let count = WatchManager.℠.history?.count ?? 0
        table.setNumberOfRows(count, withRowType: "HistoryRowController")
        for i in 0 ..< count {
            let row = self.table.rowController(at: i) as! HistoryRowController 
            let item = WatchManager.℠.history![i]
            if var time = item["date"] as? Double {
                time = time / 1000
                let date = Date(timeIntervalSince1970: time)
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
                dateFormatter.locale = NSLocale.current
                dateFormatter.dateFormat = "MM/dd/YY" //Specify your format that you want
                row.date.setText(dateFormatter.string(from: date))
            }
            if let sym = item["symbol"] as? String {
                row.token = sym 
                row.symbol.setText(sym)
                row.points.setText("\(item["points"] as! Int) pts")
                if let symbols = WatchManager.℠.symbols?[sym] {
                    row.tokenName.setText(symbols["name"] as? String)
                    if let link = symbols["image"] as? Data {
                        row.image.setImage(UIImage.init(data: link))
                    }
                }
            }
        }
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        let row = self.table.rowController(at: rowIndex) as! HistoryRowController
        updateUserActivity("com.coinswipe.openToken", userInfo: ["symbol" : row.token!], webpageURL: nil)
        
    }
    
    @IBOutlet var table: WKInterfaceTable!
    
}


class HistoryRowController: NSObject {
    var token:String!
    @IBOutlet var symbol: WKInterfaceLabel!
    @IBOutlet var image: WKInterfaceImage!
    @IBOutlet var tokenName: WKInterfaceLabel!
    @IBOutlet var points: WKInterfaceLabel!
    @IBOutlet var date: WKInterfaceLabel!
}
