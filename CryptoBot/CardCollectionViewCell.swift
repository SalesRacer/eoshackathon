
import UIKit
import expanding_collection
import Mixpanel

class CardCollectionViewCell: BasePageCollectionCell {
    
    var currentICO:ICO?
    var indexRow:Int?
    @IBOutlet weak var frontView: OneCardView!
    
    @IBAction func pinsSave(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reportLink), object:(sender as! UIButton).tag) 
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.frontView.parentView = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.additionalLoaded), name: NSNotification.Name(rawValue: NotificationName.additionalLoaded), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.closeCell), name: NSNotification.Name(rawValue: NotificationName.closeFakeViews), object: nil)
    }
    
    @objc func additionalLoaded(noti:NSNotification) {
        if let ico = noti.object as? ICO {
            if ico == self.currentICO {
                self.fill()
            }
        }
    } 
    
    func fill() {
//        if let pin = currentICO?.categories {
//            pins.text = pin
//            pins.alpha = 1
//        } else {
//            industry.text = "Suggest the tags to get the points"
//            industry.alpha = 0.5
//        }
//        if let naics = currentICO?.naics, let indust = currentICO?.industry {
//            industry.text = naics + ", " + indust
//            industry.alpha = 1
//        } else {
//            industry.text = "Suggest the industry info to get the points"
//            industry.alpha = 0.5
//        }
//        if let sto = currentICO?.story {
//            story.text = sto
//            story.alpha = 1
//        } else {
//            story.text = "Suggest the short tagline to get the points"
//            story.alpha = 0.5
//        }
//        if let sym = currentICO?.symbol, let cute = DataManager.℠.code(for: sym) {
//            cutip.text = cute
//        } else {
//            cutip.text = " - "
//        }
    }
    
    @objc func closeCell() {
        cellIsOpen(false)
    }
    
    @objc func cellTouched() {
        cellIsOpen(false) 
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.switchScroll), object: nil)
        
        if let view = self.frontContainerView as? OneCardView, let text = view.codeName.text {
            Mixpanel.mainInstance().track(event: "openTicker:\(text)")
        }
    }
}



