 

import UIKit
import FBSDKCoreKit 
import TwitterKit
import UserNotifications
import Branch
import Mixpanel
import Bugsee
import RealmSwift
import Intercom
import WatchConnectivity

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, NSUserActivityDelegate {    
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after applicq   ation launch.r
        
        let config = Realm.Configuration(
            schemaVersion: 42,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 1) {
                    
                }  
        })
        UIApplication.shared.isStatusBarHidden = true
        Intercom.setApiKey("ios_sdk-ce63443ea796f7f921bab18576be0aaa8647436f", forAppId:"sirctjn8")
        //Register an unidentified user with Intercom
        Intercom.registerUnidentifiedUser()
        Realm.Configuration.defaultConfiguration = config
        let realm = try! Realm() 
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        if let pack = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable : Any] {
            createBranchPack(pack: pack)
        }
        UIApplication.shared.applicationIconBadgeNumber = 0
        AppManager.℠
        //        Branch.setUseTestBranchKey(true)
        //        Branch.getInstance().setDebug()
        TWTRTwitter.sharedInstance().start(withConsumerKey:
            //"mOc8ap2I1YFxADtNsdsk6BkT9"
            "jaETqMXwZALraPxK6xOokRNku"
            , consumerSecret:
            "A8YLuAQpcqyTrk4Fm3nzQOeSP20tHyXamYcvXb13oFXkLxZaqZ"
            //"PEp8HG4eVbvHnvB3bhKUoJTb3G3meWWCADiJpJpuuDLbt5xW4a"
        )
        
        AppManager.℠.loginIntercom()                        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString ?? "deviceId"                
        AppManager.℠.deviceId = deviceID
        if nil == AppManager.℠.deviceToken { 
            AppManager.℠.deviceToken = "82fe22ff3a6f47de76e13be66aa4a085aed3429b634a7d2b6a6d61eeab981337"
        }
        Branch.getInstance().setIdentity("cs" + deviceID)
        Mixpanel.initialize(token: "3681d4877942c6b5ca822410106e237f")
        let branch: Branch = Branch.getInstance()
        Branch.getInstance().setRequestMetadataKey("$mixpanel_distinct_id", value:  NSString.init(string:Mixpanel.mainInstance().distinctId))
        branch.initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler: {params, error in
            // If the key 'pictureId' is present in the deep link dictionary
            //            if error == nil && params["+clicked_branch_link"] != nil && params["pictureId"] != nil {
            //                print("clicked picture link!")
            //                // load the view to show the picture
            //            } else {
            //                // load your normal view
            //            } 
        })
        //        Twitter.sharedInstance().sessionStore.saveSession(withAuthToken: //"258659657-F8aUFDdpTfySoCVDrInGOkMr4uaajwXSaPMxJJRv"
        //            "919308301589889024-QV2vTQbOTpdEs7OrKM4AN8AKkUuOquF"
        //        , authTokenSecret:
        //         "Hlbwl29qngvY790ybYrCyXBRr2aaEcrguiYgLEdZ1JXNw"
        //           // "fR13RvgUTkndhLECaFfyQP492OQjhKn2QqZBAMo8V2OT8"
        //        ) { (session, error) in }
        
        Bugsee.launch(token :"3563b9b2-4131-4135-92f8-a7f2f24ac708")
//        let linkedinHelper = LinkedinSwiftHelper(configuration:
//            LinkedinSwiftConfiguration(
//                clientId: "86dszi1yv5cvsp",
//                clientSecret: "da7AnW1si7k4VYNi",
//                state: "DLKDJF45DIWOERCM",
//                permissions: ["r_basicprofile", "r_emailaddress"],
//                redirectUrl: nil
//            )
//        )
        if WCSession.isSupported() {
            let session = WCSession.default 
            session.delegate = AppManager.℠
            session.activate()
        } 
        registerForPushNotifications()
        Mixpanel.mainInstance().track(event: "startedSession")
        return true
    }
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        // pass the url to the handle deep link call
        Branch.getInstance().continue(userActivity)
        if (userActivity.activityType == kOpenTokenActivity) {
            if let userInfo = userActivity.userInfo as? [String : AnyObject] {
                if let identifier = userInfo["symbol"] as? String {
                    AppManager.℠.scrollToToken = identifier
                    return true
                }
            }
        }
        return false
    }
    
    func application(_ application: UIApplication, willContinueUserActivityWithType userActivityType: String) -> Bool {
        if (userActivityType == kOpenTokenActivity) {
            return true
        }
        return false
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        NetworkManager.℠.updatePoints()
//        DataManager.℠.saveRealm(withDB: false)
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.copyClip), object: nil)
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        UserDefaults.standard.set(DataManager.℠.currentRating, forKey: "savedPoints")
        NetworkManager.℠.updatePoints() 
        //        DataManager.℠.saveRealm(withDB: false)
        Branch.getInstance().logout()
        UserDefaults.standard.removeObject(forKey: "loginnedOnce")
    }
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let branchHandled = Branch.getInstance().application(application,
                                                             open: url,
                                                             sourceApplication: sourceApplication,
                                                             annotation: annotation
        )
        if (!branchHandled) {
            // If not handled by Branch, do other deep link routing for the Facebook SDK, Pinterest SDK, etc
        }
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        // Add any custom logic here.
        return handled
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in 
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) { 
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        AppManager.℠.deviceToken = token // "82fe22ff3a6f47de76e13be66aa4a085aed3429b634a7d2b6a6d61eeab981337" //token    
        if NetworkManager.℠.playSession != nil {
            NetworkManager.℠.loadPoints()
        }
        Intercom.setDeviceToken(deviceToken)
        print("Device Token: \(token)")
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        createBranchPack(pack: userInfo)
    }
    
    func createBranchPack(pack:[AnyHashable : Any]) { 
        guard let aps = pack["aps"] as? [AnyHashable : Any], let badge = aps["badge"] as? Int, let alert = aps["alert"] as! [AnyHashable : Any]? else { return }
        guard let ticker = pack["ticker"] as? String else { return }
        guard let index = pack["index"] as? String else { return }
//        guard let updateId = pack["updateId"] as? String else { return }
        guard let updateType = pack["updateType"] as? String else { return }
        guard let selectedAction = pack["selectedAction"] as? String else { return }
        guard let value = pack["value"] as? String else { return }
        guard let selectedPoints = pack["selectedPoints"] as? String else { return }
        
        var newPending = Array<Dictionary<String, Any>>()
        if var pending = DataManager.℠.getAllPendingUpdates() {
            for dict in pending {
                var skip = false
                if let type = dict["updateType"] as? Int {
                    if dict["symbol"] as? String == ticker {
                        if String(type) == updateType {
                            skip = true
                        }
                    }
                }
                if !skip {
                    newPending.append(dict)
                }
            }
        }
        UserDefaults.standard.set(newPending, forKey: "pendingUpdates")
        
        
        if let mess = alert["body"] as? String {
             let points = selectedPoints
//            let reward = pack["reward"] as? String,
            var coin: ICO?
            if badge == 2 {
//                let mess2 = mess.substring(from: mess.range(of: "ICO ")!.upperBound)
//                let mess3 = mess2.substring(to: mess2.range(of: " was")!.lowerBound)
                for ico in DataManager.℠.dataBase {
                    if ico.symbol == ticker {
                        coin = ico
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.scrollToStack), object: DataManager.℠.dataBase.index(of: ico))
                    }
                }
                if coin == nil {
                    for ico in DataManager.℠.upcomingICO {
                        if ico.symbol == ticker {
                            coin = ico
                        }
                    }
                }
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.addCongrats), object: ("You just recieved tokens on your account for your valuable and important contribution. We appreciate your support, alongside with the whole Coinswipe community - thank you!", PointsBag.init(points: Int(points)!, type: 2, symbol: ticker, card: index)))
                
                let buo = BranchUniversalObject(canonicalIdentifier: "coin")
                //                    buo.addMetadataKey("ticker", value: UserDefaults.standard.string(forKey: "lastUpdatedTicker")!)
                
                let lp: BranchLinkProperties = BranchLinkProperties()
                lp.channel = "facebook"
                lp.feature = "sharing"
                
                buo.getShortUrl(with: lp) { (url, error) in
                    TWTRTwitter.sharedInstance().sessionStore.saveSession(withAuthToken: //"258659657-F8aUFDdpTfySoCVDrInGOkMr4uaajwXSaPMxJJRv"
                        "919308301589889024-9GJ0pLmzC2csr0E4IGZkxkBF0IraMBM"
                        , authTokenSecret:
                        "d3fl3lH18aTTI97121uF8u4d1qDbzqRnKeTmHkldhqUQ7"
                        // "fR13RvgUTkndhLECaFfyQP492OQjhKn2QqZBAMo8V2OT8"
                    ) { (session, error) in
                        if let userID = TWTRTwitter.sharedInstance().sessionStore.session()?.userID {
                            let client = TWTRAPIClient(userID: userID)
                            if let ready = coin {
                                client.sendTweet(withText: "New Coinswipe crowdsourced data available for \(ready.name) (\(ready.symbol)): " + url!)  { (tweet, error) in
                                    let store = TWTRTwitter.sharedInstance().sessionStore
                                    if let userID = store.session()?.userID {
                                        store.logOutUserID(userID)
                                    }
                                }
                            }
                            else {
                                client.sendTweet(withText: "New Coinswipe crowdsourced data available: " + url!) { (tweet, error) in }
                            }
                        }
                    }
                }
            }
            else if badge == 3 {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.addCongrats), object: ("The update was rejected. We appreciate your support, alongside with the whole Coinswipe community - thank you!", PointsBag.init(points: Int(points)!, type: 2, symbol: ticker, card: index)))
            }
        }
    }
    
}
