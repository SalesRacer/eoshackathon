

import UIKit
import LinearProgressBar
import FBSDKCoreKit
import FacebookLogin
import Alamofire
import PopupDialog

class ProfileView: UIViewController, UITextViewDelegate, LoginButtonDelegate {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var pending: UIButton!
    @IBOutlet weak var pendingHeight: NSLayoutConstraint!
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var currentRank: UILabel!
    @IBOutlet weak var nextRank: UILabel!
    @IBOutlet weak var pointsTo: UILabel!
    @IBOutlet weak var linkedInField: VerticallyCenteredTextView!
    @IBAction func sendLinkedIn(_ sender: Any) {
        tryToSaveLinkedIn()
    }
    @IBOutlet weak var toshiField: VerticallyCenteredTextView!
    @IBAction func sendToshi(_ sender: Any) {
        if let one = self.toshiField.text {
            NetworkManager.℠.sendToshi(toshi:one) { (data) in
                
            }
            UserDefaults.standard.set(one, forKey: "toshiaddress")
            AppManager.℠.mainVC?.showMessage("The Toshi address was saved", style: .success)
        }
    }
    
    @IBOutlet weak var porints: UILabel!
    @IBOutlet weak var progress: LinearProgressBar!
    @IBOutlet weak var updatesString: UILabel!
    @IBOutlet weak var updatesTable: UITableView! {
        didSet {
            self.updatesTable.register(UINib.init(nibName: "PaperCell", bundle: nil), forCellReuseIdentifier:"PaperCell")
        }
    }
    var paperControl:PaperController!
    
    @IBAction func openWallet(_ sender: Any) {
        guard let url = URL(string: "http://jungle.cryptolions.io/#accountInfo") else { return }
        UIApplication.shared.open(url)
//        NetworkManager.℠.openToshi()
    }
    
    @IBOutlet weak var loginPage: UIView!
    
    @IBOutlet weak var wrapping: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.porints.text = "\(DataManager.℠.currentRating) pts."
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.addFBName), name: NSNotification.Name(rawValue: NotificationName.updateAfterLogin), object: nil)
        
        if UserDefaults.standard.object(forKey: "loginned") == nil {
            self.loginPage.alpha = 1
        }
        
        if let word = UserDefaults.standard.string(forKey: "linkedinaddress"), word != "", word != " " {
            linkedInField.text = word
        }
        if let word = UserDefaults.standard.string(forKey: "toshiaddress"), word != "", word != " " {
            toshiField.text = word
        }
        
        addFBName()
        self.personLogo.layer.cornerRadius = self.personLogo.frame.width / 2
        self.personLogo.layer.masksToBounds = true
        self.personLogo.layer.borderColor = UIColor.darkGray.cgColor
        self.personLogo.layer.borderWidth = 2
        evaluateRanks()
        
        SEKeystoreService.sharedInstance.importAccount(privateKey: "5Kgb6NcZErENZGZNFPw77V3r23bi367RyUHWYvUvNihVEH7CcD6", passcode: "fakenewsmint", succeed: { (acc) in
            
            acc.getEosBalance(account: "fakenewsmint", succeed: { (num) in
                self.toshiField.text = "\(num) TNT"
            }, failed: { (err) in
                
            })
        }) { (err) in 
            
        }
    }
                                                                                       
    func evaluateRanks() {
        let currentPoints = DataManager.℠.currentRating
        var rankBefore = DataManager.℠.ranks[0]
        var rankAfter = DataManager.℠.ranks[1]
        for rank in DataManager.℠.ranks {
            if currentPoints < rank.points {
                rankAfter = rank
                break
            } else {
                rankBefore = rank
            }
        }
        let diff = rankAfter.points - currentPoints
        pointsTo.text = "Points to next rank: \(diff)"
        let all = rankAfter.points - rankBefore.points
        self.progress.progressValue = CGFloat((all - diff)/all * 100)
        currentRank.text = rankBefore.name
        nextRank.text = rankAfter.name
    }
    
    @IBOutlet weak var personLogo: UIImageView!
    @IBOutlet weak var personCaption: UILabel!
    
    func tryToSaveLinkedIn() {
        if let one = self.linkedInField.text {
            if one.contains("/") {
                NetworkManager.℠.sendLinkedIn(linkedin: one) { (data) in
                    AppManager.℠.mainVC?.showMessage("The LinkedIn address was sent for review", style: .success)
                }
                UserDefaults.standard.set(one, forKey: "linkedinaddress")
            } else {
                AppManager.℠.mainVC?.showMessage("Please check if your LinkedIn address is correct", style: .info)
            }
        }
    }
    
    @objc func addFBName() {
        if AppManager.℠.isFacebookLogged() {
            let userId = FBSDKAccessToken.current().userID!
            
            Alamofire.request("http://graph.facebook.com/\(userId)/picture?type=large").responseImage { (response) in
                if let image = response.result.value {
                    self.personLogo.image = image
                }
            }
            if let name = UserDefaults.standard.object(forKey: "loginnedFBName") as? String {
                self.personCaption.text = name
            }
        } else {
            if let name = UserDefaults.standard.object(forKey: "loginned") as? String {
                self.personCaption.text = name
            }
        }
    }
    
    @IBAction func facebookOpen(_ sender: Any) {
        AppManager.℠.loginFacebook(vc: AppManager.℠.mainVC!) 
    }
    
    @IBAction func emailOpen(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.showEmailLogin), object:nil)
    }
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        
    }
    func loginButtonDidLogOut(_ loginButton: LoginButton) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.linkedInField.delegate = self
        toshiField.delegate = self
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing)))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        textView.text = ""
        textView.textColor = UIColor.darkText
        return true
    }

    @objc func endEditing() {
        view.endEditing(true)
    }
    
    @IBAction func toshiTutorial(_ sender: Any) {
        self.dismiss(animated: true) {
            AppManager.℠.startToshiTutorial()
        }
    }
}

extension ProfileView: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing()
        return true
    }
}
