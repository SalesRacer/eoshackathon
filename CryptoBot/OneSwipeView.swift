
import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import SafariServices
import Kingfisher
import SwiftyGif
import PopupDialog
import SwiftLinkPreview
import RSLoadingView

class OneSwipeView: UIView {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var codeName: UILabel!
    @IBOutlet weak var image: UIImageView!
    var currentICO: ICO?
    var currentType: CardType?
    var isFake = false
    var isBounty = false
    @IBOutlet weak var profilePlaceholder: UIView! 
    @IBOutlet weak var profileOne: UIImageView!
    @IBOutlet weak var profileTwo: UIImageView!
    @IBOutlet weak var profileNameOne: UILabel!
    @IBOutlet weak var profileNameTwo: UILabel!
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var contributePlaceholder: UIView!
    @IBOutlet weak var profileButtonOne: SwipeButton!
    @IBOutlet weak var profileButtonTwo: SwipeButton!
    @IBOutlet weak var addAspect: UIButton!
    @IBOutlet weak var tapViewOne: UIStackView! 
    @IBOutlet weak var tapViewTwo: UIStackView!
    @IBOutlet weak var gif: UIImageView!
    @IBOutlet weak var underView: UIView!
    
    @IBOutlet weak var checkAspectsAll: UIImageView!
    @IBOutlet weak var bountyIcon: UIImageView!
    @IBOutlet weak var likeView: UIView!
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var likeText: UILabel!

    @IBOutlet weak var expandImage: UIImageView!
    @IBOutlet weak var expandButton: UIButton! // !!! 
    
    @IBOutlet weak var richGradient: GradientView!
    @IBOutlet weak var checkmarkWidth: NSLayoutConstraint!
    @IBOutlet weak var typeTitle: UILabel!
    @IBOutlet weak var richBody: UITextView!
    @IBOutlet weak var richTitle: UILabel!
    @IBOutlet weak var richLink: UILabel!
    @IBOutlet weak var richImage: UIImageView!
    @IBOutlet weak var richDate: UILabel!
    @IBOutlet weak var richVideo: UIImageView!
    @IBOutlet weak var richText: UIView!
    var openLink:String?
    var images: Array<UIImage> = []
    var cardIndex:Int? 
    @IBOutlet weak var upcoming: UIView!
    @IBOutlet weak var upcomingText: UILabel!
    var below: BelowView?
    var cardServerIndex: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.botTouched(_:)))
        //        botImage.addGestureRecognizer(tap)
        
        profilePlaceholder.alpha = 0
        picture.alpha = 0
        //        cover.alpha = 1
        //        botImage.alpha = 1
        contributePlaceholder.alpha = 0
        addAspect.alpha = 0
        
        richImage.layer.cornerRadius = 7
        richGradient.layer.cornerRadius = 7
        
        likeImage.tintColor = UIColor.white
        likeView.layer.cornerRadius = 17
        upcoming.layer.cornerRadius = 5 
        
        let gest = UITapGestureRecognizer.init(target: self, action: #selector(profileOnePressed(_:))) 
        let gestTwo = UITapGestureRecognizer.init(target: self, action: #selector(profileTwoPressed(_:)))
        tapViewOne.addGestureRecognizer(gest)
        tapViewTwo.addGestureRecognizer(gestTwo)
        
        richBody.setContentOffset(CGPoint.zero, animated: false)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.fill), name: NSNotification.Name(rawValue: NotificationName.additionalLoaded), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.closeFakeView(_:)), name: NSNotification.Name(rawValue: NotificationName.closeFakeViews), object: nil)
    }
    
    @objc func openSafariLink() {
        if let link = openLink {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.openLink), object: link) 
        }
    } 
    
    @objc func fill() {
        DataManager.℠.performRealm {
            
            if let ico = currentICO, let mode = currentType {
                
                self.isFake = false
                let card = ico.cards[cardIndex!]
                cardServerIndex = card.index 
                if ico is UpcomingICO {
                    upcoming.alpha = 1
                    upcoming.backgroundColor = kBlueColor
                    upcomingText.text = "Upcoming"
                }
                
                if let fake = card.fake {
                    upcoming.alpha = 1
                    upcoming.backgroundColor = UIColor.red
                    upcomingText.text = "Fake"
                    self.isFake = true
                } else if DataManager.℠.checkFakeNews(ico: ico.symbol!, index: card.index ?? "whatistaht") {
                    upcoming.alpha = 1
                    upcoming.backgroundColor = UIColor.red
                    upcomingText.text = "Fake (In Review)"
                    self.isFake = true
                }
                 
                if mode == .white {
                    picture.alpha = 1
                    self.openLink = ico.whitePaperURL
                    let tap = UITapGestureRecognizer.init(target: self, action: #selector(openSafariLink))
                    self.addGestureRecognizer(tap)
                    if let cov = ico.coverLink {
                        self.picture.kf.setImage(with: URL(string:cov), options: NetworkManager.℠.imageCacheOptions(), completionHandler: {
                            (image, error, cacheType, imageUrl) in
                            if let im = image {
                                self.images.append(im)
                            }
                        })
                    }
                    typeTitle.text = "Whitepaper"
                    return
                    
                }
                
                if mode == .rich || mode == .news  {
                    let tap = UITapGestureRecognizer.init(target: self, action: #selector(openSafariLink))
                    self.richText.alpha = 1
                    self.richText.addGestureRecognizer(tap)
                    var loaded = false
                    if let newsCard = card as? NewsCard {
                        self.richTitle.text = newsCard.title
                        self.openLink = card.links.first
                        self.richBody.text = newsCard.description
                        self.richBody.setContentOffset(CGPoint.zero, animated: false)
                        self.richLink.text = newsCard.source
                        if self.openLink!.contains("youtube") || self.openLink!.contains("youtu.be") {
                            self.richVideo.alpha = 0.8
                        }
                        if let im = newsCard.date {
                            self.richDate.text = im
                        }
                        if let im = newsCard.imageUrl {
                            loaded = true
                            self.richImage.kf.setImage(with: URL(string:im), options: NetworkManager.℠.imageCacheOptions())
                        }
                    }
                    if !loaded, let link = card.links.first { 
                        NetworkManager.℠.linkPreview.preview(link,
                                                               onSuccess: { result in
                                                                UIView.animate(withDuration: 0.3, animations: {
//                                                                    RSLoadingView.hide(from: self)
//                                                                    self.richText.alpha = 1
                                                                    if let name = result[SwiftLinkResponseKey.title] as? String {
                                                                        self.richTitle.text = name
                                                                    }
                                                                    if let url = result[SwiftLinkResponseKey.finalUrl] as? URL {
                                                                        self.openLink = url.absoluteString
                                                                    }
                                                                    if let canon = result[SwiftLinkResponseKey.canonicalUrl] as? String {
                                                                        self.richLink.text = canon
                                                                    }
                                                                    if let description = result[SwiftLinkResponseKey.description] as? String {
                                                                        self.richBody.text = description
                                                                        self.richBody.setContentOffset(CGPoint.zero, animated: false)
                                                                    }
                                                                    if let image = result[SwiftLinkResponseKey.image] as? String {
                                                                        self.richImage.kf.setImage(with: URL(string:image), options: NetworkManager.℠.imageCacheOptions())
                                                                    }
                                                                    if self.openLink != nil {
                                                                        if self.openLink!.contains("youtube") || self.openLink!.contains("youtu.be") {
                                                                            self.richVideo.alpha = 0.8
                                                                        }
                                                                    }
                                                                    //                                                        self.richGradient.alpha = 1
                                                                })
                        },
                                                               onError: { error in print("\(error)")})
                    }
                    typeTitle.text = "News"
                    return
                    
                }
                
//                if cardIndex! < 5 {
//                    let nameTo = Aspect.names[cardIndex!]
//                    typeTitle.text = nameTo
//                }
                typeTitle.text = "News"
                
                var nameBounty:String? 
//                switch mode {
//                case .roadmap:
//                    nameBounty = "bountyTime"
//                case .terms:
                    nameBounty = "bountyTerms"
//                case .tech:
//                    nameBounty = "bountyTech"
//                case .market:
//                    nameBounty = "bountyTam"
//                case .profile:
//                    nameBounty = "bountyTeam"
//                default:
//                    break
//                }
                self.isBounty = mode == .terms
                
                if let nameB = nameBounty, self.isBounty {
                    let gifManager = SwiftyGifManager.defaultManager
                    let gf = UIImage(gifName: nameB + ".gif")
                    gif.setGifImage(gf, manager: gifManager)
                    NotificationCenter.default.addObserver(gif, selector: #selector(gif.startAnimatingGif), name: NSNotification.Name(rawValue: NotificationName.startGif), object: nil)
                    NotificationCenter.default.addObserver(gif, selector: #selector(gif.stopAnimatingGif), name: NSNotification.Name(rawValue: NotificationName.stopGif), object: nil)
                }
                 
                switch mode {
                case .profile:
                    dimDetails()
                    profilePlaceholder.alpha = 1
                    picture.alpha = 0
                    addAspect.alpha = 0.7
                    let all = card.links
                    if all.count > 0 {
                        let fullOne = all[0]
                        let comps = fullOne.components(separatedBy: "||")
                        var firstName = comps[0]
                        var firstLink = comps[1]
                        var firstImage:String? = nil
                        if comps.count > 2 {
                            firstImage = comps[2]
                        }
                            
                        profileNameOne.text = firstName
                        if let linkOne = firstImage, let url = URL(string:linkOne) {
                            self.profileOne.kf.setImage(with: url, options: NetworkManager.℠.imageCacheOptions(), completionHandler: {
                                (image, error, cacheType, imageUrl) in
                                
                                self.profileOne.layer.borderColor = UIColor.gray.cgColor
                                self.profileOne.layer.borderWidth = 1
                                self.profileOne.layer.cornerRadius = self.profileOne.frame.width / 2.0 
                            })
                        }
                        ico.profileOneLink = firstLink
                    }
                    if ico.symbol == "BTC" {
                        var firstLink = "https://www.linkedin.com/in/gavin-andresen-6987971/"
                        var firstImage = "https://media.licdn.com/dms/image/C5103AQESC0mM3mcgzQ/profile-displayphoto-shrink_800_800/0?e=1534982400&v=beta&t=zxKwAOkxkTmqm0EcjzLkl9iHIB9fIvMaXzd6QUybWK4"
                        var firstName = "Gavin Andresen, Chief Scientist"
                        profileNameOne.text = firstName
                        if let url = URL(string:firstImage) {
                            self.profileOne.kf.setImage(with: url, options: NetworkManager.℠.imageCacheOptions(), completionHandler: {
                                (image, error, cacheType, imageUrl) in
                                
                                self.profileOne.layer.borderColor = UIColor.gray.cgColor
                                self.profileOne.layer.borderWidth = 1
                                self.profileOne.layer.cornerRadius = self.profileOne.frame.width / 2.0
                            })
                        }
                        ico.profileOneLink = firstLink
                    }
                    if all.count > 1 {
                        let fullTwo = all[1]
                        let comps = fullTwo.components(separatedBy: "||")
                        var firstName = comps[0]
                        var firstLink = comps[1]
                        var firstImage:String? = nil 
                        if comps.count > 2 {
                            firstImage = comps[2]
                        }
                        profileNameTwo.text = firstName
                        if let linkOne = firstImage, let url = URL(string:linkOne) {
                            self.profileTwo.kf.setImage(with: url, options: NetworkManager.℠.imageCacheOptions(), completionHandler: {
                                (image, error, cacheType, imageUrl) in
                                
                                self.profileTwo.layer.borderColor = UIColor.gray.cgColor
                                self.profileTwo.layer.borderWidth = 1
                                self.profileTwo.layer.cornerRadius = self.profileTwo.frame.width / 2.0
                            })
                        }
                        ico.profileTwoLink = firstLink
                    }
                    if ico.symbol == "BTC" {
                        var firstLink = "https://www.linkedin.com/in/jeffgarzik/"
                        var firstImage = "https://media.licdn.com/dms/image/C5603AQHYyndHTEYUFQ/profile-displayphoto-shrink_800_800/0?e=1534982400&v=beta&t=tmLDLyx599jZIdk_lucrtnSVDu5yKaIjRXH0AxB8ttE"
                        var firstName = "Jeff Garzik, Co Founder at Bloq"
                        profileNameTwo.text = firstName
                        if let url = URL(string:firstImage) {
                            self.profileTwo.kf.setImage(with: url, options: NetworkManager.℠.imageCacheOptions(), completionHandler: {
                                (image, error, cacheType, imageUrl) in
                                
                                self.profileTwo.layer.borderColor = UIColor.gray.cgColor
                                self.profileTwo.layer.borderWidth = 1
                                self.profileTwo.layer.cornerRadius = self.profileTwo.frame.width / 2.0
                            })
                        }
                        ico.profileTwoLink = firstLink
                    }
                    if all.count == 0, ico.symbol != "BTC" {
                        contributePlaceholder.alpha = 1
                        addAspect.alpha = 0
                    }
                    
                default:
//                    profilePlaceholder.alpha = 0
//                    picture.alpha = 0
//                    contributePlaceholder.alpha = 1
//                    addAspect.alpha = 0.7
//                    let gifManager = SwiftyGifManager.defaultManager
//                    let gf = UIImage(gifName: "btc.gif")
//                    gif.setGifImage(gf, manager: gifManager)
//                    NotificationCenter.default.addObserver(gif, selector: #selector(gif.startAnimatingGif), name: NSNotification.Name(rawValue: NotificationName.startGif), object: nil)
//                    NotificationCenter.default.addObserver(gif, selector: #selector(gif.stopAnimatingGif), name: NSNotification.Name(rawValue: NotificationName.stopGif), object: nil)
//                    return 
                    profilePlaceholder.alpha = 0
                    picture.alpha = 1
                    addAspect.alpha = 0.7
                    var addresses = card.links
                    if addresses.count > 0 {
                        if addresses.count == 1 {
                            addresses = addresses[0].components(separatedBy: "\n")
                        }
                        let add = addresses[0]
                        if let url = URL(string:add) {
                            self.picture.kf.setImage(with: url, completionHandler: {
                                (image, error, cacheType, imageUrl) in
                                if let im = image {
                                    self.images.append(im)
                                }
                            }) 
                        }
                        if addresses.count > 1 {
                            for address in addresses {
                                if let url = URL(string:address) {
                                    KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                                        if let im = image {
                                            self.images.append(im)
                                        }
                                    })
                                }
                            }
                        }
                    } else if addresses.count == 0 || addresses[0] == "" { 
                        profilePlaceholder.alpha = 0
                        picture.alpha = 0
                        contributePlaceholder.alpha = 1
                        addAspect.alpha = 0
                    }
                    
                }
                var ratingOf = 0.0
                if let ci = card.index {
                    ratingOf = DataManager.℠.loadRating(for: ico, with: ci) ?? 0
                }
                checkAspectsAll.alpha = ratingOf != 0 ? 1 : 0
                                                                
                if card.index == nil || card.index == "fakeindex" {
                    expandImage.alpha = 0
                    expandButton.alpha = 0 
                } else {
                    expandImage.alpha = 1
                    expandButton.alpha = 1
                }
            }
            if contributePlaceholder.alpha > 0 {
                if cardIndex == 0 {
                    DataManager.℠.bountyOnScreen = true
                }
                let tap = UITapGestureRecognizer.init(target: self, action: #selector(reportLink(_:))) 
                contributePlaceholder.addGestureRecognizer(tap)
            } else {
                DataManager.℠.bountyOnScreen = false 
            }
        }
    }
    
    func dimDetails() {
        if AppManager.℠.currentMode == .oneCard { 
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.dimDetails), object: (self.contributePlaceholder.alpha > 0 || currentType == .white || currentType == .rich || currentType == .profile) ? 1 : 0)
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.dimDetails), object: 4)
        }
    }
    
    @IBAction func botTouched(_ sender: UIButton) { 
        
        //        UIApplication.shared.open(URL.init(string:"https://www.facebook.com/messages/t/1815861968677883")!, options: [:], completionHandler: nil)
    }
    
    @objc func reportLink(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reportLink), object:
            (sender is UITapGestureRecognizer) ?
            currentType?.rawValue
            : (sender as! UIButton).tag
        )
    }
    
    @IBAction func profileOnePressed(_ sender: Any) {
        if let ico = self.currentICO {
            if let link = ico.profileOneLink {
                openLink(link: link)
            }
        }
    }
    
    @IBAction func profileTwoPressed(_ sender: Any) {
        if let ico = self.currentICO {
            if let link = ico.profileTwoLink {
                openLink(link: link)
            }
        }
    }
    
    func openLink(link:String?) {
        if link != nil, link! != "" {
            let svc = SFSafariViewController(url: URL(string:link!)!)
            if let topController = UIApplication.topViewController() {
                topController.present(svc, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func openShev(_ sender: Any) {
        
        
//        let pars = ["updateType" : UpdateType.cardCons.rawValue, "actionType" : UpdateActionType.add.rawValue, "value" : "Predict price will decrease by 10% within 30 days of this artcile"] as [String : Any]
//        NetworkManager.℠.updateLink(ico: DataManager.℠.currentICO!.symbol!, pars: pars) { (data) in
//            
//        }
        DataManager.℠.nextStepTutorial(toStart: 5)
        self.clipsToBounds = false
        let frame = self.frame
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addFakeViews"), object:PagerMode.one)       
        if below == nil {
            stupidBringButtons(over: false)
            below = BelowView()
            below!.frame = frame
            below!.backgroundColor = UIColor.groupTableViewBackground
            below!.layer.cornerRadius = 12
            self.insertSubview(below!, at: 0)
            UIView.animate(withDuration: 0.2) {
                self.below!.frame = CGRect.init(x: -25, y: frame.height/2, width: frame.width + 50, height: frame.height)
                self.superview!.center = CGPoint.init(x: self.superview!.center.x, y: self.superview!.center.y - 90)
            }
        } else {
            self.clipsToBounds = true
            closeFakeView(nil)
        }
    }
    
    func stupidBringButtons(over:Bool) {
        let cardVC = AppManager.℠.mainVC
        if over {
            cardVC?.view.insertSubview((cardVC?.oneCardControls)!, aboveSubview: (cardVC?.oldContainerView)!)
            cardVC?.view.insertSubview((cardVC?.floaty)!, aboveSubview: (cardVC?.oldContainerView)!)
            cardVC?.view.insertSubview((cardVC?.favouriteButton)!, aboveSubview: (cardVC?.oldContainerView)!)
        } else {
            cardVC?.view.insertSubview((cardVC?.oneCardControls)!, belowSubview: (cardVC?.oldContainerView)!)
            cardVC?.view.insertSubview((cardVC?.floaty)!, belowSubview: (cardVC?.oldContainerView)!)
            cardVC?.view.insertSubview((cardVC?.favouriteButton)!, belowSubview: (cardVC?.oldContainerView)!)
        }
    }
    
    @objc func closeFakeView(_ noti:NSNotification?) {
        if below != nil {
            stupidBringButtons(over: true)
            if nil == noti?.object {
                UIView.animate(withDuration: 0.2, animations: {
                    self.superview!.center = CGPoint.init(x: self.superview!.center.x, y: self.superview!.center.y + 90)
                    self.below!.frame = CGRect.init(x: 0, y: 0, width: self.frame.width/2, height: self.frame.height/3)
                    self.below!.alpha = 0
                    
                }, completion: { (ok) in
                    self.below?.removeFromSuperview()
                    self.below = nil
                })
            } else {
                self.superview!.center = CGPoint.init(x: self.superview!.center.x, y: self.superview!.center.y + 90)
                self.below!.frame = CGRect.init(x: 0, y: 0, width: self.frame.width/2, height: self.frame.height/3)
                self.below!.alpha = 0
                self.below?.removeFromSuperview()
                self.below = nil
            }
        }
    }
    
    
    
}


class BelowView: UIView {
    
}


@IBDesignable
class GradientView: UIView {
    
    @IBInspectable var startColor:   UIColor = .black { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = .white { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}
    
    override class var layerClass: AnyClass { return CAGradientLayer.self }
    
    var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors    = [startColor.cgColor, endColor.cgColor]
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updatePoints()
        updateLocations()
        updateColors()
    }
}
