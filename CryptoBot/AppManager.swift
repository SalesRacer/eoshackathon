
import UIKit
import FBSDKCoreKit
import RealmSwift
import FacebookShare
import FacebookCore
import FacebookLogin
import Branch 
import Arcane
import Intercom
import WatchConnectivity
import PopupDialog

enum cardsMode { 
    case browse 
    case oneCard
    case favourites
}

final class AppManager: NSObject, WCSessionDelegate {
    
    var scrollToToken: String?
    
    var currentMode: cardsMode = .browse
    weak var mainVC: CardsViewController? 
    var deviceToken: String?
    var deviceId: String?
     
    var tutorialStarted = false
    var tutorialCounter: Int = -1 
    
    var belowViewCube: Bool = false 
    
    let userDefaults = UserDefaults.init(suiteName: "group.coinswipe.watch")
    
    static let ℠ = AppManager()
    private override init() {
        if UserDefaults.standard.object(forKey: "firstLaunch") == nil {
            UserDefaults.standard.set(Date(), forKey:"firstLaunch") 
        }
        if UserDefaults.standard.object(forKey: "tutorialS3hown") == nil { 
            UserDefaults.standard.set(Date(), forKey:"tutorialSh2own")
        }
        NetworkManager.℠.checkNetwork()
        DataManager.℠
        DataManager.℠.begin() 
    }
    
    func postToWatches(dict:[String:Any]) {
        do {
            try WCSession.default.updateApplicationContext(dict)
        } catch {
            
        }
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
    }
    
    func isFacebookLogged() -> Bool  {
        return FBSDKAccessToken.current() != nil  
    }
    
    func loginFacebook(vc:UIViewController) -> Bool {
        if !isFacebookLogged() {
            let loginManager = LoginManager()   
            loginManager.logIn(readPermissions: [.publicProfile], viewController: vc) { (loginResult) in
                switch loginResult {
                case .failed(let error):
                    print(error)
                case .cancelled:
                    print("User cancelled login.")
                case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                    print("Logged in!")
                    UserDefaults.standard.set(accessToken.userId!, forKey: "loginned")
                    
                    let req = FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields":"email,name"], httpMethod: "GET")
                    req?.start(completionHandler: { (connection, response, error) in
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.updateAfterLogin), object:nil)
                        if let result = response as? [String : String] {
                            UserDefaults.standard.set(result["name"], forKey: "loginnedFBName")
                        }
                        AppManager.℠.loginIntercom()
                        if NetworkManager.℠.playSession != nil {
                            NetworkManager.℠.loadPoints()
                        }
                    })
                }
            }
            return false
        }
        return true
    } 
    
    func startToshiTutorial() {
        self.popup(mode: 0) {
            self.popup(mode: 1) {
                self.popup(mode: 2) {
                    self.popup(mode: 3) {
                        self.popup(mode: 4) {
                            self.popup(mode: 5) {
                                self.popup(mode: 6) {
                                    self.popup(mode: 7) {
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func popup(mode:Int, action:PopupDialogButton.PopupDialogButtonAction?) {
        let ratingVC = ToshiTutorialView(nibName: "ToshiTutorialView", bundle: nil)
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .vertical, transitionStyle: .fadeIn, tapGestureDismissal: true)
        let cancel = CancelButton(title: "Close", dismissOnTap: true) { }
        let next = DefaultButton.init(title:"Next", action: action)
        switch mode {
        case 1:
            let open = DefaultButton(title: "Open Toshi") {
                NetworkManager.℠.openToshi()
            }
            popup.addButtons([open, next, cancel])
        case 4:
            let open = DefaultButton(title: "Open Toshi") {
                NetworkManager.℠.openToshi()
            }
            let copy = DefaultButton(title: "Copy again") {
                UIPasteboard.general.string = "0x05a271f117c42d77f77b0e4491f603c0370806e9" 
            }
            popup.addButtons([copy, open, next, cancel])
        case 7:
            let done = CancelButton(title: "Done") { }
            popup.addButtons([done]) 
        default:
            popup.addButtons([next, cancel])
        }
        AppManager.℠.mainVC!.present(popup, animated: true, completion: nil)
        ratingVC.tweak(mode: mode)                                                    
    }
    
    static func synchronized(_ lock: AnyObject, closure: () -> Void) {
        objc_sync_enter(lock)
        closure()
        objc_sync_exit(lock)
    }
    
//    func setDefaultRealmForUser(username: String) {
//        var config = Realm.Configuration()
//
//        // Use the default directory, but replace the filename with the username
//        config.fileURL = config.fileURL!.deletingLastPathComponent()
//            .appendingPathComponent("\(username).realm")
    //
    //        // Set this as the configuration used for the default Realm
    //        Realm.Configuration.defaultConfiguration = config
    //    }
    
    func postBranchTry() {
        if let date = UserDefaults.standard.object(forKey: "yourKey") as? Date {
            let difference = Calendar.current.dateComponents([.day], from: date, to: Date()).day ?? 0 
            if difference >= 30 {
                let prevPoints = (UserDefaults.standard.object(forKey: "prevPoints") as? Int) ?? 0
                let diffPoints = DataManager.℠.currentRating - prevPoints
                var count = 0
                var name: String?
                let hundred =  Double(diffPoints) / 1000.0
                if hundred >= 1 {
                    count = Int(hundred)
                    name = "reward100"
                }
                else {
                    let ten =  Double(diffPoints) / 100.0 
                    if ten >= 1 {
                        count = Int(ten)
                        name = "reward10"
                    }
                    else { 
                        let one =  Double(diffPoints) / 10.0
                        if one >= 1 {
                            count = Int(one)
                            name = "reward1"
                        }
                    }
                } 
                if let action = name {
                    while count > 0 {
                        Branch.getInstance().userCompletedAction(action)
                        // option 2
                        let metadata: [String: Any] = [ :
                            //            "custom_dictionary": 123,
                            //            "anything": "everything"
                        ]
                        Branch.getInstance().userCompletedAction(action, withState: metadata)
                        count -= 1
                    }
                    UserDefaults.standard.set(DataManager.℠.currentRating, forKey: "prevPoints")
                }
            }
        }
    }
    
    func loginIntercom() {
        if let key = UserDefaults.standard.object(forKey: "loginned") as? String {
            Intercom.registerUser(withUserId: key)
            let userAttributes = ICMUserAttributes()
            if let nm = UserDefaults.standard.object(forKey: "loginnedFBName") as? String {
                userAttributes.name = nm
                userAttributes.email = nm
            }
            Intercom.updateUser(userAttributes)
        }
    }
}
 
extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
