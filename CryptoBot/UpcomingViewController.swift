

import UIKit
import Kingfisher

class UpcomingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DataManager.℠.currentStacks = DataManager.℠.upcomingICO 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataManager.℠.upcomingICO.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UpcomingCell") as! UpcomingCell
        let ico = DataManager.℠.upcomingICO[indexPath.row]
        cell.name.text = ico.name
        cell.symbol.text = ico.symbol
        if let link = ico.coinLogoUrl, let url = URL.init(string:link) {
            cell.logo.kf.setImage(with: url, options: NetworkManager.℠.imageCacheOptions(), completionHandler: { 
                (image, error, cacheType, imageUrl) in
                
            })
        } 
        cell.preIcoStart.text = "Pre ICO start: "
        cell.preIcoEnd.text = "Pre ICO end: "
        cell.icoStart.text = "ICO start: "
        cell.icoEnd.text = "ICO end: "
        if let def = ico.preIcoStart?.components(separatedBy: " ")[0], def != "0000-00-00" {
            cell.preIcoStart.text?.append(def)
        } else {
            cell.preIcoStart.text?.append("-//-")
        }
        if let def = ico.preIcoEnd?.components(separatedBy: " ")[0], def != "0000-00-00" {
            cell.preIcoEnd.text?.append(def)
        } else {
            cell.preIcoEnd.text?.append("-//-")
        }
        if let def = ico.icoStart?.components(separatedBy: " ")[0], def != "0000-00-00" {
            cell.icoStart.text?.append(def)
        } else {
            cell.icoStart.text?.append("-//-")
        }
        if let def = ico.icoEnd?.components(separatedBy: " ")[0], def != "0000-00-00" {
            cell.icoEnd.text?.append(def)
        } else {
            cell.icoEnd.text?.append("-//-") 
        }
        cell.icoEnd.superview?.layoutSubviews()
        cell.logo.layer.cornerRadius = 5
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let single = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier:"SingleStackViewController") as! SingleStackViewController
        single.currentICO = DataManager.℠.upcomingICO[indexPath.row]
        single.index = indexPath.row
        single.view.tag = 177 
        single.modalPresentationStyle = .overFullScreen
        single.modalTransitionStyle = .crossDissolve
        let v = UIView.init(frame:single.view.frame)
        v.backgroundColor = UIColor.black.withAlphaComponent(0.78)    
        let tap = UITapGestureRecognizer.init(target:self, action: #selector(closeUpcomingPopup))
        v.addGestureRecognizer(tap) 
        single.view.insertSubview(v, at: 0)
        present(single, animated: true, completion: nil)
    }
    
    @objc func closeUpcomingPopup() {
        self.dismiss(animated: true, completion: nil)
        AppManager.℠.currentMode = .browse
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reloadMode), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reloadStacks), object: nil)
    }

}


class UpcomingCell: UITableViewCell {
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var symbol: UILabel! 
    @IBOutlet weak var preIcoStart: UILabel!
    @IBOutlet weak var preIcoEnd: UILabel!
    @IBOutlet weak var icoStart: UILabel!
    @IBOutlet weak var icoEnd: UILabel!
}

