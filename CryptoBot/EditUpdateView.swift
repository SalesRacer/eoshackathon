
import UIKit

class EditUpdateView: UIViewController, UITextViewDelegate { 

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var commentTextField: VerticallyCenteredTextView!
    @IBOutlet weak var pending: UIButton!
    @IBOutlet weak var pendingHeight: NSLayoutConstraint!
    @IBOutlet weak var caption: UILabel!
    
    @IBOutlet weak var updatesString: UILabel!
    @IBOutlet weak var updatesTable: UITableView! {
        didSet {
            self.updatesTable.register(UINib.init(nibName: "PaperCell", bundle: nil), forCellReuseIdentifier:"PaperCell")
        }
    }
    var paperControl:PaperController!
    
    @IBOutlet weak var wrapping: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commentTextField.delegate = self
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing)))
        
        if let symbol = DataManager.℠.currentICO?.symbol {
            let all = DataManager.℠.getPendingUpdate(symbol: symbol)
            if all.count > 0 {
                paperControl = PaperController(table:self.updatesTable, ico:DataManager.℠.currentICO ?? ICO())
                paperControl.all = all
                self.updatesTable.delegate = paperControl
                self.updatesTable.dataSource = paperControl
            } else {
                wrapping.alpha = 0
                pendingHeight.constant = 0
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        textView.text = ""
        textView.textColor = UIColor.darkText
        return true
    }

    @objc func endEditing() {
        view.endEditing(true)
    }
}

extension EditUpdateView: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing()
        return true
    }
}
