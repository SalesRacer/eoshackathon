

import UIKit
import FoldingCell

class PaperController: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    enum PaperMode : Int {
        case all = 0
        case exact
    }
    
    var all = DataManager.℠.getAllPendingUpdates() 
    
    var foldingTable: UITableView! 
    
    init(table tableView:UITableView, ico:ICO) { 
        foldingTable = tableView
        tableView.layer.cornerRadius = 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (all != nil && all!.count > 0) ? all!.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaperCell", for: indexPath) as! PaperCell 
        let durations: [TimeInterval] = [0.26, 0.2, 0.2]
        cell.durationsForExpandedState = durations
        cell.durationsForCollapsedState = durations
        cell.layer.masksToBounds = false
        cell.layer.cornerRadius = 5
        
        let dict = all![indexPath.row]
        let symbol = dict["symbol"] as! String
        var icoReady:ICO?
        for item in DataManager.℠.dataBase {
            if let sym = item.symbol, sym == symbol {
                icoReady = item
            }
        }
        for item in DataManager.℠.upcomingICO {
            if let sym = item.symbol, sym == symbol {
                icoReady = item
            }
        }
        if let ico = icoReady {
            cell.name.text = ico.name
            cell.name2.text = ico.name
            cell.symbol.text = ico.symbol
            cell.symbol2.text = ico.symbol
            cell.field.text = dict["value"] as? String 
            var imagePath: String?
            if ico.brandLogoUrl != nil && !ico.brandLogoUrl!.isEmpty {
                imagePath = ico.brandLogoUrl
            } else if ico.coinLogoUrl != nil && !ico.coinLogoUrl!.isEmpty {
                imagePath = ico.coinLogoUrl
            }
            if let path = imagePath, NetworkManager.℠.playSession != nil {
                let ready = path.addingPercentEncoding(withAllowedCharacters:.urlQueryAllowed)
                NetworkManager.℠.loadImage(path: URL(string:ready!)!, view: cell.icon)
                NetworkManager.℠.loadImage(path: URL(string:ready!)!, view: cell.icon2)
            }
            var act = ""
            let action = dict["actionType"] as! Int
            switch action {
            case 0:
                act = "editing"
            case 2:
                act = "deletion"
            case 3:
                act = "adding"
            default:
                break
            }
            
            if let type = dict["updateType"] as? Int {
                if let cap = UpdateTypeText[type] { 
                    cell.caption.text = "Request on " + act + " " + cap
                    cell.caption2.text = "Request on " + act + " " + cap
                }
            }
        }
        return cell
    }
       
    
    let kCloseCellHeight: CGFloat = 75
    let kOpenCellHeight: CGFloat = 150
    
    
    var cellHeights = (0..<110).map { _ in C.CellHeight.close }
    
    fileprivate struct C {
        struct CellHeight {
            static let close: CGFloat = 75
            static let open: CGFloat = 150
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard case let cell as FoldingCell = tableView.cellForRow(at: indexPath) else {
            return
        }
        
        var duration = 0.0
        if cellHeights[indexPath.row] == kCloseCellHeight { // open cell
            cellHeights[indexPath.row] = kOpenCellHeight
            cell.unfold(true, animated: true, completion: nil)
            duration = 0.5
        } else {// close cell
            cellHeights[indexPath.row] = kCloseCellHeight
            cell.unfold(false, animated: true, completion: nil)
            duration = 1.1
        }
        
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
            tableView.beginUpdates()
            tableView.endUpdates()
        }, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard case let cell as PaperCell = cell else {
            return
        }
        
        cell.backgroundColor = .clear
        
        if cellHeights[indexPath.row] == kCloseCellHeight {
            cell.unfold(false, animated: false, completion: nil)
        } else {
            cell.unfold(true, animated: false, completion: nil)
        }
        
        //        cell.number = indexPath.row
    }
    
    public enum AnimationType {
        case open
        case close
    }
    
    func animationDuration(itemIndex:NSInteger, type:AnimationType)-> TimeInterval {
        // durations count equal it itemCount
        let durations = [0.33, 0.26, 0.26] // timing animation for each view
        return durations[itemIndex]
    }
}


class PaperCell: FoldingCell {
    
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var symbol: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var icon2: UIImageView!
    @IBOutlet weak var name2: UILabel!
    @IBOutlet weak var symbol2: UILabel!
    @IBOutlet weak var caption2: UILabel!
    @IBOutlet weak var field: UITextView!
    @IBOutlet weak var button: UIButton!
    
    override func awakeFromNib() {
        foregroundView.layer.cornerRadius = 10
        foregroundView.layer.masksToBounds = true
        super.awakeFromNib()
        field.layer.cornerRadius = 5
    }
    
    @IBAction func send(_ sender: Any) {
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
} 
