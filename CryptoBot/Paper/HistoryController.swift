

import UIKit

class HistoryController: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var foldingTable: UITableView! 
    var naics: String?
    var industry: String?
    var ready = [ICO]() 
    
    init(table tableView:UITableView, ico:ICO) {
        foldingTable = tableView
        tableView.layer.cornerRadius = 5
    }
    
//    func reload(noti:NSNotification) {
//        if let symbols = noti.object as? [String] {
//            var ready = [ICO]()
//            for sym in symbols {
//                for ico in DataManager.℠.dataBase {
//                    if ico.symbol! == String(sym) {
//                        ready.append(ico)
//                    }
//                }
//                for ico in DataManager.℠.upcomingICO {
//                    if ico.symbol! == String(sym) {
//                        ready.append(ico)
//                    }
//                }
//            }
//            ready = ready.sorted { (one, two) -> Bool in
//                if let irr = one.marketCap, let irr2 = two.marketCap {
//                    return Int(Float(irr)!) > Int(Float(irr2)!)
//                }
//                return true
//            }
//            self.ready = ready
//            self.foldingTable.reloadData()
//        }
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NetworkManager.℠.historyToKeep.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath) as! HistoryCell
        let item = NetworkManager.℠.historyToKeep[indexPath.row] 
        
        var timeString = ""
        if var time = item["date"] as? Double {
            time = time / 1000
            let date = Date(timeIntervalSince1970: time)
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want 
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = "MM/dd/YY" //Specify your format that you want
            timeString = dateFormatter.string(from: date)
        }
        var token: ICO?
        if let sym = item["symbol"] as? String {
            if let index = DataManager.℠.getElementIndex(symbol:sym, mode: 0) {
                token = DataManager.℠.dataBase[index]
            } else if let index = DataManager.℠.getElementIndex(symbol:sym, mode: 1) {
                token = DataManager.℠.upcomingICO[index]
            }
        }
        
        guard let ico = token else { return cell }
        cell.name.text = ico.name
        cell.symbol.text = ico.symbol
        cell.layer.masksToBounds = false
        cell.layer.cornerRadius = 5
        cell.caption.text = "\(item["points"] as! Int) pts." + "   " + timeString 
        
        var imagePath: String?
        if ico.brandLogoUrl != nil && !ico.brandLogoUrl!.isEmpty {
            imagePath = ico.brandLogoUrl
        } else if ico.coinLogoUrl != nil && !ico.coinLogoUrl!.isEmpty {
            imagePath = ico.coinLogoUrl
        }
        if let path = imagePath, NetworkManager.℠.playSession != nil {
            let ready = path.addingPercentEncoding(withAllowedCharacters:.urlQueryAllowed)
            NetworkManager.℠.loadImage(path: URL(string:ready!)!, view: cell.icon)
        } else {
            cell.icon.image = nil
        }
        
        return cell
    }
    
    func formatPoints(num: Double) -> String {
        var thousandNum = num / 1_000
        var millionNum = num / 1_000_000
        if  num >= 1_000 && num < 1_000_000 {
            if  floor(thousandNum) == thousandNum {
                return("\(Int(thousandNum)) K")
            }
            return("\(thousandNum.roundToPlaces(1)) K")
        }
        if  num > 1_000_000 {
            if  floor(millionNum) == millionNum {
                return "\(Int(thousandNum)) K"
            }
            return "\(millionNum.roundToPlaces(1)) M"
        }
        else{
            if  floor(num) == num {
                return "\(Int(num))" 
            }
            return "\(num)"
        }
    }
}



class HistoryCell : UITableViewCell {
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var symbol: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var icon: UIImageView!
    var ico:ICO?
    
    override func awakeFromNib() {
        self.layer.cornerRadius = 10
        super.awakeFromNib()
        self.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.open)))
    } 
    
    @objc func open() {
        AppManager.℠.mainVC!.dismiss(animated: true, completion: nil)
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.distanceMoveTo), object:ico?.symbol ?? "") 
    }
}



extension Double {
    // Rounds the double to decimal places value
    mutating func roundToPlaces(_ places : Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self.rounded() * divisor) / divisor
    }
}
   
