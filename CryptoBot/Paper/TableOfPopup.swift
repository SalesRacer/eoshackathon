
import UIKit
import Cosmos
import FoldingCell

enum TableOfPopupMode {
    case pending
    case naics
    case industry
    case history
}

class TableOfPopup: UIViewController {
    var mode = TableOfPopupMode.pending
    
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    
    var paperControl:NSObject!
    var currentIco:ICO! 
    
    @IBOutlet weak var foldingTable: UITableView! {
        didSet {
            self.foldingTable.register(UINib.init(nibName: "PaperCell", bundle: nil), forCellReuseIdentifier:"PaperCell")
            self.foldingTable.register(UINib.init(nibName: "NaicsCell", bundle: nil), forCellReuseIdentifier:"NaicsCell")
            self.foldingTable.register(UINib.init(nibName: "HistoryCell", bundle: nil), forCellReuseIdentifier:"HistoryCell")
        } 
    }
    
    var naics: String!
    var industry: String!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var caption: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if mode == .pending {
            paperControl = PaperController(table:self.foldingTable, ico:DataManager.℠.currentICO ?? ICO()) 
        } else if mode == .history {
            paperControl = HistoryController(table:self.foldingTable, ico:DataManager.℠.currentICO ?? ICO())
            titleLabel.text = "Points history"
            caption.text = "Check the latest influenced tokens"
        } else {
            paperControl = NaicsController(table:self.foldingTable, ico:DataManager.℠.currentICO ?? ICO()) 
            titleLabel.text = "Similar ICOs"
            caption.text = "\(naics!) \(industry!)"
            var symbols = [String]()
            if mode == .naics { 
              symbols = DataManager.℠.symbolsOfNaics(naics)
            } else if mode == .industry {
                symbols = DataManager.℠.symbolsOfIndustry(industry)
            }
            var ready = [ICO]()
            for sym in symbols { 
                for ico in DataManager.℠.dataBase {
                    if ico.symbol! == String(sym) {
                        ready.append(ico)
                    }
                }
                for ico in DataManager.℠.upcomingICO {
                    if ico.symbol! == String(sym) {
                        ready.append(ico)
                    }
                }
            }
            ready = ready.sorted { (one, two) -> Bool in
                if let irr = one.marketCap, let irr2 = two.marketCap {
                    return Int(Float(irr)!) > Int(Float(irr2)!)
                }
                return true
            }
            (paperControl as! NaicsController).ready = ready
        }
        self.foldingTable.delegate = paperControl as? UITableViewDelegate
        self.foldingTable.dataSource = paperControl as? UITableViewDataSource
    }
        
}
