

import Foundation
import Alamofire
import SwiftyJSON
import FBSDKCoreKit
import Kingfisher
import Branch
import SwiftMessages
import Arcane
import SwiftLinkPreview

final class NetworkManager {
    static let ℠ = NetworkManager()
    var playSession: String? = nil
    var parameterToChange: UpdateType?
    var pointsOnServer: Int? = nil
    var isOffline = false
    var offlineReason = false
    var isServeer = false
    
    let linkPreview: SwiftLinkPreview
    var timer: Timer?
    
    var loaded = false
    
    private init() {
        linkPreview = SwiftLinkPreview(session: URLSession.shared,
                                       workQueue: SwiftLinkPreview.defaultWorkQueue,
                                       responseQueue: DispatchQueue.main,
                                       cache: InMemoryCache.init())
        ImageCache.default.maxDiskCacheSize = 200 * 1024 * 1024
    }
    
    
    func sendEOS() {
        let (pk, pub) = generateRandomKeyPair(enclave: .Secp256k1)
        
        var transfer = Transfer()
        transfer.from = "konstantin13"
        transfer.to = "fakenewsmint"
        transfer.quantity = "0.2000 EOS" // "custom token here"   
        transfer.memo = "News XXX for token YYY was marked as fake with the comment: ZZZ"
        
        do {
            let importedPk = try PrivateKey(keyString: "5J2TRMPBgjiVdS3cQhq8a2xTzy58aTLFtw4o3o6t6uNHYAnn5M3")
            let importedPub = PublicKey(privateKey: importedPk!)
            
            Currency.transferCurrency(transfer: transfer, code: "eosio.token", privateKey: importedPk!, completion: { (result, error) in
                if error != nil {
                    if error is RPCErrorResponse {
                        print("\((error as! RPCErrorResponse).errorDescription())")
                    } else {
                        print("other error: \(String(describing: error?.localizedDescription))")
                    }
                } else {
                    print("done.")
                }
            })
        } catch {
            
        }
    }
    
    
    func markAsFake(link:String, author:String, date:String, votesUp:Int, votesDown:Int) {
        let account = "konstantin13"
        
        do {
            let importedPk = try PrivateKey(keyString: "5J2TRMPBgjiVdS3cQhq8a2xTzy58aTLFtw4o3o6t6uNHYAnn5M3")
            var data: String = "Link: " + link
            data = data + " BadAuthor: " + author
            data = data + " ThatDate: " + date
            data = data + " votesUp: " + "\(votesUp)"
            data = data + " votesDown: " + "\(votesDown)"
            let abi = try! AbiJson(code: "konstantin13", action: "publish", json: data)
            
            TransactionUtil.pushTransaction(abi: abi, account: account, privateKey: importedPk!, completion: { (result, error) in
                if error != nil {
                    if (error! as NSError).code == RPCErrorResponse.ErrorCode {
                        print("\(((error! as NSError).userInfo[RPCErrorResponse.ErrorKey] as! RPCErrorResponse).errorDescription())")
                    } else {
                        print("other error: \(String(describing: error?.localizedDescription))")
                    }
                } else {
                    print("Ok. Txid: \(result!.transactionId)")
                }
            })
        } catch {
            
        }
    }
    
    func checkNetwork() {
        timer = Timer.scheduledTimer(withTimeInterval: 20, repeats: false) { (time) in
            if !self.loaded {
                self.checkNetwork()
            } else {
                self.timer?.invalidate()
            }
        }                                                                                                 
        let reachability = SCNetworkReachabilityCreateWithName(nil, kServerAddress) 
        var flags = SCNetworkReachabilityFlags()
        SCNetworkReachabilityGetFlags(reachability!, &flags)
        //        func verifyURL(completion: @escaping (_ isValid: Bool)->()) {
        if let url = NSURL(string: kServerAddress) {
            let request = NSMutableURLRequest(url: url as URL)
            request.httpMethod = "HEAD"
            let task = URLSession.shared.dataTask(with: request as URLRequest) { (_, response, error) in
                if let httpResponse = response as? HTTPURLResponse, error == nil && httpResponse.statusCode == 200 {
                    self.isServeer = true
                } else {
                    NetworkManager.℠.offlineReason = true
                }
                if !flags.contains(.reachable) || !self.isServeer  {
                    self.isOffline = true
                    //            DataManager.℠.realmLoaded = DataManager.℠.loadRealm()
                } else {
                    if let name = UserDefaults.standard.object(forKey:"loggedName"), let pass = UserDefaults.standard.object(forKey:"loggedPassword") {
                        self.login(name: name as! String, pass: pass as! String)
                    } else {
                        self.login(name: "mobile_rest_client@m.com", pass: "systemcoinswipe")
                    }
                }
            }
            task.resume()
        }
    } 
    
    
    func loadCount() {
        loadNaics()
        loadIndustry()
        if let play = self.playSession {
            Alamofire.request(kServerAddress + "/api/finished/count", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["PLAY_SESSION" : play]).responseData { (response) in
                switch response.result {
                case .success(let data):
                    let json = JSON(data)
                    DataManager.℠.dataBaseCount = json["data"].intValue
                case .failure(let error):
                    print("Request failed with error: \(error)")
                }
            }
        }
    }
    
    func loadIndustry() {
        if let play = self.playSession {
            Alamofire.request(kServerAddress + "/api/getindustry", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["PLAY_SESSION" : play]).responseData { (response) in
                switch response.result {
                case .success(let data):
                    if let json = JSON(data)["data"].dictionaryObject {
                        for key in json.keys {
                            let val = json[key] as! [String]
                            DataManager.℠.industryReady.append((key, val[0])) 
                        }
                    }
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
                }
            }
        }
    }
    
    func loadNaics() {
        if let play = self.playSession {
            Alamofire.request(kServerAddress + "/api/getallnaisc", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["PLAY_SESSION" : play]).responseData { (response) in
                switch response.result {
                case .success(let data):
                    if let json = JSON(data)["data"].dictionaryObject {
                        for key in json.keys {
                            let val = json[key] as! String
                            let k = val.index(val.startIndex, offsetBy: val.count > 6 ? 6 : val.count) 
                            let year = String(val.prefix(upTo: k))
                            DataManager.℠.naicsReady.append((year, key))
                        }
                    }
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
                }
            }
        }
    }
    
    func paginationLink(_ index:Int) -> String {
        return kServerAddress + "/api/icos?page=0&count=20&status=ended" ///api/finishedall/pagination?page=\(index)&count=10&status=ended" 
    }
    
    func searchBunch(_ name:[String]) {
//        var method = "searchIndustry="
//        method.append(name[0])
//        Alamofire.request((kServerAddress + "/api/icos?" + method), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["PLAY_SESSION" :  self.playSession!]).responseData { (response) in 
//            switch response.result {
//            case .success(let data):
//                let json = JSON(data)
//                let array = json["data"]
//            //                DataManager.℠.parseDatabase(json: array)
//            case .failure(let error):
//                print("Request failed with error: \(error)")
//            }
//        }
    }
    
    func loadDataBase() {
//        DataManager.℠.realmLoaded = DataManager.℠.loadRealm()
        Alamofire.request(kServerAddress + "/api/finishedall", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["PLAY_SESSION" : self.playSession!]).responseData { (response) in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let array = json["data"]
                if array.array == nil {
                    let alert = UIAlertController.init(title: "Looks like the server is temporary down for maintenance", message: "Please try again a bit later", preferredStyle: .alert)
                    if let vc = UIApplication.topViewController() {
                        alert.show(vc, sender: nil)
                    }
//                                        DataManager.℠.realmLoaded = DataManager.℠.loadRealm()
                } else {
                    DataManager.℠.parseDatabase(json: array)
                    
                    self.loadProcessing()
                }
            case .failure(let error):
                print("Request failed with error: \(error)")
            }
        }
    }
    
    func loadDataBasePart(index:Int) {
        Alamofire.request(paginationLink(index), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["PLAY_SESSION" :  self.playSession!]).responseData { (response) in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let array = json["data"]
            //                DataManager.℠.parseDatabase(json: array)
            case .failure(let error):
                print("Request failed with error: \(error)")
            }
        }
    }
    
    func login(name:String, pass:String) {
        if let id = AppManager.℠.deviceId {
            Alamofire.request(kServerAddress + "/registration", method: .post, parameters: ["name": id, "email" : name, "password" : pass, "role" : 1], encoding: JSONEncoding.default, headers: nil).responseData { (response) in
                Alamofire.request(kServerAddress + "/api/login", method: .post, parameters: ["email" : name, "password" : pass], encoding: JSONEncoding.default, headers: nil).responseData { (response) in
                    switch response.result {
                    case .success(let data):
                        if data != nil {
                            self.loaded = true
                            let json = JSON(data)
                            let string = json["data"].rawString()
                            if let str = string, let range = str.range(of:"[") {
                                let str2 = str.substring(from:range.upperBound)
                                self.playSession = str2.substring(to: str2.range(of: "]")!.lowerBound) 
                            }
                            //                                        if !DataManager.℠.realmLoaded {
                            self.loadCount()
                            if AppManager.℠.deviceToken != nil { 
                                self.loadPoints() 
                            }
                            UserDefaults.standard.set(name, forKey: "loggedName")
                            UserDefaults.standard.set(pass, forKey: "loggedPassword")
//                                            self.loadDataBasePart(index: 0)
                            //                if !AppManager.℠.realmLoaded {
                            self.loadDataBase()
                            //                                        } 
                            //                }
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.playSession), object: nil) 
                        } else {
                            NetworkManager.℠.isOffline = true
                            NetworkManager.℠.offlineReason = true
//                            self.login(name: name, pass: pass)
                        }
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                    }
                }
            }
        }
    }
    
    func openToshi() {
        if let url = URL(string: "toshi://") {
            if (UIApplication.shared.canOpenURL(url)) {
                UIApplication.shared.open(url, options: [:]) { (completed) in
                }
            }
        } else if let url = URL(string: "https://itunes.apple.com/us/app/toshi-ethereum-wallet/id1278383455?mt=8") {
            if (UIApplication.shared.canOpenURL(url)) {
                UIApplication.shared.open(url, options: [:]) { (completed) in
                }
            }
        }
    }
    
    func loadProcessing() {
        DispatchQueue.global(qos: .background).async {
            Alamofire.request(kServerAddress + "/api/processingall", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["PLAY_SESSION" : self.playSession!]).responseData { (data) in
                switch data.result {
                case .success(let data):
                    let json = JSON(data)
                    let array = json["data"]
                    DataManager.℠.parseUpcoming(json: array)
                    
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
                }
            }
        }
    }
                
    func retrieve(ico:String, completionHandler: @escaping (DataResponse<Data>) -> Void) {         
        Alamofire.request(kServerAddress + "/api/ico/getinfo/\(ico)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["PLAY_SESSION" : self.playSession!]).responseData(completionHandler: completionHandler)
    }
    
    func report(ico:String, text:String, completionHandler: @escaping (DataResponse<Data>) -> Void) {
        Alamofire.request(kServerAddress + "/api/ico/report", method: .post, parameters: ["ticker": ico, "report": text], encoding: JSONEncoding.default, headers: ["PLAY_SESSION" : self.playSession!]).responseData(completionHandler: completionHandler)
    }
    
    func reportLink(ico:String, text:String, completionHandler: @escaping (DataResponse<Data>) -> Void) {
        Alamofire.request(kServerAddress + "/api/ico/report", method: .post, parameters: ["ticker": ico, "report": text], encoding: JSONEncoding.default, headers: ["PLAY_SESSION" : self.playSession!]).responseData(completionHandler: completionHandler)
    }
    
    func sendLinkedIn(linkedin:String, completionHandler: @escaping (DataResponse<Data>) -> Void) {
        Alamofire.request(kServerAddress + "/api/savelinkedinforuser", method: .post, parameters: ["linkedin": linkedin], encoding: JSONEncoding.default, headers: ["PLAY_SESSION" : self.playSession!]).responseData(completionHandler: completionHandler)
    }
    
    func sendToshi(toshi:String, completionHandler: @escaping (DataResponse<Data>) -> Void) {
        Alamofire.request(kServerAddress + "/api/savetoshiforuser", method: .post, parameters: ["toshi": toshi], encoding: JSONEncoding.default, headers: ["PLAY_SESSION" : self.playSession!]).responseData(completionHandler: completionHandler)
    }
 
    func fakeLink(ico:String, message:String, completionHandler: @escaping (DataResponse<Data>) -> Void) {
        let pars = ["updateType" : 7, "actionType" : 4, "value" : message] as [String : Any]
        DataManager.℠.savePendingUpdate(symbol: ico, cardIndex: nil, pars: pars)
        var params: Dictionary<String,Any> = ["deviceToken" : AppManager.℠.deviceToken ?? "deviceToken", "ticker": ico, "udid" : AppManager.℠.deviceId ?? "deviceId"]
        params.merge(pars) { (one, two) -> Any? in
            return one
        }
        if AppManager.℠.isFacebookLogged() {
            params["fbid"] = FBSDKAccessToken.current().userID
        }
        if let card = DataManager.℠.oneCardTop {
            params["index"] = card.cardServerIndex ?? "QQ"
        } else {
            params["index"] = "QQ"
        }
        UserDefaults.standard.set(ico, forKey: "lastUpdatedTicker")
        Alamofire.request(kServerAddress + "/api/ico/update", method: .post, parameters: params, encoding: JSONEncoding.default, headers: ["PLAY_SESSION" : self.playSession!]).responseData(completionHandler: completionHandler)
    }
    
    func updateLink(ico:String, pars:Dictionary<String,Any>, completionHandler: @escaping (DataResponse<Data>) -> Void) {
        DataManager.℠.savePendingUpdate(symbol: ico, cardIndex: nil, pars: pars) 
        var params: Dictionary<String,Any> = ["deviceToken" : AppManager.℠.deviceToken ?? "deviceToken", "ticker": ico, "udid" : AppManager.℠.deviceId ?? "deviceId"] 
        params.merge(pars) { (one, two) -> Any? in
            return one
        }
        if AppManager.℠.isFacebookLogged() {
            params["fbid"] = FBSDKAccessToken.current().userID
        }
        if let card = DataManager.℠.oneCardTop {
            params["index"] = card.cardServerIndex ?? "AQ"
        } else {
            params["index"] = "AQ"
        }
        UserDefaults.standard.set(ico, forKey: "lastUpdatedTicker")
        Alamofire.request(kServerAddress + "/api/ico/update", method: .post, parameters: params, encoding: JSONEncoding.default, headers: ["PLAY_SESSION" : self.playSession!]).responseData(completionHandler: completionHandler)
    }
    
    func sendRating(ico:ICO, name:String?, rating:Int) {
        Alamofire.request(kServerAddress + "/api/ico/addinfo/rating", method: .post, parameters: ["ratings": [["symbol" : ico.symbol!, "index": name ?? "", "rating" : String(rating), "udid" : AppManager.℠.deviceId]]], encoding: JSONEncoding.default, headers: ["PLAY_SESSION" : self.playSession!]).responseData { (data) in
            switch data.result {
            case .success(let data):
                if let json = JSON(data).dictionaryObject {
                    DataManager.℠.parseRates(json, to:ico)
                }
            case .failure(let error):
                print("Request failed with error: \(error)")
            }
        }
    }
    
    func loadRating(ico:ICO) {
        Alamofire.request(kServerAddress + "/api/ico/addinfo/rating/\(ico.symbol!)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["PLAY_SESSION" : self.playSession!]).responseData { (data) in
            switch data.result {
            case .success(let data):
                if let json = JSON(data).dictionaryObject {
                    DataManager.℠.parseRates(json, to:ico)
                }
            case .failure(let error):
                print("Request failed with error: \(error)")
            }
        }
    }
    
    
    func loadRewards() {
        if let id = AppManager.℠.deviceId {
            Alamofire.request(kServerAddress + "/api/reward/\(id)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["PLAY_SESSION" : self.playSession!]).responseData { (data) in
                switch data.result {
                case .success(let data):
                    if let json = JSON(data).dictionaryObject {
                        
                    }
                case .failure(let error):
                    print("Request failed with error: \(error)")
                }
            }
        }
    }
    
    func deleteRewards() { 
        if let id = AppManager.℠.deviceId {
            Alamofire.request(kServerAddress + "/api/reward/\(id)", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: ["PLAY_SESSION" : self.playSession!]).responseData { (data) in
                switch data.result {
                case .success(let data):
                    if let json = JSON(data).dictionaryObject {
                        
                    }
                case .failure(let error):
                    print("Request failed with error: \(error)")
                }
            }
        }
    }
    
    func imageCacheOptions() -> KingfisherOptionsInfo {
        let downloader = ImageDownloader(name: "huge_image_downloader")
        downloader.downloadTimeout = 150.0
        let cache = ImageCache(name: "longer_cache")
        cache.maxDiskCacheSize = 60 * 60 * 24 * 30
        return [.downloader(downloader), .targetCache(cache)] 
    }
    
    func loadImage(path:URL, view:UIImageView) {
        view.kf.setImage(with: path, options: imageCacheOptions())
    }
    
    func searchLinkedIn(text:String, completionHandler: @escaping (String) -> Void)  {
        Alamofire.request(searchString(text: text.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData { (response) in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let arr = json["items"]
                if arr.count > 0 {
                    let string = arr[0]["link"].stringValue
                    DispatchQueue.main.async { completionHandler(string) }
                }
                
            case .failure(let error):
                print("Request failed with error: \(error)")
            }
        }
    }
    
    private func searchString(text:String) -> String {
        //        return "http://api.duckduckgo.com/?q=\(text)&format=json&t=Coinswipe"
        //        return "http://www.faroo.com/api?q=\("linkedin" + text)&src=web&f=json"
        return "https://www.googleapis.com/customsearch/v1?q=\("linkedin" + text)&key=AIzaSyAfxCc-j82B1DeqjEhZ5Tjcyq7kCrhf9Y0&cx=014861902992379716148:nwsdcwbr1n0"
        //        return "https://www.google.com/search?q=\(text)"//"&key=AIzaSyAfxCc-j82B1DeqjEhZ5Tjcyq7kCrhf9Y0&cx=014861902992379716148:nwsdcwbr1n0"
    }
    
    func updatePoints() {
        updatePoints(card: nil, type: nil, ticker: nil)
    }
    
    func updatePoints(card:String?, type:Int?, ticker:String?) {
        UserDefaults.standard.set(DataManager.℠.currentRating, forKey: "savedPoints")
        var fbid: String? = nil
        if AppManager.℠.isFacebookLogged() {
            fbid = FBSDKAccessToken.current().userID
        }
        if let id = AppManager.℠.deviceId, let serverPoints = pointsOnServer, let deviceToken = AppManager.℠.deviceToken {
            let points = DataManager.℠.currentRating - serverPoints
            if points > 0 {
                pointsOnServer = DataManager.℠.currentRating
                var params = ["udid": id, "fbid": fbid, "points" : points, "deviceToken" : deviceToken] as [String : Any]
                if let c = card {
                    params["index"] = c
                }
                if let c = type {
                    params["type"] = c
                }
                if let c = ticker {
                    params["symbol"] = c
                }
                Alamofire.request(kServerAddress + "/api/points", method: .post, parameters: params, encoding: JSONEncoding.default, headers: ["PLAY_SESSION" : self.playSession!]).responseData { (response) in
                    switch response.result {
                    case .success(let data):
//                        let json = JSON(data)
//                        print(json)

                        self.loadPoints() 
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        
                    }
                }
            }
        }
    }
 
    var historyToKeep = [[String:Any]]()
    var symbols = UserDefaults.standard.object(forKey:"symbolsToKeep") as? [String : [String : Any]] ?? [String : [String : Any]]()
    
    func resendToWatch() {
        if self.playSession != nil {
            AppManager.℠.postToWatches(dict: ["PointsHistory" : historyToKeep, "playsession" : self.playSession!, "Symbols" : symbols])
        }
    }
    
    func parseHistory(dict:[JSON]) {
        for d in dict {
            let inner = d.dictionaryValue
            var n = [String:Any]()
            let keys = inner.keys
            for key in keys {
                n[key] = inner[key]?.rawValue
            }
            historyToKeep.append(n)
        }
        let count = historyToKeep.count
        for i in 0 ..< count {
            let item = historyToKeep[i]
            if let sym = item["symbol"] as? String {
                if nil == symbols[sym] {
                    symbols[sym] = [String : String]()
                    loadAdditionalStuff(symbol: sym)
                }
            }
        }
        resendToWatch()
    }
    
    func saveImageData(data:Data?, name:String?, symbol:String) {
        if nil != data, var dict = symbols[symbol] {
            dict["image"] = data
            dict["name"] = name
            symbols[symbol] = dict
        }
        resendToWatch()
    }
    
    func loadAdditionalStuff(symbol:String) {
        Alamofire.request(kServerAddress + "/api/getlogo?symbol=\(symbol)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["PLAY_SESSION" :  self.playSession!]).responseData { (response) in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let dict = json.dictionaryValue
                if let sym = response.request?.url?.absoluteString.components(separatedBy: "=")[1] {
                    guard let dat = dict["data"]?.dictionaryValue else { return }
                    let name = dat["name"]?.stringValue
                    let link = dat["logoUrl"]?.stringValue
                    KingfisherManager.shared.retrieveImage(with: URL.init(string: link!)!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                        if image != nil {
                            self.saveImageData(data: image!.jpegData(compressionQuality:0.9), name: name, symbol: sym) 
                        }
                    })
                }
                print(data)
            case .failure(let error):
                print("Request failed with error: \(error)")
            }
        }
    }
    
    func loadNews(symbol:String) {
        Alamofire.request(kServerAddress + "/api/getnewsbysymbol/\(symbol)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["PLAY_SESSION" :  self.playSession!]).responseData { (response) in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                let dict = json.dictionaryValue
                if let arr = dict["data"]?.dictionaryValue {
                    for key in arr.keys {
                        if let d:Dictionary = arr[key]!.dictionaryObject {
                            let index = key 
                            let link = d["url"] as! String
                            let sourece = (d["source"] as! Dictionary<String, String>)["name"] as! String
                            let imageUrl = d["urlToImage"] as? String
                            let title = d["title"] as? String
                            let caption = d["description"] as? String
                            var date = "" 
                            if let dat = d["publishedAt"] as? Double {
                                let nsdate = NSDate(timeIntervalSince1970: dat)
                                let formatter = DateFormatter()
                                formatter.dateFormat = "yyyy-MM-dd"
                                date = formatter.string(from: Date())
                            }
                            DataManager.℠.createNewsCard(ico: symbol, link: link, index: index, source: sourece, imageUrl: imageUrl, title: title, description: caption, date: date)
                        }
                    }
                }
                var icoFin:ICO!
                if let arr = DataManager.℠.newsPack[symbol] {
                    for ico in DataManager.℠.dataBase {
                        if ico.symbol == symbol {
                            icoFin = ico
                        }
                    }
                    for ico in DataManager.℠.upcomingICO {
                        if ico.symbol == symbol {
                            icoFin = ico
                            
                        }
                    }
                    icoFin.cards.insert(contentsOf: arr, at: 0)
                    //                    DataManager.℠.newsPack[symbol]?.removeAll()
                }
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.additionalLoaded), object: icoFin)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reloadPager), object: icoFin)
                }
                print(data)
            case .failure(let error):
                print("Request failed with error: \(error)")
            }
        }
    }
    
    func loadPoints() {
        if let deviceID = AppManager.℠.deviceId, let deviceToken = AppManager.℠.deviceToken {
            var server = kServerAddress + "/api/points/ext?udid=\(deviceID)&deviceToken=\(deviceToken)"
            if AppManager.℠.isFacebookLogged() {
                if let fbid = FBSDKAccessToken.current().userID {
                    server = server + "&fbid=\(fbid)"
                }
            }
            Alamofire.request(server, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["PLAY_SESSION" :  self.playSession!]).responseData { (response) in
                switch response.result {
                case .success(let data):
                    let json = JSON(data)
                    let dict = json.dictionaryValue
                    guard let dat = dict["data"]?.dictionaryValue else { return }
                    DispatchQueue.main.async {
                        if let history = dat["history"]?.arrayValue {
                            self.parseHistory(dict: history)
                        }
                    }
                    
                    self.pointsOnServer = dat["points"]?.intValue ?? 0
                    if var points = self.pointsOnServer {
                        if self.pointsOnServer == 0 {
                            if let saved = UserDefaults.standard.object(forKey: "savedPoints") as? Int {
                                DataManager.℠.currentRating = Int(saved) 
                            }
                        } else {
                            DispatchQueue.main.async {
                                DataManager.℠.currentRating = 0
                                if UserDefaults.standard.object(forKey: "bonusAdded") == nil, points > 0 {
                                    
                                    let messageView: MessageView = MessageView.viewFromNib(layout: .centeredView)
                                    messageView.configureBackgroundView(width: 300)
                                    messageView.configureContent(title: "Thanks for being an early Coinswiper!", body: "As promised, here's your 500 free points!  Login with facebook or email to make sure your points are stored. \n\nFor each friend that installs the beta in the next 21 days we'll give you another 500 points and your friend 500 points.", iconImage: nil,iconText: "😎", buttonImage: nil, buttonTitle: "OK") { _ in
                                        SwiftMessages.hide()
                                    }
                                    messageView.backgroundView.backgroundColor = UIColor.init(white: 0.97, alpha: 1)
                                    messageView.backgroundView.layer.cornerRadius = 10
                                    var config = SwiftMessages.defaultConfig
                                    config.presentationStyle = .center
                                    config.duration = .forever
                                    config.dimMode = .blur(style: .dark, alpha: 1, interactive: true)
                                    config.presentationContext  = .window(windowLevel: UIWindow.Level.statusBar)
                                    SwiftMessages.show(config: config, view: messageView)
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.pulsePoints), object: PointsBag.init(points: 500, type: 0, symbol: nil, card: nil))
                                }
                            }
                        }
                        DispatchQueue.main.async {
                            UserDefaults.standard.set(true, forKey: "bonusAdded")
                        }
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.pulsePoints), object: PointsBag.init(points: points, type: 0, symbol: nil, card: nil))
                        
                        Branch.getInstance().loadRewards { (changed, error) in
                            if (error == nil) {
                                let credits = Branch.getInstance().getCredits()
                                print("credit: \(credits)")
                                Branch.getInstance().redeemRewards(credits, callback: {(success, error) in
                                    if success {
                                        print("Redeemed credits!")
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.addCongrats), object: ("You just recieved reward on your account for your share. We appreciate your support, alongside with the whole Coinswipe community - thank you!", PointsBag.init(points: credits, type: 3, symbol: nil, card: nil)))   
                                    } 
                                    else {
                                        print("Failed to redeem credits: \(error)")
                                    }
                                })
                            }
                        }
                        
                        print(json)
                    }
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    
                    
                }
            }
        }
    }
}

import SystemConfiguration

public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}

 
//STORY_PROP(3001, "Story Property", "CARD_PROPERTY"),
//TAGS_PROP(3002, "Tags Property", "CARD_PROPERTY"),
//CONS_PROP(3003, "Cons Property", "CARD_PROPERTY"),
//PROS_PROP(3004, "Pros Property", "CARD_PROPERTY"),
//Обновление их происходит так же как и additional info только нужно указывать индекс к какой карточке это свойство. Мой локальный тестовый пример:
//{
//    "ticker":"THETA",
//    "deviceToken":"82fe22ff3a6f47de76e13be66aa4a085aed3429b634a7d2b6a6d61eeab981337",
//    "value":"Super Mega Story Property",
//    "updateType":3001,
//    "fbid":null,
//    "udid":"test-udid",
//    "actionType":3,
//    "index":"C"
//}
