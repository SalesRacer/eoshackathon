

import UIKit
import FBSDKCoreKit 
import FacebookLogin
import Alamofire
import AlamofireImage
import SideMenu
import Intercom
import LTMorphingLabel
import PopupDialog 

class SideMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, LoginButtonDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userImage: UIImageView!
//    @IBOutlet weak var userName: UILabel!
//    @IBOutlet weak var loginButtonView: UIView!
    
//    @IBOutlet weak var uploadButton: UIView!
//    @IBOutlet weak var additionalLine: UIView!
//    @IBOutlet weak var additionalLineTwo: UIView!
    
    var navigationControllerSaved: UINavigationController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.view.backgroundColor = UIColor.white 
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        
//        self.userImage.layer.cornerRadius = self.userImage.frame.width / 2
//        self.userImage.layer.masksToBounds = true
//        self.userImage.layer.borderColor = UIColor.darkGray.cgColor
//        self.userImage.layer.borderWidth = 2
        
//        self.uploadButton.layer.cornerRadius = 10
//        self.uploadButton.layer.borderColor = UIColor.darkGray.cgColor
//        self.uploadButton.layer.borderWidth = 2
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tweakFacebookView()
        navigationControllerSaved = navigationController
        Intercom.setLauncherVisible(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        Intercom.setLauncherVisible(false) 
    }
    
//    @IBOutlet weak var topView: UIView!
//    @IBOutlet weak var bottomView: UIView!
    
    func tweakFacebookView() { 
//        let loginButton = LoginButton(readPermissions: [ .publicProfile ])
//        loginButton.delegate = self
//        loginButton.center = CGPoint.init(x: self.loginButtonView.bounds.width/2, y: self.loginButtonView.bounds.height/2)
//        self.loginButtonView.addSubview(loginButton)
//
//        if AppManager.℠.isFacebookLogged() {
//            let userId = FBSDKAccessToken.current().userID!
//            
////            Alamofire.request("http://graph.facebook.com/\(userId)/picture?type=large").responseImage { (response) in
////                if let image = response.result.value {
////                    self.userImage.image = image
////                }
////            }
////            let req = FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields":"email,name"], httpMethod: "GET")
////            req?.start(completionHandler: { (connection, response, error) in
////                if let result = response as? [String : String] {
////                    self.userName.text = result["name"]
////                }
////            })
//            UIView.animate(withDuration: 0.5, animations: {
//                self.uploadButton.alpha = 1
////                self.bottomView.alpha = 1
////                self.additionalLineTwo.alpha = 1
//            })
//        }
//        else {
//            self.uploadButton.alpha = 0
////            self.bottomView.alpha = 0
////            self.additionalLineTwo.alpha = 0
//        }
    }
    
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        self.tweakFacebookView()
        NetworkManager.℠.loadPoints() 
    }
    func loginButtonDidLogOut(_ loginButton: LoginButton) {
        self.tweakFacebookView()
    } 
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserDefaults.standard.object(forKey: "loginned") == nil ? 7 : 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sideCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        var imageName: String
        switch indexPath.row {
        case 0:
            imageName = "cards" 
            sideCell.name.text = "All offerings"
        case 1:
            imageName = "heart"
            sideCell.name.text = "Favorites"
        case 2:
            imageName = "exclamation"
            sideCell.name.text = "Report an error"
        case 3:
            imageName = "time"
            sideCell.name.text = "Pending Updates"
        case 4:
            imageName = "question"
            sideCell.name.text = "About"
        case 5:
            imageName = "star"
            let dataManager = DataManager.℠
            if dataManager.currentStacks == dataManager.dataBase {
                sideCell.name.text = "Upcoming ICOs"
            } else {
                sideCell.name.text = "Live ICOs"
            } 
//            sideCell.isUserInteractionEnabled = false
//            sideCell.comingSoon.alpha = 1
//            sideCell.name.textColor = UIColor.lightGray
//        case 6:
//            imageName = "wallet"
//            sideCell.name.text = "Wallet"
        case 6:
            imageName = "eject"
            sideCell.name.text = UserDefaults.standard.object(forKey: "loginned") != nil ? "Logout" : "Login"
        default:
            imageName = "cards"
            sideCell.name.text = "All offerings"
        }
        let image = UIImage.init(named: imageName)
        let templateImage = image?.withRenderingMode(.alwaysTemplate)
        sideCell.thumbnail.image = templateImage
        sideCell.thumbnail.tintColor = UIColor.white 
        return sideCell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let sideCell = tableView.cellForRow(at: indexPath) as! SideMenuCell
        let dataManager = DataManager.℠
        switch indexPath.row {
        case 0:
            dataManager.currentStacks = dataManager.dataBase
            AppManager.℠.currentMode = .browse
            DataManager.℠.currentICO = dataManager.currentStacks[0]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.scrollToItem), object: 0)
        case 1:
            dataManager.currentStacks = dataManager.favourites 
            AppManager.℠.currentMode = .favourites
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.scrollToItem), object: 0) 
        case 2:
            let about = EurekaViewController()
            about.type = .problem
            self.navigationController?.pushViewController(about, animated: true)
            return indexPath
        case 4:
            SideMenuManager.default.menuRightNavigationController?.dismiss(animated: true, completion: {
                self.showAbout()
            }) 
            return indexPath
        case 3:
            SideMenuManager.default.menuRightNavigationController?.dismiss(animated: true, completion: {
                if let all = dataManager.getAllPendingUpdates(), all.count > 0 {
                    let ratingVC = TableOfPopup(nibName: "TableOfPopup", bundle: nil)
                     
                    // Create the dialog
                    let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
                    //        ratingVC.currentIco =
                    //            ratingVC.titleLabel.text = "Review your rating"
                    //            ratingVC.caption.text = "Change the overall rating of the ICO based on your impression. \n\nSuggest the short tagline, that represents qualities of this ICO on your opinion and receive additional points after the approval"
                    
                    // Create first button
                    let buttonOne = CancelButton(title: "CLOSE", height: 50) {
                        
                    }
                    
                    // Add buttons to dialog
                    popup.addButtons([buttonOne])
                    
                    // Present dialog
                    AppManager.℠.mainVC!.present(popup, animated: true, completion: nil)    
                } else {
                    let alertController = UIAlertController(title: "You haven't updated any tokens yet", message: "Please use the \"edit\" button to help the crowdfinding society and get the points", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction.init(title: "OK", style: .cancel, handler: nil))
                    AppManager.℠.mainVC!.present(alertController, animated: true, completion: {
                        
                    })
                }
            })
            return indexPath
        case 5:
            if sideCell.name.text == "Upcoming ICOs" {
                sideCell.name.text = "Live ICOs"
                dataManager.currentStacks = dataManager.upcomingICO
            } else {
                sideCell.name.text = "Upcoming ICOs"
                dataManager.currentStacks = dataManager.dataBase
            }
            if dataManager.currentStacks.count > 0 {
                DataManager.℠.currentICO = dataManager.currentStacks[0]
                AppManager.℠.currentMode = .browse
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.scrollToItem), object: 0)
            }
            //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //            let controller = storyboard.instantiateViewController(withIdentifier: "UpcomingViewController")
//            self.navigationControllerSaved?.pushViewController(controller, animated: true)
//            return indexPath 
        case 6:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.unsubKeyboard), object: nil)
            SideMenuManager.default.menuRightNavigationController?.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.showLogin), object: 0)
            }) 
            return indexPath
        default:
            break
        }
        
        if dataManager.currentStacks.count < 1 {
            let alertController = UIAlertController(title: "You haven't favoritized any tokens yet", message: "Please use the \"heart\" button on the bottom of the screen", preferredStyle: .alert)
            alertController.addAction(UIAlertAction.init(title: "OK", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: {
                
            })
            dataManager.currentStacks = dataManager.dataBase
            AppManager.℠.currentMode = .browse
        }
        SideMenuManager.default.menuRightNavigationController?.dismiss(animated: true, completion: {
            
        }) 
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.menuClick), object: sideCell.name.text)
        return indexPath
    }
    
    func showAbout() {
        let ratingVC = SimplePopup(nibName: "SimplePopup", bundle: nil)
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
        ratingVC.titleLabel.text = "Special thanks"
        ratingVC.textView.text = "https://newsapi.org \nhttps://coinmarketcap.com/ \nhttps://icons8.com/ \nhttps://github.com/Alamofire/Alamofire \nhttps://github.com/xmartlabs/Eureka \nhttps://github.com/Yalantis/Koloda \nhttps://github.com/realm/realm-cocoa \nhttps://github.com/jonkykong/SideMenu \nhttps://github.com/onevcat/Kingfisher \nhttps://www.intercom.com/ \nhttps://github.com/kciter/Floaty \nhttps://github.com/Orderella/PopupDialog"
        ratingVC.textHeight.constant = 300 
        let cancel = CancelButton(title: "Close", dismissOnTap: true) {
            
        }
        popup.addButtons([cancel])
        AppManager.℠.mainVC!.present(popup, animated: true, completion: nil)
    }
    
    func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.height / 7
    }
}


class SideMenuCell: UITableViewCell {
    
    @IBOutlet weak var name: LTMorphingLabel!
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var comingSoon: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.comingSoon.alpha = 0
//        self.comingSoon.layer.borderColor = UIColor.lightGray.cgColor
//        self.comingSoon.layer.borderWidth = 1
//        self.comingSoon.layer.cornerRadius = 5
    }
    
}
