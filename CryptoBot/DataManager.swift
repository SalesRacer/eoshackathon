

import Foundation
import SwiftyJSON
import RealmSwift
import Alamofire
import SwiftSpinner

class DataManager {
    static let ℠ = DataManager()
    private init() {
        self.dataBase = Array() 
        self.favourites = Array()
        self.currentStacks = self.dataBase
        self.pages = [ ]
        self.currentRating = UserDefaults.standard.integer(forKey: "saved")
        
        upcomingICO = Array()
        
//        let nai = naicsOve.components(separatedBy: "\n")
//        for str in nai {
//            let read = str.components(separatedBy: ": ")
//            naicsReady.append((read[1], read[0]))
//        }
    }
    
    func begin() { 
        DispatchQueue.init(label: "add").async {
            self.createCoins()
            
            let stream = InputStream(fileAtPath: Bundle.main.path(forResource: "naics", ofType: "csv", inDirectory: nil)!)!
            let csv = try! CSVReader(stream: stream)
            while let row = csv.next() {
                self.naicsData[row[0]] = row[1]
            }
            self.naicsData["5122"] = "Sound Recording Industries"
            self.generateRanks()
        }
    }
    
//    E-1,0,1,
//    E-2,1,1.1, 500
//    E-3,2,1.25," 1,500 "
//    E-4,3,1.27," 2,000 "
//    E-4,4,1.62," 5,000 "
//    E-5,5,2," 10,000 "
//    E-6,6,2.6," 33,750 "
//    E-7,7,3," 50,625 "
//    E-8,8,3.2," 75,937 "
//    E-8,9,4," 113,906 "
//    E-9,10,4.5," 170,859 "
//    E-9,11,5," 256,289 "
//    E-9,12,10," 384,433 "
//    O-1,13,15," 576,650 "
//    O-2,14,20," 864,975 "
//    O-3,15,25," 1,297,463 "
//    O-4,16,30," 1,946,195 "
//    O-5,17,35," 2,919,292 "
//    O-6,18,40," 4,378,938 "
//    O-7,19,45," 6,568,408 "
//    O-8,20,60," 9,852,612 "
    
    func generateRanks() {
        ranks.append(contentsOf:[
            Rank.init(name: "E-1", multi: 1, points: 0),
            Rank.init(name: "E-2", multi: 1.1, points: 500),
            Rank.init(name: "E-3", multi: 1, points: 1500),
            Rank.init(name: "E-4", multi: 1, points: 3000),
            Rank.init(name: "E-5", multi: 1, points: 5000),
            Rank.init(name: "E-6", multi: 1, points: 10000),
            Rank.init(name: "E-7", multi: 1, points: 20000),
            Rank.init(name: "E-8", multi: 1, points: 40000),
            Rank.init(name: "E-9", multi: 1, points: 50000)])
    }
                                                                          
    func createCoins() {
        coins = [Coin.init(name: "Bitcoin", symbol: "BTC", type: kCoinTypes["PoW"] ?? "?", country: "?", entity: kEntityTypes["Individual"] ?? "?", date: "1" + kDate[9] + "09", hash: kHashTypes["sha256"] ?? "?", contract:""),
        //                Coin.init(name: "Bitcoin Cash", symbol: "BCH", type: "1", country: "1", entity: "1", date: "8117", hash: "B", contract:""),
        //                Coin.init(name: "Propy", symbol: "PRO", type: "1", country: "1", entity: "1", date: "8" + kDate[15] + "17", hash: "6", contract:"0x226bb599a12c826476e3a771454697ea52e9e220"),
        //                Coin.init(name: "Well", symbol: "WELL", type: "1", country: "1", entity: "1", date: "2" + kDate[25] + "18", hash: "6", contract:""),
//        Coin.init(name: "POA", symbol: "POA", type: kCoinTypes["PoW"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Company"] ?? "?", date: "2" + kDate[25] + "18", hash: "?", contract:"0x9e8df8fd7c6c724880e2d73d8faf7fc7ff98c5c4"),
//        Coin.init(name: "Dash", symbol: "DASH", type: kCoinTypes["PoW/PoS"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Individual"] ?? "?", date: "1" + kDate[18] + "14", hash: kHashTypes["x11"] ?? "?", contract:""),
//        Coin.init(name: "NEM", symbol: "XEM", type: kCoinTypes["PoI"] ?? "?", country: kCountries["Japan"] ?? "??", entity: kEntityTypes["Individual"] ?? "?", date: "3" + kDate[31] + "15", hash: kHashTypes["sha3-512"] ?? "?", contract:""),
//        Coin.init(name: "SysCoin", symbol: "SYS", type: kCoinTypes["PoW"] ?? "?", country: kCountries["Canada"] ?? "??", entity: kEntityTypes["Corporation"] ?? "?", date: "7" + kDate[16] + "14", hash: kHashTypes["scrypt"] ?? "?", contract:""),
//        Coin.init(name: "Ethereum Classic", symbol: "ETC", type: kCoinTypes["PoW"] ?? "?", country: kCountries["Switzerland"] ?? "??", entity: kEntityTypes["Group"] ?? "?", date: "7" + kDate[30] + "15", hash: kHashTypes["ethash"] ?? "?", contract:""),
//        Coin.init(name: "Ethereum", symbol: "ETH", type: kCoinTypes["PoW"] ?? "?", country: kCountries["Switzerland"] ?? "??", entity: kEntityTypes["Group"] ?? "?", date: "7" + kDate[22] + "14", hash: kHashTypes["ethash"] ?? "?", contract:""),
//        Coin.init(name: "Nxt", symbol: "NXT", type: kCoinTypes["PoS/LPoS"] ?? "?", country: kCountries["Austria"] ?? "??", entity: kEntityTypes["Individual"] ?? "?", date: "9" + kDate[28] + "13", hash: kHashTypes["sha256"] ?? "?", contract:""),
//        Coin.init(name: "Siacoin", symbol: "SC", type: kCoinTypes["PoW"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Corporation"] ?? "?", date: "3" + kDate[16] + "15", hash: kHashTypes["Blake2b"] ?? "?",  contract:""),
//
//
//        Coin.init(name: "AdEx", symbol: "ADX", type: kCoinTypes["ERC20"] ?? "?", country: "?", entity: kEntityTypes["Group"] ?? "?", date: "6" + kDate[30] + "17", hash: "?", contract:""),
//        Coin.init(name: "Melon", symbol: "MLN", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Switzerland"] ?? "??", entity: kEntityTypes["Stock"] ?? "?", date: "2" + kDate[15] + "17", hash: "?", contract:""),
//        Coin.init(name: "Stratis", symbol: "STRAT", type: kCoinTypes["PoW/PoS"] ?? "?", country: kCountries["UK"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "6" + kDate[20] + "16", hash: kHashTypes["x13"] ?? "?", contract:""),
//        Coin.init(name: "BitConnect", symbol: "BCC", type: kCoinTypes["PoW/PoS"] ?? "?", country: kCountries["UK"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "B" + kDate[15] + "16", hash: kHashTypes["scrypt"] ?? "?", contract:""),
//        Coin.init(name: "Ark", symbol: "ARK", type: kCoinTypes["DPoS"] ?? "?", country: kCountries["?"] ?? "??", entity: kEntityTypes["Group"] ?? "?", date: "B" + kDate[7] + "16", hash: kHashTypes["dpos"] ?? "?", contract:""),
//        Coin.init(name: "Spectrecoin", symbol: "XSPEC", type: kCoinTypes["PoS"] ?? "?", country: kCountries["?"] ?? "??", entity: kEntityTypes["Group"] ?? "?", date: "B" + kDate[11] + "16", hash: kHashTypes["pos3"] ?? "?", contract:""),
//        Coin.init(name: "Aeon", symbol: "AEON", type: kCoinTypes["PoW"] ?? "?", country: kCountries["Liechtenstein"] ?? "??", entity: kEntityTypes["Foundation"] ?? "?", date: "6" + kDate[6] + "14", hash: kHashTypes["cryptonightLite"] ?? "?", contract:""),
//        Coin.init(name: "NEO", symbol: "NEO", type: kCoinTypes["dBFT"] ?? "?", country: kCountries["China"] ?? "??", entity: kEntityTypes["Individual"] ?? "?", date: "A" + kDate[1] + "15", hash: kHashTypes["sha256"] ?? "?", contract:""),
//        Coin.init(name: "NEO", symbol: "NEO", type: kCoinTypes["dBFT"] ?? "?", country: kCountries["China"] ?? "??", entity: kEntityTypes["Individual"] ?? "?", date: "A" + kDate[1] + "15", hash: kHashTypes["sha256ripemd160"] ?? "?", contract:""),
//        Coin.init(name: "Lisk", symbol: "LISK", type: kCoinTypes["DPoS"] ?? "?", country: kCountries["Germany"] ?? "??", entity: kEntityTypes["Group"] ?? "?", date: "2" + kDate[22] + "16", hash: kHashTypes["dpos"] ?? "?", contract:""),
//        Coin.init(name: "Storj", symbol: "STORJ", type: kCoinTypes["PoR"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Group"] ?? "?", date: "7" + kDate[18] + "14", hash: kHashTypes["scrypt"] ?? "?", contract:""),
//        Coin.init(name: "Gambit", symbol: "GAM", type: kCoinTypes["PoW/PoS"] ?? "?", country: kCountries["Seychelles"] ?? "??", entity: kEntityTypes["Corporation"] ?? "?", date: "5" + kDate[5] + "15", hash: kHashTypes["scrypt"] ?? "?", contract:""),
//        Coin.init(name: "Populous", symbol: "PPT", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["UK"] ?? "??", entity: kEntityTypes["Individual"] ?? "?", date: "6" + kDate[24] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Augur", symbol: "REP", type: kCoinTypes["PoW"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Group"] ?? "?", date: "8" + kDate[1] + "15", hash: kHashTypes["sha256ripemd160"] ?? "?", contract:""),
//        Coin.init(name: "Qtum", symbol: "QTUM", type: kCoinTypes["PoS"] ?? "?", country: kCountries["China"] ?? "??", entity: kEntityTypes["Group"] ?? "?", date: "3" + kDate[12] + "17", hash: kHashTypes["pos3"] ?? "?", contract:""),
//        Coin.init(name: "Golem", symbol: "GNT", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Poland"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "B" + kDate[11] + "16", hash: kHashTypes["Equihash"] ?? "?", contract:""),
//        Coin.init(name: "OmiseGo", symbol: "OMG", type: kCoinTypes["PoS"] ?? "?", country: kCountries["Singapore"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "7" + kDate[15] + "17", hash: kHashTypes["PoS"] ?? "?", contract:""),
//        Coin.init(name: "Swarmcity", symbol: "SWT", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Group"] ?? "?", date: "B" + kDate[1] + "16", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "DigixDAO", symbol: "DGD", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Singapore"] ?? "??", entity: kEntityTypes["DAO"] ?? "?", date: "3" + kDate[1] + "16", hash: kHashTypes["smartcontract"] ?? "?", contract:""),
//        Coin.init(name: "Edgeless", symbol: "EDG", type: kCoinTypes["PoW/PoS"] ?? "?", country: kCountries["Germany"] ?? "??", entity: kEntityTypes["Group"] ?? "?", date: "2" + kDate[28] + "17", hash: kHashTypes["sha512"] ?? "?", contract:""),
//        Coin.init(name: "Bitquence", symbol: "BQX", type: kCoinTypes["PoS"] ?? "?", country: kCountries["?"] ?? "??", entity: kEntityTypes["Individual"] ?? "?", date: "7" + kDate[7] + "14", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Waves", symbol: "WAVES", type: kCoinTypes["PoS"] ?? "?", country: kCountries["?"] ?? "??", entity: kEntityTypes["Individual"] ?? "?", date: "4" + kDate[12] + "16", hash: kHashTypes["PoS"] ?? "?", contract:""),
//        Coin.init(name: "NAV Coin", symbol: "NAV", type: kCoinTypes["PoW/PoS"] ?? "?", country: kCountries["New Zealand"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "6" + kDate[6] + "14", hash: kHashTypes["x13"] ?? "?", contract:""),
//        Coin.init(name: "Aeternity", symbol: "AE", type: kCoinTypes["PoW/PoS"] ?? "?", country: kCountries["Liechtenstein"] ?? "??", entity: kEntityTypes["Foundation"] ?? "?", date: "4" + kDate[3] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "0x", symbol: "ZRX", type: kCoinTypes["PoW"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Group"] ?? "?", date: "8" + kDate[15] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Nexium", symbol: "NXC", type: kCoinTypes["PoW"] ?? "?", country: kCountries["France"] ?? "??", entity: kEntityTypes["Stock"] ?? "?", date: "B" + kDate[1] + "16", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Metal", symbol: "MTL", type: kCoinTypes["PoPP"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Stock"] ?? "?", date: "1" + kDate[3] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Etheroll", symbol: "DICE", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["?"] ?? "??", entity: kEntityTypes["Company"] ?? "?", date: "2" + kDate[13] + "17", hash: kHashTypes["sha256"] ?? "?", contract:""),
//        Coin.init(name: "Dentacoin", symbol: "DCN", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Netherlands"] ?? "??", entity: kEntityTypes["Foundation"] ?? "?", date: "A" + kDate[1] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "iExec RLC", symbol: "RLC", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["France"] ?? "??", entity: kEntityTypes["Group"] ?? "?", date: "4" + kDate[19] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "FirstCoin", symbol: "FRST", type: kCoinTypes["PoW"] ?? "?", country: kCountries["Dubai"] ?? "??", entity: kEntityTypes["Individual"] ?? "?", date: "C" + kDate[1] + "16", hash: kHashTypes["scrypt"] ?? "?", contract:""),
//        Coin.init(name: "Santiment", symbol: "SAN", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Switzerland"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "7" + kDate[4] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Komodo ", symbol: "KMD", type: kCoinTypes["dPoW/PoW"] ?? "?", country: kCountries["China"] ?? "??", entity: kEntityTypes["DOA"] ?? "?", date: "A" + kDate[15] + "16", hash: kHashTypes["Equihash"] ?? "?", contract:""),
//        Coin.init(name: "Humaniq ", symbol: "HMQ", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["UK"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "4" + kDate[6] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "FunFair ", symbol: "FUN", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Singapore"] ?? "??", entity: kEntityTypes["Individual"] ?? "?", date: "6" + kDate[22] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Zcoin", symbol: "XZC", type: kCoinTypes["PoW"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["?"] ?? "?", date: "9" + kDate[28] + "16", hash: kHashTypes["Lyra2RE"] ?? "?", contract:""),
//        Coin.init(name: "Wings", symbol: "WINGS", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Russia"] ?? "??", entity: kEntityTypes["Group"] ?? "?", date: "B" + kDate[18] + "16", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "SingularDTV", symbol: "SNGLS", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Group"] ?? "?", date: "A" + kDate[5] + "16", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "FirstBlood", symbol: "1ST", type: kCoinTypes["?"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Corporation"] ?? "?", date: "9" + kDate[25] + "16", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "EOS", symbol: "EOS", type: kCoinTypes["DPoS"] ?? "?", country: kCountries["Cayman Islands"] ?? "??", entity: kEntityTypes["Company"] ?? "?", date: "6" + kDate[26] + "17", hash: kHashTypes["DPoS"] ?? "?", contract:""),
//        Coin.init(name: "BitCrystals", symbol: "BCY", type: kCoinTypes["PoS"] ?? "?", country: kCountries["Switzerland"] ?? "??", entity: kEntityTypes["DAO"] ?? "?", date: "7" + kDate[29] + "15", hash: kHashTypes["DPoS"] ?? "?", contract:""),
//        Coin.init(name: "Quantum Resistant Ledger", symbol: "QRL", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["?"] ?? "??", entity: kEntityTypes["?"] ?? "?", date: "5" + kDate[1] + "17", hash: kHashTypes["sha256"] ?? "?", contract:""),
//        Coin.init(name: "Gnosis", symbol: "GNO", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Gibraltar"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "4" + kDate[24] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Tokes", symbol: "TKS", type: kCoinTypes["Waves"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Individual"] ?? "?", date: "C" + kDate[2] + "16", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Status", symbol: "SNT", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Switzerland"] ?? "??", entity: kEntityTypes["Corporation"] ?? "?", date: "6" + kDate[20] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "MobileGo", symbol: "MGO", type: kCoinTypes["Waves"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["NonProfit"] ?? "?", date: "4" + kDate[25] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Civic", symbol: "CVC", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Corporation"] ?? "?", date: "7" + kDate[6] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "OBITS", symbol: "OBITS", type: kCoinTypes["BitShares"] ?? "?", country: kCountries["Denmark"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "C" + kDate[1] + "15", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "VOISE", symbol: "VSM", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Spain"] ?? "??", entity: kEntityTypes["?"] ?? "?", date: "5" + kDate[6] + "17", hash: kHashTypes["scrypt"] ?? "?", contract:""),
//        Coin.init(name: "Golos", symbol: "GOLOS", type: kCoinTypes["PoW"] ?? "?", country: kCountries["Russia"] ?? "??", entity: kEntityTypes["Group"] ?? "?", date: "B" + kDate[1] + "16", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "IOTA", symbol: "MIOTA", type: kCoinTypes["?"] ?? "?", country: kCountries["Germany"] ?? "??", entity: kEntityTypes["NonProfit"] ?? "?", date: "B" + kDate[25] + "15", hash: kHashTypes["sha3"] ?? "?", contract:""),
//        Coin.init(name: "Pluton", symbol: "PLU", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["UK"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "6" + kDate[21] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Lykke", symbol: "LKK", type: kCoinTypes["?"] ?? "?", country: kCountries["Switzerland"] ?? "??", entity: kEntityTypes["Corporation"] ?? "?", date: "9" + kDate[16] + "16", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Centra", symbol: "CTR", type: kCoinTypes["?"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Corporation"] ?? "?", date: "9" + kDate[19] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Synereo", symbol: "AMP", type: kCoinTypes["Pow/PoSC"] ?? "?", country: kCountries["Israel"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "3" + kDate[23] + "15", hash: kHashTypes["scrypt"] ?? "?", contract:""),
//        Coin.init(name: "ION", symbol: "ION", type: kCoinTypes["PoW"] ?? "?", country: kCountries["Seychelles"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "5" + kDate[16] + "15", hash: kHashTypes["scrypt"] ?? "?", contract:""),
//        Coin.init(name: "Counterparty", symbol: "XCP", type: kCoinTypes["PoW"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Corporation"] ?? "?", date: "1" + kDate[2] + "14", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Litecoin", symbol: "LTC", type: kCoinTypes["PoW"] ?? "?", country: kCountries["Singapore"] ?? "??", entity: kEntityTypes["NonProfit"] ?? "?", date: "7" + kDate[10] + "11", hash: kHashTypes["scrypt"] ?? "?", contract:""),
//        Coin.init(name: "Taas", symbol: "TAAS", type: kCoinTypes["PoM"] ?? "?", country: kCountries["Singapore"] ?? "??", entity: kEntityTypes["Foundation"] ?? "?", date: "3" + kDate[27] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "TenX", symbol: "PAY", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Singapore"] ?? "??", entity: kEntityTypes["Company"] ?? "?", date: "6" + kDate[24] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Monaco", symbol: "MCO", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Switzerland"] ?? "??", entity: kEntityTypes["Company"] ?? "?", date: "5" + kDate[18] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Darcrus", symbol: "DAR", type: kCoinTypes["Waves"] ?? "?", country: kCountries["?"] ?? "??", entity: kEntityTypes["Individual"] ?? "?", date: "C" + kDate[21] + "16", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "WeTrust", symbol: "TRST", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Corporation"] ?? "?", date: "3" + kDate[2] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Bankcoin", symbol: "B@", type: kCoinTypes["PoW/PoS"] ?? "?", country: kCountries["Mexico"] ?? "??", entity: kEntityTypes["?"] ?? "?", date: "B" + kDate[5] + "16", hash: kHashTypes["x11"] ?? "?", contract:""),
//        Coin.init(name: "Incent", symbol: "INCNT", type: kCoinTypes["Waves"] ?? "?", country: kCountries["Australia"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "9" + kDate[1] + "16", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Metaverse ETP", symbol: "ETP", type: kCoinTypes["PoW"] ?? "?", country: kCountries["China"] ?? "??", entity: kEntityTypes["NonProfit"] ?? "?", date: "9" + kDate[5] + "16", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Fuck Token", symbol: "FUCK", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["?"] ?? "??", entity: kEntityTypes["?"] ?? "?", date: "7" + kDate[13] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Aragon", symbol: "ANT", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Spain"] ?? "??", entity: kEntityTypes["NonProfit"] ?? "?", date: "5" + kDate[17] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Chronobank", symbol: "TIME", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Australia"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "C" + kDate[12] + "16", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Lunyr", symbol: "LUN", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Stock"] ?? "?", date: "3" + kDate[29] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "SONM", symbol: "SNM", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Russia"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "6" + kDate[15] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Bancor", symbol: "BNT", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Israel"] ?? "??", entity: kEntityTypes["NonProfit"] ?? "?", date: "6" + kDate[12] + "17", hash: kHashTypes["sha256"] ?? "?", contract:""),
//        Coin.init(name: "Voxels", symbol: "VOX", type: kCoinTypes["PoW"] ?? "?", country: kCountries["Argentina"] ?? "??", entity: kEntityTypes["Corporation"] ?? "?", date: "B" + kDate[3] + "15", hash: kHashTypes["scrypt"] ?? "?", contract:""),
//        Coin.init(name: "Mothership", symbol: "MSP", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Estonia"] ?? "??", entity: kEntityTypes["NonProfit"] ?? "?", date: "5" + kDate[7] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "district0x", symbol: "DNT", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["?"] ?? "?", date: "7" + kDate[18] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Quantum", symbol: "QAU", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Singapore"] ?? "??", entity: kEntityTypes["NonProfit"] ?? "?", date: "????", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "adToken", symbol: "ADT", type: kCoinTypes["?"] ?? "?", country: kCountries["?"] ?? "??", entity: kEntityTypes["?"] ?? "?", date: "6" + kDate[26] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "TokenCard", symbol: "TKN", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["UK"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "5" + kDate[2] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Zcash", symbol: "ZEC", type: kCoinTypes["PoW"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "????", hash: kHashTypes["Equihash"] ?? "?", contract:""),
//        Coin.init(name: "Matchpool", symbol: "GUP", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["?"] ?? "??", entity: kEntityTypes["?"] ?? "?", date: "4" + kDate[2] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Cofound.it", symbol: "CFI", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Sweden"] ?? "??", entity: kEntityTypes["?"] ?? "?", date: "6" + kDate[7] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "SunContract", symbol: "SNC", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Slovenia"] ?? "??", entity: kEntityTypes["Company"] ?? "?", date: "6" + kDate[28] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Patientory", symbol: "PTOY", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Corporation"] ?? "?", date: "5" + kDate[31] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "CoinDash", symbol: "CDT", type: kCoinTypes["?"] ?? "?", country: kCountries["Gibraltar"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "7" + kDate[17] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Waves Community Token", symbol: "WCT", type: kCoinTypes["Waves"] ?? "?", country: kCountries["Russia"] ?? "??", entity: kEntityTypes["?"] ?? "?", date: "3" + kDate[1] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "FundYourselfNow", symbol: "FYN", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["Singapore"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "????", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "OpenAnx", symbol: "OAX", type: kCoinTypes["ERC20"] ?? "?", country: kCountries["China"] ?? "??", entity: kEntityTypes["NonProfit"] ?? "?", date: "7" + kDate[21] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Tierion", symbol: "TNT", type: kCoinTypes["?"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Corporation"] ?? "?", date: "8" + kDate[27] + "17", hash: kHashTypes["sha256"] ?? "?", contract:""),
//        Coin.init(name: "Legends Room", symbol: "LGD", type: kCoinTypes["?"] ?? "?", country: kCountries["US"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "4" + kDate[18] + "17", hash: kHashTypes["scrypt"] ?? "?", contract:""),
//        Coin.init(name: "Rialto", symbol: "XRL", type: kCoinTypes["?"] ?? "?", country: kCountries["UK"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "????", hash: kHashTypes["Arbitrage"] ?? "?", contract:""),
//        Coin.init(name: "Feathercoin", symbol: "FTC", type: kCoinTypes["?"] ?? "?", country: kCountries["?"] ?? "??", entity: kEntityTypes["Individual"] ?? "?", date: "4" + kDate[16] + "13", hash: kHashTypes["NeoScrypt"] ?? "?", contract:""),
//        Coin.init(name: "Wagerr", symbol: "WGR", type: kCoinTypes["PoS"] ?? "?", country: kCountries["Belize"] ?? "??", entity: kEntityTypes["Limited company"] ?? "?", date: "7" + kDate[1] + "17", hash: kHashTypes["?"] ?? "?", contract:""),
//        Coin.init(name: "Stox", symbol: "STX", type: kCoinTypes["?"] ?? "?", country: kCountries["?"] ?? "??", entity: kEntityTypes["Gibraltar"] ?? "?", date: "8" + kDate[2] + "17", hash: kHashTypes["?"] ?? "?", contract:"")
        ]
    }
    
    func symbolsOfNaics(_ naics:String) -> [String] {
        let k = naics.index(naics.startIndex, offsetBy: naics.count >= 6 ? 6 : naics.count)
        let pref = naics.prefix(upTo: k) 
        if pref.count > 0 {
            var symbols = [String]()
            for sym in DataManager.℠.naicsReady {
                if sym.0 == pref {
                    if !symbols.contains(sym.1) {
                        symbols.append(sym.1) 
                    }
                }
            }
            return symbols
        } else {
            return []
        }
    }
    
    func symbolsOfIndustry(_ naics:String) -> [String] { // ???
        var ready = naics
        let sep = naics.components(separatedBy: "/")
        if sep.count > 0 {
            var symbols = [String]()
            for one in sep {
                for sym in DataManager.℠.industryReady {
                    if sym.1 == one {
                        if !symbols.contains(sym.0) {
                            symbols.append(sym.0) 
                        }
                    }
                }
            }
            return symbols
        } else {
            
            var symbols = [String]()
            for sym in DataManager.℠.industryReady {
                if sym.1 == ready {
                    if !symbols.contains(sym.0) {
                        symbols.append(sym.0)
                    }
                }
            }
            return symbols
        }
    }

//    func symbolsOfIndustry(_ ind:String) -> [String] {
//        let pref = ind.components(separatedBy: ",")
//        if pref.count > 0 {
//            let trimmedString = myString.trimmingCharacters(in: .whitespacesAndNewlines)
//            var symbols = [String]()
//            for sym in DataManager.℠.naicsReady {
//                if sym.0 == pref {
//                    if !symbols.contains(sym.1) {
//                        symbols.append(sym.1)
//                    }
//                }
//            }
//            return symbols
//        } else {
//            return []
//        }
//    }
    
    var naicsData = Dictionary<String, String>()
    var naicsReady = [(String, String)]()
    var industryReady = [(String, String)]()
    
    var pages: Array<Array<ICO>>
    var ranks: Array<Rank> = []
    var dataBase: Array<ICO> 
    var dataBaseCount: Int = 10 
    var favourites: Array<ICO>
    var favsSymbols = UserDefaults.standard.array(forKey: "favourites") as? Array<String>  ?? Array<String>()
    var currentICO: ICO?
    var currentStacks: Array<ICO> 
    var realmLoaded = false
    
    var currentRating:Int = 0
    var upcomingICO:Array<UpcomingICO>
    
    var loginEnoughPoints:Int = 0
    var bountyOnScreen:Bool = false // ha ha
    
    var counetrr = 0
    
    var oneCardTop: OneSwipeView? = nil // !!!
    var oneCardBaseTop: OneCardView? = nil
    
    var newsPack: Dictionary<String,[Card]> = Dictionary()
    
    func parseDatabase(json: JSON) { 
        self.dataBase.removeAll()
        for dict in json.array! {
            let ico = self.createICOfromDict(dict: dict.dictionary!)
            self.dataBase.append(ico)
            if let sym = ico.symbol {
                if favsSymbols.contains(sym) {
                    favourites.append(ico)
                }
            }
        } 
        self.currentStacks = self.dataBase
        self.reorderDB(sort: .price)
        DataManager.℠.showInitialLoading(view: nil)
//        createCards()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.updateCurrentICO), object: index)
        //                saveRealm(withDB: true) 
    }
    
    func createNewsCard(ico:String, link:String, index: String, source:String, imageUrl:String?, title:String?, description:String?, date:String?) {
        let card = NewsCard() 
        card.type = CardType.rich
        card.links.append(link)
        card.index = index
        card.source = source
        card.imageUrl = imageUrl
        card.title = title
        card.description = description
        card.date = date
        var arr = newsPack[ico] ?? [Card]()
        if ico == "EOS", newsPack[ico] != nil, newsPack[ico]?.count == 1 {
            let card = Card()
            card.type = CardType.rich
            card.links.append("https://www.cnbc.com/2018/05/31/a-blockchain-start-up-just-raised-4-billion-without-a-live-product.html")
            card.index = "fakeandfirst"
            card.cons = "Predict price will decrease by 10% within 30 days of this artcile"
            card.fake = "Money was raised over last ten months, not all at once as suggested by the headline"
            arr.append(card)
            newsPack[ico] = arr
        }                                    
        arr.append(card)
        newsPack[ico] = arr
    }
                                                                                                   
    func createBountyCard(ico:String) { 
        let card = Card()
        card.type = CardType.terms
        card.index = "fakeindex"
        var icoFin:ICO!
        for ic in DataManager.℠.dataBase {
            if ic.symbol == ico {
                icoFin = ic
            }
        }
        for ic in DataManager.℠.upcomingICO {
            if ic.symbol == ico {
                icoFin = ic
            }
        }
        icoFin.cards.insert(card, at: 0)
    }
    
    func createCards() {
        //        for ico in DataManager.℠.dataBase {
//            if ico.symbol == "BTC" {
//                createCard(ico: ico, link: "https://www.engadget.com/2018/06/13/bitcoin-bitfinex-price-manipulation-cryptocurrency/")
//                createCard(ico: ico, link: "https://www.express.co.uk/finance/city/976816/Bitcoin-price-ripple-cryptocurrency-ethereum-BTC-to-USD-XRP-news")
//                createCard(ico: ico, link: "https://ethereumworldnews.com/bitcoin-btc-still-stable-after-the-bithumb-hack/")
//                createCard(ico: ico, link: "https://www.cnbc.com/2018/06/20/bitcoin-is-not-for-me-goldman-sachs-ceo-lloyd-blankfein-says.html")
//                createCard(ico: ico, link: "https://www.newsbtc.com/2018/06/20/bitcoin-days-future-past-explained/")
//                createCard(ico: ico, link: "https://www.news.com.au/finance/money/investing/fatal-flaw-thats-doomed-bitcoin/news-story/533ad826ee91c70318bdf8f7b50ef989")
//            }
//            if ico.symbol == "EOS" {
//                createCard(ico: ico, link: "https://www.cnbc.com/amp/2018/05/31/a-blockchain-start-up-just-raised-4-billion-without-a-live-product.html")
//                createCard(ico: ico, link: "https://www.coindesk.com/eos-locked-7-accounts-implications-everyone-crypto/")
//                createCard(ico: ico, link: "https://www.ccn.com/eos-freezes-seven-accounts-following-mainnet-snafu/")
//                createCard(ico: ico, link: "https://www.coindesk.com/cold-reception-crypto-reacted-eos-blockchain-freeze/")
//                createCard(ico: ico, link: "https://thenextweb.com/hardfork/2018/06/19/eos-cryptocurrency-constitution/")
//                createCard(ico: ico, link: "https://www.ccn.com/eos-faces-constitutional-crisis-over-frozen-accounts/")
//            }
//        }
//        for ico in DataManager.℠.upcomingICO {
//            if ico.symbol == "GRAM" {
//                createCard(ico: ico, link: "https://news.crunchbase.com/news/telegram-ico-raises-1-7b/")
//                createCard(ico: ico, link: "https://news.bitcoin.com/telegram-rakes-in-over-1-5-billion-ditches-ico-for-an-open-network/")
//                createCard(ico: ico, link: "http://cryptonewsreview.com/telegram-to-put-brakes-on-public-ico-for-its-gram-token-wsj/")
//                createCard(ico: ico, link: "https://www.wsj.com/articles/telegram-messaging-app-scraps-plans-for-public-coin-offering-1525281933")
//            }
//        }
        
    }
    
    func parseUpcoming(json: JSON) {
        self.upcomingICO.removeAll()
        guard let arr = json.array else { return }
        for dict in arr {
            let ico = self.createUpcomingICOfromDict(dict: dict.dictionary!)
            
            if ico.name != "Doft" {
                self.upcomingICO.append(ico)
            }
            if let sym = ico.symbol {
                if self.favsSymbols.contains(sym) {
                    self.favourites.append(ico)
                }
            }
        }
        self.reorderUpcoming()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.fillSuggestions), object: nil)
    }

    
    func saveRealm(withDB:Bool) {
        //        DispatchQueue.global(qos: .userInitiated).async {
        if DataManager.℠.dataBase.count > 0 {
            let realm = try! Realm()
            for ico in self.dataBase {
                try! realm.write() {
                    realm.add(ico, update:true)
                }
            }
            for ico in self.upcomingICO {
                try! realm.write() {
                    realm.add(ico, update:true)
                }
            }
        }
//        self.reloadDataBase(realm: realm)
//    }
        }
    
    func reloadDataBase(realm: Realm) {
        self.dataBase.removeAll()
        self.dataBase.append(contentsOf:realm.objects(ICO.self).toArray(ofType: ICO.self))
    }
    
    func loadRealm() -> Bool {
        let realm = try! Realm()
            self.dataBase.removeAll()
            self.dataBase.append(contentsOf:realm.objects(ICO.self).toArray(ofType: ICO.self))
        self.upcomingICO.append(contentsOf:realm.objects(UpcomingICO.self).toArray(ofType: UpcomingICO.self))
            
            self.currentStacks = self.dataBase
            let loaded = self.dataBase.count > 0
//            if loaded {
//                DataManager.℠.showInitialLoading(view: nil) 
//            }
//        }
        return dataBase.count > 0
    }
    
    func filterTickers(withString string:String) -> Array<ICO> {
        var filteredCandies = dataBase.filter({( ico : ICO) -> Bool in
            return (ico.name?.lowercased().contains(string.lowercased()))!
        })
        let filteredCandies2 = dataBase.filter({( ico : ICO) -> Bool in
            return (ico.symbol?.lowercased().contains(string.lowercased()))!
        })
        filteredCandies.append(contentsOf:filteredCandies2)
        return filteredCandies.orderedSet
    }
    
    var counter = 0
    
    func createUpcomingICOfromDict(dict: [String:JSON]) -> UpcomingICO {
        let ico = UpcomingICO()
        for (key,value) in dict {
            if value.type == .string {
                if ico.responds(to: Selector(key)) {
                    ico.setValue(value.string, forKey: key)
                }
            }
            if key == "price" {
                ico.price = String(value.floatValue)
            }
            if key == "marketCap" {
                ico.marketCap = String(value.intValue) 
            }
            if key == "cover" {
                ico.coverLink = value.stringValue 
            }
            if key == "existsDataCount" {
                ico.existsDataCount = value.intValue
            }
        }
        return ico
    }
    
    func createICOfromDict(dict: [String:JSON]) -> ICO {
        let ico = ICO()
        for (key,value) in dict {
            if value.type == .string {
                if ico.responds(to: Selector(key)) {
                    ico.setValue(value.string, forKey: key)
                }
            }
            if key == "price" {
                ico.price = String(value.floatValue) 
            }
            if key == "marketCap" {
                ico.marketCap = String(value.intValue)
            }
            if value.stringValue == "TSLA" {
                
            }
            if key == "existsDataCount" {
                ico.existsDataCount = value.intValue
            }
        }
//        ico.onDayRate = ICO.onDayRates[ico.name!]
//        if ico.initialPrice != nil {
//            print("initialPriceICO" + ico.initialPrice!)
//        }
        return ico
    }
    
    
    func performRealm(_ handler: () -> Void) { 
        let realm = try! Realm()
        try! realm.write() {
            handler()
        }
    }
    
    func showInitialLoading(view:UIView?) {
        if let v = view {
            print("loadingshow")
            SwiftSpinner.useContainerView(v)
            SwiftSpinner.show("Getting latest ICO data")
        } else {
            print("loadinghide")
            SwiftSpinner.hide() 
            
            if let token = AppManager.℠.scrollToToken {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.distanceMoveTo), object:token) 
            }
        }
    }
    
    func nextStepTutorial(toStart:Int) {
        if AppManager.℠.tutorialStarted, toStart == AppManager.℠.tutorialCounter + 1 {
            AppManager.℠.tutorialCounter += 1
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.startShowcase), object: AppManager.℠.tutorialCounter) 
        }
    }
     
    func reorderDB(sort:SortingStyle) {
        switch sort {
        case .price:
            self.dataBase = self.dataBase.sorted(by: { (one, two) -> Bool in
                if one.initialPrice != nil {
                    if two.initialPrice != nil {
                        let priceOne = Float(one.IRR())
                        let priceTwo = Float(two.IRR())
                        return priceTwo < priceOne
                    }
                    return true
                }
                return false
            })
        case .alpha:
            self.dataBase = self.dataBase.sorted(by: { (one, two) -> Bool in
                one.name! < two.name!
            })
        case .filled:
            self.dataBase = self.dataBase.sorted(by: { (one, two) -> Bool in
                one.existsDataCount > two.existsDataCount
            })
        }
        self.currentStacks = self.dataBase
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reloadStacks), object: index)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reloadMode), object: nil)
    }
    
    
    
    func reorderUpcoming() {
            self.upcomingICO = self.upcomingICO.sorted(by: { (one, two) -> Bool in
                one.existsDataCount > two.existsDataCount || one.coverLink != nil
            })
        if currentStacks == upcomingICO {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reloadStacks), object: index)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reloadMode), object: nil)
        }
    }
    
    func parseRates(_ rates:[String:Any], to ico:ICO) {
        guard let result = rates["data"] as? [String:Any] else { return }
        guard let ready = result[ico.symbol!] as? [String:[String:String]] else { return }
        
        performRealm {
            ico.percentAll = unwrapSign(sign: ready["total"])
//            ico.percentTeam = unwrapSign(sign: ready["linkedProfiles"])
//            ico.percentTime = unwrapSign(sign: ready["roadmap"])
//            ico.percentTerms = unwrapSign(sign: ready["terms"])
//            ico.percentTech = unwrapSign(sign: ready["techStack"])
//            ico.percentTam = unwrapSign(sign: ready["stats"])
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.ratingLoaded), object: ico)
    }
    
    func unwrapSign(sign:[String:String]?) -> String? {
        if let go = sign {
            if let pos = go["positive"], let neg = go["negative"] {
                
                if let p = Int(pos), let n = Int(neg) {
                    if p == 0 {                        if n == 0 {
                            return nil
                        } else {
                            return "0%"
                        }
                    } else {
                        if n == 0 {
                            return "100%"
                        } else {
                            let percent = Float(p) / Float(p + abs(n)) * 100
                            return "\(Int(percent))%"
                        }
                    }
                }
            }
        }
        return nil
    }
    
    func getRatingsInfo() -> Dictionary<String,Dictionary<String,Double>> {
        if let data = UserDefaults.standard.object(forKey: "ratingsInfo") as? Data {
            if let dictionary = NSKeyedUnarchiver.unarchiveObject(with: data) as? Dictionary<String,Dictionary<String,Double>> { 
                return dictionary
            }
        }
        return Dictionary<String,Dictionary<String,Double>>()
    }
    func saveRatingsInfo(info:Dictionary<String,Dictionary<String,Double>>) {
        let dataExample: Data = NSKeyedArchiver.archivedData(withRootObject: info)
        UserDefaults.standard.set(dataExample, forKey: "ratingsInfo")
    }
    
    func saveRating(rating:Double, for ico:ICO, with index:String) { 
        var ratings = getRatingsInfo()
        var dict = ratings[ico.name!]
        
        if dict == nil { dict = Dictionary<String,Double>() }
        if dict![index] != nil {
            return
        }
        dict![index] = rating
        
        var ove: Dictionary<String,[Double]>?
        if let data = UserDefaults.standard.object(forKey: "ratingsInfoOve") as? Data {
            ove = NSKeyedUnarchiver.unarchiveObject(with: data) as? Dictionary<String,[Double]>
        }
                
        if ove == nil { ove = Dictionary<String,[Double]>() }
        var oveValue = ove![ico.name!] ?? [0,0]
        oveValue = rating < 0 ? [oveValue[0] + 1, Double(dict!.count)] : [oveValue[0], Double(dict!.count)]
        ove![ico.name!] = oveValue   
        let dataExample = NSKeyedArchiver.archivedData(withRootObject: ove!)  
        UserDefaults.standard.set(dataExample, forKey: "ratingsInfoOve")
        
        ratings[ico.name!] = dict
        saveRatingsInfo(info: ratings)
    }
    
    func saveOveRating(rating:Double, for ico:ICO) {
        var ratings = getRatingsInfo()
        var dict = ratings[ico.name!]
        
        var ove: Dictionary<String,[Double]>?
        if let data = UserDefaults.standard.object(forKey: "ratingsInfoOve") as? Data {
            ove = NSKeyedUnarchiver.unarchiveObject(with: data) as? Dictionary<String,[Double]>
        }
        
        if ove == nil { ove = Dictionary<String,[Double]>() }
        var oveValue = ove![ico.name!] ?? [0,0]
        oveValue = [Double(dict!.count) - rating * Double(dict!.count), Double(dict!.count)]
        ove![ico.name!] = oveValue
        let dataExample = NSKeyedArchiver.archivedData(withRootObject: ove!)
        UserDefaults.standard.set(dataExample, forKey: "ratingsInfoOve")
        
        ratings[ico.name!] = dict
        saveRatingsInfo(info: ratings)
    }
    func loadRating(for ico:ICO, with index:String) -> Double? { 
        var ratings = getRatingsInfo()
        if let name = ratings[ico.name!], let num = name[index] {
            return num
        } else {
            return nil
        }
    }
    func loadOveRating(ico:String) -> Double? {
        if let data = UserDefaults.standard.object(forKey: "ratingsInfoOve") as? Data {
            if let ove = NSKeyedUnarchiver.unarchiveObject(with: data) as? Dictionary<String,[Double]> {
                if let rating = ove[ico] {
                    if rating[0] == 0 {
                        return 1.0
                    } else if rating[0] == rating[1] {
                        return 4.5
                    }
                    return Double(rating[0]) / Double(rating[1])
                }
            }
        }
        return nil
    }
    
    
    
    var coins = [Coin]()
    
    func code(for sym:String) -> String? {
        for coin in coins {
            if coin.symbol == sym {
                var symbol = coin.symbol
                symbol = symbol.padding(toLength: 6, withPad: "0", startingAt: 0)
                let instance = "0"
                let check = "7"
                let security = "GNT" == symbol ? "U" : "0"
                let reserved = "0"
                return symbol + instance + check + "\n" + coin.type + coin.country + coin.entity + security + coin.date + coin.hash + reserved
            }
        }
        return nil
    }
    
    func decode(for sym:String) -> String? {
        var str = ""
        for coin in coins {
            if coin.symbol == sym {
                str.append("Name: \(coin.name)(\(coin.symbol))\n")
                if let type = kCoinTypes.someKey(forValue: coin.type) {
                    str.append("Type: \(type)\n")
                }
                if let country = kCountries.someKey(forValue: coin.country) {
                    str.append("Country: \(country)\n")
                }
                if let entity = kEntityTypes.someKey(forValue: coin.entity) {
                    str.append("Entity: \(entity)\n")
                }
                let i = coin.date.index(coin.date.startIndex, offsetBy: 1)
                let day = String(coin.date.prefix(upTo: i))
                let k = coin.date.index(coin.date.endIndex, offsetBy: -2)
                let year = String(coin.date.suffix(from: k))
                let start = coin.date.index(coin.date.startIndex, offsetBy: 1)
                let end = coin.date.index(coin.date.endIndex, offsetBy: -2)
                let range = start..<end
                let mo = coin.date.substring(with: range)
                let month = kDate.index(of: mo)
                if month != nil {
                    str.append("Date: \(month!)/\(day)/\(year)\n")
                }
                if let hash = kHashTypes.someKey(forValue: coin.hash) {
                    str.append("Type: \(hash)\n")           
                }   
                if coin.name == "Golem" {
                    str.append("Utility\n") 
                }
                return str
            }
        }
        return nil
    }
    
    
    func savePendingUpdate(symbol:String, cardIndex:String?, pars:Dictionary<String,Any>) {
        var data = UserDefaults.standard.object(forKey: "pendingUpdates") as? Array<Dictionary<String, Any>> ?? Array<Dictionary<String, Any>>()
        var copy = pars
        copy["symbol"] = symbol
        data.append(copy)
        UserDefaults.standard.set(data, forKey: "pendingUpdates")
    }
    //        let pars = ["updateType" : parameterToChange.rawValue, "actionType" : UpdateActionType.delete.rawValue, "value" : toSend] as [String : Any]
    
    func getPendingUpdate(symbol:String) -> Array<Dictionary<String, Any>> {
        let data = UserDefaults.standard.object(forKey: "pendingUpdates") as? Array<Dictionary<String, Any>> ?? Array<Dictionary<String, Any>>()
        var arr = Array<Dictionary<String, Any>>()
        for dict in data {
            if symbol == dict["symbol"] as? String {
                arr.append(dict)
            }
        }
        return arr
    }
    
    func getAllPendingUpdates() -> Array<Dictionary<String, Any>>? {
        return UserDefaults.standard.object(forKey: "pendingUpdates") as? Array<Dictionary<String, Any>>
    }
    
    func getElementIndex(symbol:String, mode:Int) -> Int? {
        var db = mode == 0 ? dataBase : upcomingICO
        for ico in db {
            if ico.symbol != nil, ico.symbol! == symbol {
                return db.index(of: ico)
            }
        }
        return nil 
    }
    
    func checkFakeNews(ico:String, index:String) -> Bool {
        var userdef = Dictionary<String, [String]>()
        if let ud = UserDefaults.standard.dictionary(forKey: "fakedNews") as? Dictionary<String, [String]> {
            userdef = ud
        }
        if let arr = userdef[ico] {
            if arr.contains(index) {
                return true
            }
        }
        return false
    }
    
    func saveFakeNews(ico:String, index:String) {
        var userdef = Dictionary<String, [String]>()
        if let ud = UserDefaults.standard.dictionary(forKey: "fakedNews") as? Dictionary<String, [String]> {
            userdef = ud
        }
        var arr = userdef[ico] ?? [String]()
        arr.append(index)
        userdef[ico] = arr
        UserDefaults.standard.set(userdef, forKey: "fakedNews")   
    }
}

extension Array where Element: Hashable {
    var orderedSet: Array  {
        var set = Set<Element>()
        return filter { set.insert($0).inserted }
    }
}

extension Dictionary where Value: Equatable {
    func someKey(forValue val: Value) -> Key? {
        return first(where: { $1 == val })?.key
    }
}

extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}
