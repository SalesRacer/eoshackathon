

import UIKit
import FSPagerView
import PopupDialog

enum PagerMode {
    case main
    case mainLimited
    case one
}

class PagerViewController: UIViewController, FSPagerViewDelegate, FSPagerViewDataSource {
    
    var mode = PagerMode.main
 
    @IBOutlet weak var pageView: FSPagerView! {
        didSet {
            self.pageView.register(UINib.init(nibName: "BelowPageView", bundle: nil), forCellWithReuseIdentifier:"BelowPageView")
        }
    }
    @IBOutlet weak var pageControl: FSPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageView.transformer = FSPagerViewTransformer(type: .cubic)
        pageView.itemSize = CGSize.init(width: pageView.frame.width, height: pageView.frame.height * 1.0)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reload), name: NSNotification.Name(rawValue: NotificationName.addFakeViews), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reload), name: NSNotification.Name(rawValue: NotificationName.reloadPager), object: nil)
    }
    
    @objc func reload(noti:NSNotification) {
        if let mo = noti.object as? PagerMode {
            mode = mo
        } else {
            if let sym = DataManager.℠.currentICO!.symbol {
                mode = DataManager.℠.code(for: sym) != nil ? .main : .mainLimited
            }
        } 
        pageControl.numberOfPages = self.numberOfItems(in: pageView)
        pageControl.currentPage = 0
        pageView.reloadData()
        pageView.scrollToItem(at: 0, animated: false)
    }
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        let stories = DataManager.℠.currentICO?.stories
        switch mode { case .main : return 4 + (stories != nil ? stories!.count : 0); case .mainLimited : return 4 + (stories != nil ? stories!.count : 0); case .one : return 4 }
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        pageControl.currentPage = targetIndex
        if AppManager.℠.tutorialCounter == 5 {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.5) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.closeFakeViews), object: nil)
                DataManager.℠.nextStepTutorial(toStart: 6)
            }
        }
    }
    
    func findLink(cell:BelowPageView) {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let range = NSRange(location: 0, length: cell.textView.text.count)
            let resultArray = detector.matches(in: cell.textView.text, options: [], range: range)
            for result in resultArray {
                if result.resultType == NSTextCheckingResult.CheckingType.link {
                    cell.link = result.url
                }
            } 
        } catch {
            
        }
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "BelowPageView", at: index) as! BelowPageView
        let currentICO = DataManager.℠.currentICO
        
        var underline = false
        cell.pencilView.alpha = 1
        cell.insertSubview(cell.bigTouchView, at:cell.subviews.count - 1)
        cell.insertSubview(cell.touchView, at:cell.subviews.count - 1) 
        let icoSymbol = currentICO?.symbol ?? ""
        
        let icoName = currentICO?.name ?? "ICO" + "(\(icoSymbol))"
        
        cell.touchView.addGestureRecognizer(cell.tap!)
        cell.tap!.isEnabled = true
        
        cell.bigTouchView.addGestureRecognizer(cell.tapBig!)
        cell.tapBig!.isEnabled = false
        cell.tapBigSimple!.isEnabled = false 
        
        if AppManager.℠.currentMode == .browse {
            var ind = index
//            if mode == .mainLimited, ind > 0 {
//                ind += 1
//            } else if index == 1 {
//                cell.tap!.isEnabled = false
//            }
            switch ind {
            case 0:
                cell.name.text = "Story"
                if let story = currentICO?.story {
                    cell.textView.text = story
                } else {
                    underline = true
                    cell.textView.text = "Click to suggest the short description for \(icoName) and get the bounty"
                }
                cell.tag = UpdateType.story.rawValue
//            case 1:
//                cell.name.text = "CUTIP"
//                if let sym = currentICO?.symbol, let cute = DataManager.℠.code(for: sym) {
//                    cell.textView.text = cute
//                } else {
//                    underline = false
//                    cell.textView.text = "- || -"
//                }
//                cell.tapBig!.isEnabled = true
//                cell.pencilView.alpha = 0
//                cell.tag = UpdateType.cutip.rawValue
            case 1:
                cell.name.text = "Industry"
                if let industry = currentICO?.industry {
                    cell.textView.text = industry
                } else {
                    underline = true
                    cell.textView.text = "Click to suggest the industry for \(icoName) and get the bounty"
                }
                cell.tapBig!.isEnabled = true
                cell.tag = UpdateType.industry.rawValue
            case 2:
                cell.name.text = "NAICS"
                if let industry = currentICO?.naics {
                    cell.textView.text = industry
                } else {
                    underline = true
                    cell.textView.text = "Click to suggest the NAICS for \(icoName) and get the bounty"
                }
                cell.tapBig!.isEnabled = true
                cell.tag = UpdateType.naics.rawValue 
            case 3:
                cell.name.text = "Tags"
                if let pin = currentICO?.categories, currentICO?.categories != "" {
                    cell.textView.text = pin
                } else {
                    underline = true
                    cell.textView.text = "Click to suggest the tags for \(icoName) and get the bounty"
                }
                cell.tag = UpdateType.pins.rawValue
            default:
                break
            }
            if ind > 3 {
                if let sto = currentICO?.stories {
                    let key = Array(sto.keys)[3 + sto.count - ind]
                    cell.name.text = key
                    cell.textView.text = sto[key]
                }
            }
        } else {
            let ind = DataManager.℠.oneCardTop?.cardIndex ?? 0
            guard (currentICO?.cards.count)! > 0 else { return cell }
            guard let card = currentICO?.cards[ind] else { return cell }
            let cardIndex = card.type.rawValue
            let aspectName = CardTypeText[cardIndex]!
            switch index {
            case 0:
                if var fake = card.fake {
                    cell.name.text = "Fake News! Reason"
                    if fake == "1" {
                        fake = "Reasonable explanation with link here"
                    }
                    cell.textView.text = fake 
                } else {
                    var addName:String?
                    if ind < 5, let str = UpdateTypeText[cardIndex], card.type != .rich { 
                        let i = str.index(str.endIndex, offsetBy: -5)
                        addName = String(str.prefix(upTo: i))
                    }
                    addName = addName ?? UpdateTypeText[cardIndex]
                    cell.name.text = addName! + " " + "Story"
                    if let story = card.story {
                        cell.textView.text = story
                    } else {
                        underline = true
                        cell.textView.text = "Click to suggest the short description for \(aspectName) for \(icoName) and get the bounty"
                    }
                    cell.tag = UpdateType.cardStory.rawValue
                }
            case 1:
                cell.name.text = "Tags"
                if let story = card.tags {
                    cell.textView.text = story
                } else {
                    underline = true
                    cell.textView.text = "Click to suggest the tags for \(aspectName) for \(icoName) and get the bounty"
                }
                cell.tag = UpdateType.cardTags.rawValue
            case 2:
                cell.name.text = "Pros"
                if let story = card.pros {
                    cell.textView.text = story
                } else {
                    underline = true
                    cell.textView.text = "Click to suggest the pros of \(aspectName) for \(icoName) and get the bounty"
                }
                cell.tag = UpdateType.cardPros.rawValue
            case 3:
                cell.name.text = "Cons"
                if let story = card.cons {
                    cell.textView.text = story
                } else {
                    underline = true
                    cell.textView.text = "Click to suggest the cons of \(aspectName) for \(icoName) and get the bounty"
                }
                cell.tag = UpdateType.cardCons.rawValue
            default:
                break
            }
        } 
        if underline {
            let paragraph = NSMutableParagraphStyle()
            paragraph.alignment = .center
            cell.textView.attributedText = NSAttributedString(string: cell.textView.text, attributes:
                [.underlineStyle: NSUnderlineStyle.single.rawValue, .foregroundColor : UIColor.lightText, .font : UIFont.init(name: "Futura", size: 14)!, .paragraphStyle: paragraph])
        } else {
            let paragraph = NSMutableParagraphStyle()
            paragraph.alignment = .center
            cell.textView.attributedText = NSAttributedString(string: cell.textView.text, attributes:
                [.underlineStyle: 0, .foregroundColor : UIColor.lightText, .font : UIFont.init(name: "Futura", size: 14)!, .paragraphStyle: paragraph])
        }
        cell.link = nil
        findLink(cell: cell)
        if nil != cell.link  {
            cell.tapBig!.isEnabled = true
        }
        if !cell.tapBig!.isEnabled {
            cell.bigTouchView.addGestureRecognizer(cell.tapBigSimple!)
            cell.tapBigSimple!.isEnabled = true
        }
        return cell
    }
      
}


class BelowPageView: FSPagerViewCell {
    @IBOutlet weak var edit: UIButton!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var textView: VerticallyCenteredTextView!
    var tap:UITapGestureRecognizer?
    var tapBig:UITapGestureRecognizer?
    var tapBigSimple:UITapGestureRecognizer?
    var link:URL?
    
    @IBOutlet weak var bigTouchView: UIView!
    @IBOutlet weak var touchView: UIView!
    @IBOutlet weak var pencilView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        tap = UITapGestureRecognizer.init(target: self, action: #selector(self.pressed(_:)))
        tapBigSimple = UITapGestureRecognizer.init(target: self, action: #selector(self.pressed(_:)))
        tapBig = UITapGestureRecognizer.init(target: self, action: #selector(self.openLink))
        
        self.layer.cornerRadius = 5
        backgroundColor = UIColor.init(red: 7/255.0, green: 15/255.0, blue: 72/255.0, alpha: 1)
        clipsToBounds = true
    }
    @objc @IBAction func pressed(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reportLink), object: self.tag) 
    }
    
    @objc func openLink() {
        if self.tag == UpdateType.cutip.rawValue {
            let ratingVC = CutipDescriptionView(nibName: "CutipDescriptionView", bundle: nil)
            let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
            ratingVC.name.text = "CUTIP Description (Work in progress)"
            ratingVC.caption.text = DataManager.℠.decode(for:(DataManager.℠.currentICO?.symbol)!) 
            let buttonThree = CancelButton(title: "CANCEL", dismissOnTap: true) {
            }
            popup.addButtons([buttonThree])
            AppManager.℠.mainVC!.present(popup, animated: true, completion: nil)
        }
        if self.tag == UpdateType.naics.rawValue {
            guard let naics = DataManager.℠.currentICO?.naics else { return }
            let read = DataManager.℠.symbolsOfNaics(naics)
            if read.count == 0 {
                return
            }
            let ratingVC = TableOfPopup(nibName: "TableOfPopup", bundle: nil)
            ratingVC.mode = .naics
            ratingVC.naics = naics
            ratingVC.industry = DataManager.℠.currentICO?.industry ?? ""
            if let ind = DataManager.℠.naicsData[naics] {
                ratingVC.industry = ind
            }
            // Create the dialog 
            let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
            let buttonOne = CancelButton(title: "CLOSE", height: 50) {
                
            }
            
            // Add buttons to dialog
            popup.addButtons([buttonOne])
            
            // Present dialog 
            AppManager.℠.mainVC!.present(popup, animated: true, completion: nil)
            return
        }
        if self.tag == UpdateType.industry.rawValue {
            let naics = DataManager.℠.currentICO?.naics ?? ""
            let ratingVC = TableOfPopup(nibName: "TableOfPopup", bundle: nil)
            ratingVC.mode = .industry
            ratingVC.naics = naics 
            ratingVC.industry = DataManager.℠.currentICO?.industry ?? ""
//            if let ind = DataManager.℠.naicsData[naics] {
//                ratingVC.industry = ind
//            }
            // Create the dialog
            let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
            let buttonOne = CancelButton(title: "CLOSE", height: 50) {
                
            }
            
            // Add buttons to dialog
            popup.addButtons([buttonOne])
            
            // Present dialog
            AppManager.℠.mainVC!.present(popup, animated: true, completion: nil)
            return
        }
        if nil != link {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.openLink), object: link?.absoluteString)
        }
    }
}

class VerticallyCenteredTextView: UITextView {
    override var contentSize: CGSize {
        didSet {
            var topCorrection = (bounds.size.height - contentSize.height * zoomScale) / 2.0
            topCorrection = max(0, topCorrection)
            contentInset = UIEdgeInsets(top: topCorrection, left: 0, bottom: 0, right: 0)
        }
    }
}
