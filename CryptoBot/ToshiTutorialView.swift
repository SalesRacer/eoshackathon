

import UIKit 

class ToshiTutorialView: UIViewController, UITextViewDelegate {

    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var caption: UILabel!
    
    var paperControl:PaperController!
    
    @IBOutlet weak var upHeight: NSLayoutConstraint!
    @IBOutlet weak var pasteStack: UIStackView!
    
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var downHeight: NSLayoutConstraint!
    @IBAction func sendWallet(_ sender: Any) {
    }
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var middleHeight: NSLayoutConstraint!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
                                  
    func tweak(mode:Int) {
        downHeight.constant = 0
        middleHeight.constant = 0
        upHeight.constant = 10
        imageHeight.constant = 300
        pasteStack.alpha = 0
        name.text = "Toshi Tutorial (\(mode + 1)/8)"
        switch mode {
        case 0:
            caption.text = "Here's the small tutorial that will help you connect to your wallet in the Toshi app to turn your points into tokens"
            upHeight.constant = 0
            imageHeight.constant = 0
        case 1:
            caption.text = "Download and install Toshi app"
            picture.image = UIImage.init(named: "toshi5")
        case 2:
            caption.text = "Click \"My Wallet\" button in Toshi"
            picture.image = UIImage.init(named: "toshi3")
        case 3:
            caption.text = "Press the (+) Add Custom Token button"
            picture.image = UIImage.init(named: "toshi3") 
        case 4:
            caption.text = "Paste this contract address: \n0x05a271f117c42d77f77b0e4491f603c0370806e9 \n(copied into clipboard)"
            UIPasteboard.general.string = "0x05a271f117c42d77f77b0e4491f603c0370806e9"    
            picture.image = UIImage.init(named: "toshi4")
        case 5:
            caption.text = "Click \"Receive\" in the Teracoin wallet you added to Toshi"
            picture.image = UIImage.init(named: "toshi1")
        case 6:
            caption.text = "Click the Copy button to copy your address"
            picture.image = UIImage.init(named: "toshi2")
        case 7:
            caption.text = "Paste the address to your profile in Coinswipe Fake Financial News or right here!"
            pasteStack.alpha = 1
            downHeight.constant = 10
            middleHeight.constant = 80
            imageHeight.constant = 0
            picture.image = nil
            picture.alpha = 0 
        default:
            break
        }
        self.updateViewConstraints()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        textView.text = ""
        textView.textColor = UIColor.darkText
        return true
    }

    @objc func endEditing() {
        view.endEditing(true)
    }
}

extension ToshiTutorialView: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing()
        return true
    }
}
