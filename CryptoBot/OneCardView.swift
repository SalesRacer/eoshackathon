

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import SafariServices
import Kingfisher
import SwiftyGif
import PopupDialog
import expanding_collection
import Cosmos

class OneCardView: UIView {
    
    enum CardType : Int {
        case profile = 0
        case tech
        case terms
        case roadmap 
        case market
        case white
        case rich
    }
    
    @IBOutlet weak var name: UILabel! 
    @IBOutlet weak var codeName: UILabel!
    @IBOutlet weak var inception: UILabel!
    @IBOutlet weak var image: UIImageView!
    weak var parentView: CardCollectionViewCell! 
    var currentICO: ICO?
    var currentType: CardType?
    @IBOutlet weak var botImage: UIImageView!
    @IBOutlet weak var iconMissed: UIButton!
    @IBOutlet weak var go: UIButton!
    @IBOutlet weak var botAspect: NSLayoutConstraint!
    @IBOutlet weak var cover: UIImageView!
    @IBOutlet weak var coverMissed: UIView!
    @IBOutlet weak var noInceptionView: UIView!
    @IBOutlet weak var noInceptionLabel: UILabel!
    @IBOutlet weak var addAspect: UIButton!
    @IBOutlet weak var openShevron: UIButton!
    @IBOutlet weak var upcoming: UIView!
    @IBOutlet weak var checkAspectsAll: UIImageView!
    @IBOutlet weak var rates: UILabel!
    @IBOutlet weak var ratesView: UIView!
    @IBOutlet weak var starsView: UIView!
    @IBOutlet weak var stars: CosmosView!
    @IBOutlet weak var advert: UILabel!
    @IBOutlet weak var expand: UIImageView!

    
    var images: Array<UIImage> = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.botTouched(_:)))
        botImage.addGestureRecognizer(tap)
        
        cover.alpha = 1
        botImage.alpha = 1
        addAspect.alpha = 0
        
        upcoming.layer.cornerRadius = 5
         
        NotificationCenter.default.addObserver(self, selector: #selector(self.additionalLoaded), name: NSNotification.Name(rawValue: NotificationName.additionalLoaded), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.additionalLoaded), name: NSNotification.Name(rawValue: NotificationName.additionalLoaded), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.ratingLoaded(noti:)), name: NSNotification.Name(rawValue: NotificationName.ratingLoaded), object: nil)
        
        glow() 
    }
    
    func glow() {
        UIView.animate(withDuration: 1, delay: 0, options: .allowAnimatedContent, animations:   {
            self.expand.alpha = 0.3
        }, completion: { (finished) in
            UIView.animate(withDuration: 1, delay: 0, options: .allowAnimatedContent, animations:   {
                self.expand.alpha = 1
            }, completion: { (finished) in
                UIView.animate(withDuration: 3, delay: 0, options: .allowAnimatedContent, animations:   {
                }, completion: { (finished) in
                    self.glow()
                })
            })
        })
    }
    
    @objc func additionalLoaded(noti:NSNotification) {
        if let ico = noti.object as? ICO {
            if ico == self.currentICO {
                self.fill()
            }
        }
    }
    
    @objc func ratingLoaded(noti:NSNotification) {
        if let ico = noti.object as? ICO {
            if ico.symbol == self.currentICO?.symbol {
                showRating()
            }
        }
    }
    
    func showRating() {
        guard let ico = self.currentICO else { return }
        let saved = UserDefaults.standard.object(forKey: K.ratingsReadyIco) as? [String] ?? [String]()
        if saved.contains((ico.symbol)!), let rate = DataManager.℠.loadOveRating(ico: (ico.name)!) {
            self.starsView.alpha = 1
            if rate == 1.0 {
                stars.rating = 5
            } else {
                stars.rating = 5 - (rate * 5)
            }
            if let def = ico.percentAll {
                rates.text = def 
                UIView.animate(withDuration: 0.5) {
                    self.ratesView.alpha = 0.7
                }
            }
        } else {
            self.starsView.alpha = 0
            ratesView.alpha = 0
        }
    }
    
    func df2so(_ price: Double) -> String{
        let numberFormatter = NumberFormatter()
        numberFormatter.groupingSeparator = ","
        numberFormatter.groupingSize = 3
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.decimalSeparator = "."
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 2
        return numberFormatter.string(from: price as NSNumber)!
    }
    
    @objc func fill() {
        DataManager.℠.performRealm {
            if let ico = currentICO {
                
                self.advert.text = "Tap to rate \(ico.symbol!)'s news" 
                if ico is UpcomingICO {
                    upcoming.alpha = 1
                } else {
                    upcoming.alpha = 0
                }
                if self.currentICO?.intro != nil || self.currentICO?.descript != nil || self.currentICO?.reason != nil { 
                    self.go.alpha = 1
                } else {
                    self.go.alpha = 0
                }
//                if ico.aspects?.count == 5 {
//                    checkAspectsAll.alpha = 1
//                } else {
//                    checkAspectsAll.alpha = 0
//                }
//                profilePlaceholder.alpha = 0
//                picture.alpha = 0
                cover.alpha = 1
                botImage.alpha = 1
                addAspect.alpha = 0.7 
                if let def = ico.percentAll {
                    rates.text = def
                } else { ratesView.alpha = 0 }
                let index = DataManager.℠.currentStacks.index(of: ico)
                if let white = ico.coverLink, let url = URL(string:white.trimmingCharacters(in: .whitespacesAndNewlines)) {
                    NetworkManager.℠.loadImage(path: url, view: self.cover)
                    self.coverMissed.alpha = 0
                    self.cover.alpha = 1
                }
                else {
                    self.coverMissed.alpha = 1
                    self.cover.alpha = 0
                }
                if let ind = index {
                    let i = ind + 1
                    if i < DataManager.℠.currentStacks.count {
                        let ico2 = DataManager.℠.currentStacks[i] 
                        if let white2 = ico2.coverLink, let url = URL(string:white2.trimmingCharacters(in: .whitespacesAndNewlines)) {
                            KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                                
                            })
                        }
                    }
                }
                var imagePath: String? 
                if ico.brandLogoUrl != nil && !ico.brandLogoUrl!.isEmpty {
                    imagePath = ico.brandLogoUrl
                } else if ico.coinLogoUrl != nil &&  !ico.coinLogoUrl!.isEmpty {
                    imagePath = ico.coinLogoUrl
                }
                
                image.image = nil
                if imagePath == nil {
                    iconMissed.alpha = 1
                }
                else if let path = imagePath, NetworkManager.℠.playSession != nil {
                    iconMissed.alpha = 0
                    let ready = path.addingPercentEncoding(withAllowedCharacters:.urlQueryAllowed)
                    image.kf.setImage(with: URL(string:ready!)!, placeholder: nil, options:nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
                        if self.iconMissed.alpha == 0 {
                            self.image.alpha = 1
                        } else {
                            self.image.alpha = 0     
                        }
                    })
                }
                
                if ico is UpcomingICO {
                    if ico is UpcomingICO {
                        if let def = (ico as? UpcomingICO)?.icoStart?.components(separatedBy: " ")[0], def != "0000-00-00", let def2 = (ico as? UpcomingICO)?.icoEnd?.components(separatedBy: " ")[0], def2 != "0000-00-00" {
                            inception.text = "Starts: \(def)\nEnds: \(def2)"
                            inception.alpha = 1
                            noInceptionView.alpha = 0
                        } else {
                            inception.alpha = 0
                            noInceptionView.alpha = 1
                            noInceptionLabel.text = "Suggest initial price" 
                        }
                    }
                } else {
                    inception.text = "Inception ➢ Yesterday 0.00x" 
                    
                    if ico.marketCap != nil, let doubl = Double(ico.marketCap!), doubl > 0 {
                        let amountString = df2so(doubl)
                        inception.text = "Market Cap ➢ $\(amountString)"
//                        let ince = ico.IRR().rounded(toPlaces: 2)
//                        if ince < 0.01 {
//                            inception.text = "Inception ➢ Yesterday < 0.01x"
//                        }
//                        else
//                            if ince < 1 {
//                                let formatter = NumberFormatter()
//                                formatter.minimumFractionDigits = 0
//                                formatter.maximumFractionDigits = 2
//                                formatter.decimalSeparator = "."
//                                if let form = formatter.string(from: NSNumber(value:ince)) {
//                                    inception.text = "Inception ➢ Yesterday 0\(form)x"
//                                }
//                            } else {
//                                let numberFormatter = NumberFormatter()
//                                numberFormatter.numberStyle = NumberFormatter.Style.decimal
//                                if let formattedNumber = numberFormatter.string(from: NSNumber(value:ico.IRR().rounded(toPlaces: 2))) {
//                                    inception.text = "Inception ➢ Yesterday \(formattedNumber)x"
//                                }
//                        }
                        inception.alpha = 1
                        noInceptionView.alpha = 0
                    } else {
                        inception.alpha = 0
                        noInceptionLabel.text = "Suggest initial price info"
                        noInceptionView.alpha = 1
                    }
                }
                showRating()
                
//                if contributePlaceholder.alpha > 0 {
//                    let tap = UITapGestureRecognizer.init(target: self, action: #selector(reportLink(_:)))
//                    contributePlaceholder.addGestureRecognizer(tap)
//                }
                if ico == DataManager.℠.currentICO {
                    DataManager.℠.oneCardBaseTop = self   
                }
            }
        }
        parentView.fill() 
    }
    
    @IBAction func botTouched(_ sender: UIButton) { 
        let ratingVC = SimplePopup(nibName: "SimplePopup", bundle: nil)
        let intro = (self.currentICO?.intro != nil ? (self.currentICO?.intro)! + "\n\n" : "")
        let desc = (self.currentICO?.descript != nil ? (self.currentICO?.descript)! + "\n\n" : "")
        let reason = self.currentICO?.reason != nil ? "Reason of allocation:\n" + (self.currentICO?.reason)! + "\n\n" : ""
        var cutip = ""
        if let sym = currentICO?.symbol, nil != DataManager.℠.code(for: sym) {
            cutip += "CUTIP Description (Work in progress)\n\n"
            cutip += DataManager.℠.decode(for:(DataManager.℠.currentICO?.symbol)!) ?? "" 
        }
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true) 
        ratingVC.textView.text = intro  + desc + reason + cutip
        let buttonTwo = DefaultButton(title: "OK", dismissOnTap: true) {
            
        }
        popup.addButtons([buttonTwo]) 
        AppManager.℠.mainVC!.present(popup, animated: true, completion: nil)
    }
    
    @objc func reportLink(_ sender: Any) {
        let ratingVC = SimplePopup(nibName: "SimplePopup", bundle: nil)
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .vertical, transitionStyle: .bounceDown, tapGestureDismissal: true)
        ratingVC.textHeight.constant = 30 
        ratingVC.titleLabel.text = "Add / Edit item"
        ratingVC.textView.text = "Please select the aspect to add / edit"
        let one = DefaultButton(title: "Cover", dismissOnTap: true) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reportLink), object: 333)
        }
        let two = DefaultButton(title: "Initial price", dismissOnTap: true) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reportLink), object: 444)
        }
        let three = DefaultButton(title: "Icon", dismissOnTap: true) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reportLink), object: 111)
        }
        let five = DefaultButton(title: "Add News", dismissOnTap: true) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reportLink), object: 5556)
        }
        let four = CancelButton(title: "OK", dismissOnTap: true) {
            
        }
        popup.addButtons([one, two, three, five, four])
        AppManager.℠.mainVC!.present(popup, animated: true, completion: nil)
    }
    
    @IBAction func openShev(_ sender: Any) {
        DataManager.℠.nextStepTutorial(toStart: 1)
        parentView.cellIsOpen(!parentView.isOpened)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addFakeViews"), object:PagerMode.one) 
    }
    
    func openLink(link:String) { 
        let svc = SFSafariViewController(url: URL(string:link)!)
        if let topController = UIApplication.topViewController() {
            topController.present(svc, animated: true, completion: nil)
        }
    }
    
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

class SwipeButton: UIButton {
    //    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    //        super.touchesEnded(touches, with: event)
    //    }
    //    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    //        super.touchesBegan(touches, with: event)
    //    }
}
