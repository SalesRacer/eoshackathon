import UIKit
import Presentation
import paper_onboarding
import SwiftMessages

class ParallaxController: PresentationController, PaperOnboardingDataSource, PaperOnboardingDelegate {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.view.backgroundColor = UIColor.white
        //        self.configureSlides()
        
        let onboarding = PaperOnboarding.init()
        onboarding.dataSource = self
        onboarding.delegate = self
        onboarding.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        view.addSubview(onboarding)
        let skip = UIButton.init(frame: CGRect.init(x: view.frame.size.width - 100, y: 0, width: 100, height: 100))
        skip.setTitle("Skip", for: UIControl.State.normal)
        skip.titleLabel?.font = UIFont.init(name: "Futura", size: 13)
        skip.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        skip.addTarget(self, action: #selector(hide), for: UIControl.Event.touchUpInside)
        if !NetworkManager.℠.isOffline {
            view.addSubview(skip)
        }
        
        // add constraints
        for attribute: NSLayoutConstraint.Attribute in [.left, .right, .top, .bottom] {
            let constraint = NSLayoutConstraint(item: onboarding,
                                                attribute: attribute,
                                                relatedBy: .equal,
                                                toItem: view,
                                                attribute: attribute,
                                                multiplier: 1,
                                                constant: 0)
            view.addConstraint(constraint)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.hide), name: NSNotification.Name(rawValue: "hide"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showOffline()

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func onboardingItem(at index: Int) -> OnboardingItemInfo {
        let font1 = UIFont.init(name: "Futura", size: 23)!
        let font2 = UIFont.init(name: "Futura", size: 21)!
        return [
            OnboardingItemInfo.init(informationImage: UIImage.init(named: "logo")!, title: "Welcome to", description: "CoinSwipe™ \nFake Financial News Mint", pageIcon: UIImage.init(named: "coin")!, color: kOrangeColor, titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: font1, descriptionFont: font2),
            OnboardingItemInfo.init(informationImage: UIImage.init(named: "intro1")!, title: " ", description: "Fake financial news kills trillions of dollars", pageIcon: UIImage.init(named: "coin")!, color: kOrangeColor, titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: font1, descriptionFont: font2),
            OnboardingItemInfo.init(informationImage: UIImage.init(named: "intro2")!, title: " ", description: "Find fake ICO news and help great projects grow", pageIcon: UIImage.init(named: "coin")!, color: kOrangeColor, titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: font1, descriptionFont: font2),
            OnboardingItemInfo.init(informationImage: UIImage.init(named: "intro3")!, title: " ", description: "Win tokens from the crypto community", pageIcon: UIImage.init(named: "coin")!, color: kOrangeColor, titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: font1, descriptionFont: font2),
            OnboardingItemInfo.init(informationImage: UIImage.init(named: "logo")!, title: " ", description: "Let's get started!", pageIcon: UIImage.init(named: "coin")!, color: kOrangeColor, titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: font1, descriptionFont: font2)][index] as! OnboardingItemInfo
    }
    
    func showOffline() {
        if NetworkManager.℠.isOffline {
            var config = SwiftMessages.Config()
            config.presentationStyle = .center
            config.presentationContext = .window(windowLevel: UIWindow.Level.alert)
            config.duration = .seconds(seconds: 10.5)
            config.dimMode = .gray(interactive: true)
            config.interactiveHide = false
            config.preferredStatusBarStyle = .lightContent
            config.eventListeners.append() { event in
                if case .didHide = event { print("yep") }
            }
            let view = MessageView.viewFromNib(layout: .cardView)
            view.configureTheme(.warning)
            view.configureDropShadow()
            let iconText = "❕"
            if !NetworkManager.℠.offlineReason {
                view.configureContent(title: "The app is offline", body: "Please connect to the internet and try again later", iconImage: nil, iconText: iconText, buttonImage: nil, buttonTitle: "OK") { (button) in
                    SwiftMessages.hide()
                }
            } else {
                view.configureContent(title: "The server is offline", body: "Maintenance is in process. Please try again later", iconImage: nil, iconText: iconText, buttonImage: nil, buttonTitle: "OK") { (button) in
                    SwiftMessages.hide()
                }
            }
            SwiftMessages.show(config: config, view: view)
        }
    }
    
    func onboardingItemsCount() -> Int {
        return 5
    }
    
    func onboardingDidTransitonToIndex(_ index: Int) {
        if index == 4 && !NetworkManager.℠.isOffline {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "hide"), object: nil)
            }
        }
    }
    
    func onboardingConfigurationItem(_ item: OnboardingContentViewItem, index: Int) {
        item.backgroundColor = UIColor.clear
        //    item.titleLabel?.backgroundColor = .redColor()
        //    item.descriptionLabel?.backgroundColor = .redColor()
        //    item.imageView = ...
    }
    
    func onboardingWillTransitonToIndex(_ index: Int) {
        showOffline()
    }
    
    var enableTapsOnPageControl: Bool { return true }
    
    private func configureSlides() {
        let storyboard = UIStoryboard(name: "Preview", bundle: nil)
        var vcs: [UIViewController] = []
        for i in 0...4 {
            vcs.append(storyboard.instantiateViewController(withIdentifier :"VC\(i+1)"))
        }
        self.add(vcs)
    }
    
    @objc func hide() {
        completeHide()
    }
    
    func completeHide() {
//        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            self.dismiss(animated: true) { 
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.skippedIntro), object: nil)
            }
        }
//    }
    
}


class EndViewController: UIViewController {
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "hide"), object: nil)
        }
    }
}

class StartViewController: UIViewController {
    @IBOutlet weak var bottomMessage: UILabel!
    @IBAction func skip(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "hide"), object: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomMessage.text = "Initialization ..."
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeText), name: NSNotification.Name(rawValue: NotificationName.reloadStacks), object: nil)
    }
    
    @objc func changeText() {
        bottomMessage.text = "Swipe to continue >>>"
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationName.reloadStacks), object: nil)
    }
}
