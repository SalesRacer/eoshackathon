
import Foundation
import UIKit

let kBlueColor = UIColor.init(red: 60/255, green: 130/255, blue: 230/255, alpha: 1)
let kOrangeColor = UIColor.init(red: 255/255, green: 120/255, blue: 0/255, alpha: 1) 
let kServerAddress =  "https://stage.coinswipe.io"//https://coinswipe.terabot.io"///https://coinswipe.terabot.io"//"https://api.coinswipe.io"// // //"https://stage.spreadbot.com" //

struct K {
    static let ratingsReadyIco = "ratingsReadyIco"   
}

struct NotificationName { 
    static let scrollToStack = "scrollToStack"
    static let scrollToItem = "scrollToItem"
    static let reloadStacks = "reloadStacks"
    static let updateCurrentICO = "updateCurrentICO" 
    static let menuClick = "menuClick"
    static let reloadMode = "reloadMode"
    static let botTouched = "botTouched" 
    static let paintIt = "paintIt"
    static let scrolledToIndex = "scrolledToIndex"
    static let playSession = "playSession"
    static let additionalLoaded = "additionalLoaded"
    static let stackOpened = "stackOpened" 
    
    static let showSlides = "showSlides"
    static let reportLink = "reportLink"
    static let showReport = "showReport"
    static let showDetails = "showDetails"
    static let pulsePoints = "pulsePoints"
    static let showInitialLoading = "showInitialLoading"
    static let addCongrats = "addCongrats"                                                                                                                      
    static let showTutorial = "showTutorial"
    static let dimDetails = "dimDetails"
    static let copyClip = "copyClip"
    static let unsubKeyboard = "unsubKeyboard"
    static let showLogin = "showLogin"
    static let showEmailLogin = "showEmailLogin"
    static let stopTutorial = "stopTutorial"
    static let updateAfterLogin = "updateAfterLogin"   
    static let draggedCard = "draggedCard"
    static let switchScroll = "switchScroll"
    static let openLink = "openLink"
    static let changeBack = "changeBack"
    static let openShevron = "openShevron"
    static let addFakeViews = "addFakeViews"
    static let closeFakeViews = "closeFakeViews"
    static let startGif = "startGif"
    static let stopGif = "stopGif"
    static let distanceMoveTo = "distanceMoveTo"
    static let fillSuggestions = "fillSuggestions"
    static let ratingLoaded = "ratingLoaded"
    static let reloadPager = "reloadPager"
    static let startShowcase = "startShowcase"
    static let skippedIntro = "skippedIntro"            
    
     
}

let kOpenTokenActivity = "com.coinswipe.openToken" 



struct PointsBag {
    var points: Int 
    var type: Int?
    //LEGACY(0, "Legacy"),
    //RATE(1, "for Rate"),
    //UPDATE(2, "for Update"),
    //REFERRAL(3, "for Referral"); 
    var symbol: String?
    var card: String?
}
                                                      
struct SlideshowControls {
    enum slidePlayTag: Int {
        case play = 0
        case stop = 1
    }
}

enum PushResponse: Int {
    case none = 0
    case info
    case updateAccept
    case updateRefuse
}

enum SortingStyle {
    case price
    case alpha
    case filled
}

enum UpdateType: Int { 
    case profile = 0
    case tech = 1
    case terms = 2
    case roadmap = 3
    case market = 4
    case whitepaper = 5
    case rich = 6
    case news = 7
    case story = 1005
    case cutip = 19899
    case naics = 1006
    case pins = 1007
    case intro = 1008
    case description = 1009
    case industry = 1010
    case reason = 1011
    case icon = 2000
    case initialPrice = 2001
    case cover = 2002
    case cardStory = 3001
    case cardTags = 3002
    case cardPros = 3004
    case cardCons = 3003
}




let UpdateTypeText: Dictionary<Int, String> =
    [UpdateType.profile.rawValue : "Team info",
     UpdateType.tech.rawValue : "Tech info",
     UpdateType.terms.rawValue : "Terms info",
     UpdateType.roadmap.rawValue : "Time info",
     UpdateType.market.rawValue : "Market info",
     UpdateType.whitepaper.rawValue : "Whitepaper", 
     UpdateType.rich.rawValue : "Article",
     UpdateType.story.rawValue : "Story",
     UpdateType.naics.rawValue : "NAICS",
     UpdateType.pins.rawValue : "Tags",
     UpdateType.intro.rawValue : "Intro",
     UpdateType.description.rawValue : "Description",
     UpdateType.industry.rawValue : "Industry",
     UpdateType.reason.rawValue : "Reason",
     UpdateType.icon.rawValue : "Icon",
     UpdateType.initialPrice.rawValue : "Price",
     UpdateType.cover.rawValue : "Cover",
     UpdateType.cardStory.rawValue : "Story",
     UpdateType.cardTags.rawValue : "Tags",
     UpdateType.cardCons.rawValue : "Cons",
     UpdateType.cardPros.rawValue : "Pros"]


struct ScreenSize {
    static let width = UIScreen.main.bounds.size.width
    static let height = UIScreen.main.bounds.size.height
    static let frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: ScreenSize.height)
    static let maxWH = max(ScreenSize.width, ScreenSize.height)
} 

struct DeviceType {
    static let iPhone4orLess  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH < 568.0
    static let iPhone5orSE    = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 568.0
    static let iPhone678      = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 667.0
    static let iPhone678p     = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 736.0
    static let iPhoneX        = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 812.0
    static let IS_IPAD             = UIDevice.current.userInterfaceIdiom == .pad
}


