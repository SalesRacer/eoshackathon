

import Foundation
import UIKit
import RealmSwift 

struct Aspect {
    enum quality {
        case trust(Int)
        case bad(Int)
        case none(Int)
        func evaluate() -> Int {
            switch self {
            case let .trust(value),
                 let .bad(value),
                 let .none(value):
                return value
            }
        }
        static func ==(lhs: Aspect.quality, rhs: Aspect.quality) -> Bool {
            switch (lhs, rhs) {
            case (let .none(code1), let .none(code2)):
                return code1 == code2
            case (let .bad(code1), let .bad(code2)):
                return code1 == code2
            case (let .trust(code1), let .trust(code2)):
                return code1 == code2
                
            default:
                return false
            }
        }
    }
    static let names = ["Team", "Tech", "Terms", "Time", "TAM (Market Size)", " "]
}

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }
        
        return array 
    }
}

class ICO : Object {
    
//    @objc dynamic//
   @objc dynamic  var indexRealm: Int = 0
//    @objc dynamic var brandLogo: String? = nil
//    @objc dynamic var circulatingSupply: String? = nil
//    @objc dynamic var coinLogo: String? = nil
//    @objc dynamic var domain: String? = nil
//    @objc dynamic var downloadFrom: String? = nil
//    @objc dynamic var googleSearchItem: String? = nil
//    @objc dynamic var grow1h: String? = nil
//    @objc dynamic var grow24h: String? = nil
//    @objc dynamic var grow7d: String? = nil
//    @objc dynamic var id: String? = nil
    @objc dynamic var marketCap: String? = nil
//    @objc dynamic                              
   @objc dynamic  var name: String? = nil
//    @objc dynamic
   @objc dynamic  var price: String? = nil
//    @objc dynamic
   @objc dynamic  var symbol: String? = nil
//    @objc dynamic var twitter: String? = nil
//    @objc dynamic var volume24: String? = nil
//    @objc dynamic
   @objc dynamic  var whitePaperURL: String? = nil
//    @objc dynamic
   @objc dynamic  var coverLink: String? = nil
//    @objc dynamic
   @objc dynamic  var brandLogoUrl: String? = nil
//    @objc dynamic
   @objc dynamic  var coinLogoUrl: String? = nil
//    @objc dynamic
   @objc dynamic  var initialPrice: String? = nil
//    @objc dynamic var onDayRate: String?
//    @objc dynamic
    @objc dynamic var favorited: Bool = false
//    @objc dynamic
   @objc dynamic  var loaded: Bool = false
    var stories: Dictionary<String,String>? = nil
//    @objc dynamic 
    @objc dynamic var existsDataCount: Int = 0
    
    @objc dynamic var roadmapImages: String? = nil
    @objc dynamic var linkedProfiles: String? = nil
    @objc dynamic var linkedinProfiles: String? = nil
    @objc dynamic var techStackImages: String? = nil
    @objc dynamic var stats: String? = nil
    @objc dynamic var terms: String? = nil
//    @objc dynamic
    var cards = [Card]()
    @objc dynamic var categories: String = ""
    @objc dynamic var story: String? = nil
    @objc dynamic var naics: String? = nil
    @objc dynamic var intro: String? = nil
    @objc dynamic var descript: String? = nil
    @objc dynamic var industry: String? = nil
    @objc dynamic var reason: String? = nil
    
    
    let aspectRoadmap = RealmOptional<Int>()
    let aspectProfiles = RealmOptional<Int>()
    let aspectTechStack = RealmOptional<Int>()
    let aspectStats = RealmOptional<Int>()
    let aspectTerms = RealmOptional<Int>()
    
    //stupid
//    @objc dynamic var percentTeam: String? = nil
//    @objc dynamic var percentTech: String? = nil
//    @objc dynamic var percentTerms: String? = nil
//    @objc dynamic var percentTime: String? = nil
//    @objc dynamic var percentTam: String? = nil
//    @objc dynamic
    @objc dynamic var percentAll: String? = nil
//    @objc dynamic
    
    
//    @objc dynamic
    @objc dynamic var profileOneLink: String? = nil
//    @objc dynamic
    @objc dynamic var profileTwoLink: String? = nil
    
   var aspects: Dictionary<String, Int>? = nil 
    
    //    enum propertyNames : String {
    //
    //    }
    
    override static func primaryKey() -> String? {
        return "symbol"
    }

    override static func ignoredProperties() -> [String] {
        return ["aspects", "loaded", "whitePaperURL", "coverLink", "brandLogoUrl", "coinLogoUrl", "initialPrice", "loaded", "existsDataCount", "roadmapImages", "linkedProfiles", "linkedinProfiles", "techStackImages", "stats", "terms", "cards", "categories", "story", "naics", "intro", "descript", "industry", "reason", "aspectRoadmap", "aspectProfiles", "aspectTechStack", "aspectStats", "aspectTerms", "percentAll", "profileOneLink", "profileTwoLink"]
    }

    
    func IRR() -> Double {
        guard let priceVal = price, let dayOne = self.initialPrice
            else { return 0.0 }
        if let dayOneDouble = Double(dayOne) {
            if let doublePrice = Double(priceVal) {
                return Double(doublePrice) / Double(dayOneDouble)
            }
        }
        return 0.0
    }
}


enum UpdateActionType : Int {
    case edit = 0
    case add
    case delete
    case create
}

enum CardType : Int {
    case profile = 0
    case tech
    case terms
    case roadmap
    case market
    case white
    case rich
    case news 
    case cover = 333
}

let CardTypeText: Dictionary<Int, String> =
    [CardType.profile.rawValue : "Team info",
     CardType.tech.rawValue : "Tech info",
     CardType.terms.rawValue : "Terms info",
     CardType.roadmap.rawValue : "Time info",
     CardType.market.rawValue : "Market info",
     CardType.cover.rawValue : "Cover",
     CardType.white.rawValue : "Whitepaper",
     CardType.rich.rawValue : "News",
     CardType.news.rawValue : "News"] 

class Card {
    var type: CardType = .rich
    var index: String? = nil
    var links = [String]()
    var story: String?
    var tags: String?
    var pros: String?
    var cons: String?
    var fake: String?
}

class NewsCard: Card {
    var source: String?
    var imageUrl: String?
    var title: String?
    var description: String?
    var date: String? 
}


struct Rank {
    var name: String!
    var multi: Double!
    var points: Int!
}
                                          

class UpcomingICO: ICO {
//    @objc dynamic
    @objc dynamic var preIcoStart: String? = nil
//    @objc dynamic
    @objc dynamic var preIcoEnd: String? = nil
//    @objc dynamic
    @objc dynamic var icoStart: String? = nil
//    @objc dynamic
    @objc dynamic var icoEnd: String? = nil
}


                                                                                      



struct Coin {
    var name = ""
    var symbol = ""
    var type = ""
    var country = ""
    var entity = ""
    var date = ""
    var hash = ""
    var contract = ""
}

let kCoinTypes = ["PoW" : "1",
                  "PoW/PoS" : "2",
                  "PoS" : "3",
                  "PoI" : "4",
                  "PoS/LPoS" : "5",
                  "ERC20" : "6",
                  "DPoS" : "7",
                  "dBFT" : "8",
                  "PoR" : "9",
                  "PoPP" : "A",
                  "PoA" : "B",
                  "dPoW/PoW" : "C",
                  "Waves" : "D",
                  "BitShares" : "E",
                  "Pow/PoSC" : "F",
                  "PoM" : "G"]

let kCountries = ["US" : "US",
                  "Canada" : "CA",
                  "Japan" : "JP",
                  "Cayman Islands" : "KY",
                  "Switzerland" : "SZ", 
                  "UK" : "UK",
                  "Liechtenstein" : "LI",
                  "China" : "CN",
                  "Germany" : "DE",
                  "Seychelles" : "SC",
                  "Poland" : "PL",
                  "Singapore" : "SG",
                  "New Zealand" : "NZ",
                  "France" : "FR",
                  "Netherlands" : "NL",
                  "Dubai" : "AE",
                  "Russia" : "RU",
                  "Gibraltar" : "GI",
                  "Denmark" : "DK",
                  "Spain" : "ES",
                  "Israel" : "IL",
                  "Mexico" : "MX",
                  "Australia" : "AU",
                  "Argentina" : "AR", 
                  "Estonia" : "EE",
                  "Sweden" : "SE",
                  "Slovenia" : "SI",
                  "Belize" : "BZ" 
]

let kHashTypes = ["sha256" : "1",
                  "x11" : "2",
                  "sha512" : "3",
                  "scrypt" : "4",
                  "PoS" : "5",
                  "ethash" : "6",
                  "x13" : "7",
                  "Blake2b" : "8", 
                  "dpos" : "9",
                  "pos3" : "A",
                  "SigHash" : "B",
                  "cryptonightLite" : "C",
                  "sha256ripemd160" : "D",
    "smartcontract" : "E",
    "Equihash" : "F",
    "Lyra2RE" : "G",
    "sha3" : "H",
    "Arbitrage" : "J",
    "NeoScrypt" : "K",
    "sha3-512" : "L"]

let kEntityTypes = ["Individual" : "1",
                    "Corporation" : "2",
                    "Group" : "3",
                    "Company" : "4",
                    "Foundation" : "5",
                    "DAO" : "6",
                    "Limited company" : "7",
                    "Stock" : "8",
                    "NonProfit" : "9",
                    "A" : "pos3",]

let kDate = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",  "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]    
