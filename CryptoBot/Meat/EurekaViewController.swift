
import Foundation
import UIKit
import Eureka
import SwiftMessages

enum EurekaType: Int {
    case creation = 0
    case about
    case problem
    case link
}

class EurekaViewController: FormViewController {
    
    var type: EurekaType = .creation
    var text: String = ""
    var created: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if created {
            return
        }
        created = true
        if type == .creation {
            form +++
                Section("Name")
                <<< TextRow() { row in
                    row.title = "ICO Ticker"
                    row.placeholder = "Enter name here"
                    }.cellSetup({ (cell, row) in
                        self.updateLayer(cell.layer)
                        cell.titleLabel?.font = UIFont(name: "Futura", size: 13)
                    }).cellUpdate({ (cell, row) in
                        cell.textField?.font = UIFont(name: "Futura", size: 11)
                    })
                +++ self.createMulti(headerText: "Linked profiles", footerText: "Add the links to the team member profiles.", rowText: "Add New Profile")
                +++ self.createMulti(headerText: "Roadmap image", footerText: "Add the links to the roadmaps.", rowText: "Add New Link")
                +++ self.createMulti(headerText: "ICO terms", footerText: "Add the links to the terms.", rowText: "Add New Term")
                +++ self.createMulti(headerText: "Tech stack + diagrams", footerText: "Add the links to the tech data.", rowText: "Add New Tech")
                +++ self.createMulti(headerText: "Market size and stats", footerText: "Add the links to the TAM.", rowText: "Add New Link")
                <<< ButtonRow() { row in
                    row.title = "Send (Coming soon)"
                    }.cellSetup({ (cell, row) in
                        self.updateLayer(cell.layer)
                        cell.textLabel?.font = UIFont(name: "Futura", size: 15)
                        cell.textLabel?.textColor = UIColor.lightGray
                        cell.textLabel?.numberOfLines = 0
                    })
        } else
            if type == .about {
                form +++
                    Section("About")
                    <<< LabelRow() { row in
                        row.title = "https://icons8.com/"
                        }.cellSetup({ (cell, row) in
                            self.updateLayer(cell.layer)
                            cell.textLabel?.font = UIFont(name: "Futura", size: 13)
                            cell.textLabel?.numberOfLines = 0
                        })
                    <<< LabelRow() { row in
                        row.title = "https://coinmarketcap.com/"
                        }.cellSetup({ (cell, row) in
                            self.updateLayer(cell.layer)
                            cell.textLabel?.font = UIFont(name: "Futura", size: 13)
                            cell.textLabel?.numberOfLines = 0
                        })
                    <<< LabelRow() { row in
                        row.title = "https://github.com/Alamofire/Alamofire/blob/master/LICENSE"
                        }.cellSetup({ (cell, row) in
                            self.updateLayer(cell.layer)
                            cell.textLabel?.font = UIFont(name: "Futura", size: 13)
                            cell.textLabel?.numberOfLines = 0
                        })
                    <<< LabelRow() { row in
                        row.title = "https://github.com/xmartlabs/Eureka/blob/master/LICENSE"
                        }.cellSetup({ (cell, row) in
                            self.updateLayer(cell.layer)
                            cell.textLabel?.font = UIFont(name: "Futura", size: 13)
                            cell.textLabel?.numberOfLines = 0
                        })
                    <<< LabelRow() { row in
                        row.title = "https://github.com/Yalantis/Koloda/blob/master/LICENSE"
                        }.cellSetup({ (cell, row) in
                            self.updateLayer(cell.layer)
                            cell.textLabel?.font = UIFont(name: "Futura", size: 13)
                            cell.textLabel?.numberOfLines = 0
                        })
                    <<< LabelRow() { row in
                        row.title = "https://github.com/realm/realm-cocoa/blob/master/LICENSE"
                        }.cellSetup({ (cell, row) in
                            self.updateLayer(cell.layer)
                            cell.textLabel?.font = UIFont(name: "Futura", size: 13)
                            cell.textLabel?.numberOfLines = 0
                        })
                    <<< LabelRow() { row in
                        row.title = "https://github.com/jonkykong/SideMenu/blob/master/LICENSE"
                        }.cellSetup({ (cell, row) in
                            self.updateLayer(cell.layer)
                            cell.textLabel?.font = UIFont(name: "Futura", size: 13)
                            cell.textLabel?.numberOfLines = 0
                        })
                    <<< LabelRow() { row in
                        row.title = "https://github.com/onevcat/Kingfisher"
                        }.cellSetup({ (cell, row) in
                            self.updateLayer(cell.layer)
                            cell.textLabel?.font = UIFont(name: "Futura", size: 13)
                            cell.textLabel?.numberOfLines = 0
                        })
                    <<< LabelRow() { row in
                        row.title = "https://www.intercom.com/"
                        }.cellSetup({ (cell, row) in
                            self.updateLayer(cell.layer)
                            cell.textLabel?.font = UIFont(name: "Futura", size: 13)
                            cell.textLabel?.numberOfLines = 0
                        })
                    <<< LabelRow() { row in
                        row.title = "https://github.com/kciter/Floaty"
                        }.cellSetup({ (cell, row) in
                            self.updateLayer(cell.layer)
                            cell.textLabel?.font = UIFont(name: "Futura", size: 13)
                            cell.textLabel?.numberOfLines = 0
                        })
                    <<< LabelRow() { row in
                        row.title = "https://github.com/Orderella/PopupDialog"
                        }.cellSetup({ (cell, row) in
                            self.updateLayer(cell.layer)
                            cell.textLabel?.font = UIFont(name: "Futura", size: 13)
                            cell.textLabel?.numberOfLines = 0
                        })
            } else
                if type == .problem {
                    form +++
                        Section("Report an error") 
                        <<< TextRow() { row in
                            row.title = "ICO Ticker"
                            row.placeholder = DataManager.℠.currentICO?.symbol
                            }.cellSetup({ (cell, row) in
                                self.updateLayer(cell.layer)
                                cell.titleLabel?.font = UIFont(name: "Futura", size: 13)
                            }).cellUpdate({ (cell, row) in
                                cell.textField?.font = UIFont(name: "Futura", size: 11)
                            })
                        <<< TextAreaRow() { row in
                            row.title = " "
                            row.placeholder = "Please describe the problem here" 
                            }.cellSetup({ (cell, row) in
                                self.updateLayer(cell.layer)
                                cell.textView?.font = UIFont(name: "Futura", size: 11)
                            }).cellUpdate({ (cell, row) in
                                cell.textView?.font = UIFont(name: "Futura", size: 13)
                            })
                        <<< ButtonRow() { row in
                            row.title = "Send"
                            }.cellSetup({ (cell, row) in
                                self.updateLayer(cell.layer)
                                cell.textLabel?.font = UIFont(name: "Futura", size: 15)
                                cell.textLabel?.textColor = UIColor.blue
                                cell.textLabel?.numberOfLines = 0
                                let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.sendReport))
                                cell.addGestureRecognizer(tap)
                            })
                }  else
                    if type == .link {
                        form +++
                            Section(" ")
                            <<< LabelRow() { row in
                                row.title = text
                                }.cellSetup({ (cell, row) in
//                                    self.updateLayer(cell.layer)
                                    cell.textLabel?.font = UIFont(name: "Futura", size: 13)
                                    cell.textLabel?.numberOfLines = 0
                                })
                            <<< TextAreaRow() { row in
                                row.title = " "
                                row.placeholder = "Please copy the link here"
                                }.cellSetup({ (cell, row) in
                                    self.updateLayer(cell.layer)
                                    cell.textView?.font = UIFont(name: "Futura", size: 11)
                                }).cellUpdate({ (cell, row) in
                                    cell.textView?.font = UIFont(name: "Futura", size: 13)
                                })
                            <<< ButtonRow() { row in
                                row.title = "Send"
                                }.cellSetup({ (cell, row) in
                                    self.updateLayer(cell.layer)
                                    cell.textLabel?.font = UIFont(name: "Futura", size: 15)
                                    cell.textLabel?.textColor = UIColor.blue
                                    cell.textLabel?.numberOfLines = 0
                                    let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.sendLink))
                                    cell.addGestureRecognizer(tap)
                                })
        }
    }
    
    func createMulti(headerText:String, footerText:String, rowText:String) -> MultivaluedSection {
        return MultivaluedSection(multivaluedOptions: [.Insert, .Delete],
                                  header: headerText,
                                  footer: footerText) {
                                    $0.addButtonProvider = { section in
                                        return ButtonRow(){
                                            $0.title = rowText
                                            }.cellSetup({ (cell, row) in
                                                self.updateLayer(cell.layer)
                                                cell.textLabel?.font = UIFont(name: "Futura", size: 13)
                                            })
                                    }
                                    $0.multivaluedRowToInsertAt = { index in
                                        return URLRow() {
                                            $0.placeholder = " "
                                            }.cellSetup({ (cell, row) in
                                                self.updateLayer(cell.layer)
                                                cell.titleLabel?.font = UIFont(name: "Futura", size: 13)
                                            }).cellUpdate({ (cell, row) in
                                                cell.textField?.font = UIFont(name: "Futura", size: 11)
                                            })
                                    }
        }
    }
    
    func updateLayer(_ layer:CALayer) {
        layer.borderColor = UIColor.clear.cgColor
        layer.cornerRadius = 10
        layer.borderWidth = 5
    }
    
    @objc func sendReport() {
        guard let row = form.allRows[0] as? TextRow, let name = row.placeholder else {
            self.showMessage("Please input the ICO name", style: .warning)
            return
        }
        guard let text = form.allRows[1].baseValue as? String, !text.isEmpty else {
            self.showMessage("Please input the report text", style: .warning)
            return
        }
        NetworkManager.℠.report(ico: name, text: text) { (data) in
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.pulsePoints), object: 10) 
            self.showMessage("The report has been sent", style: .success)
            self.dismiss(animated: true, completion: nil) 
        } 
    }
    
    @objc func sendLink() {
//        guard let name = form.allRows[0].title as? String, !name.isEmpty else { return }
        //        gua
        //
        //       }rd let text = form.allRows[1].baseValue as? String, !text.isEmpty else {
        //            self.showMessage("Please input the link", style: .warning)
        //            return
//        var pars = ["icon" : "", "initialPrice" : "", "cover" : "", "aspectTime" : [], "aspectTech" : [], "aspectTeam" : "", "aspectTerms" : [], "aspectTam" : []] as! Dictionary<String,Any>
//        if let par = NetworkManager.℠.parameterToChange, (par == "icon" || par == "initialPrice" || par == "cover" || par == "aspectTeam" || par == "price") {
//            pars.merge([NetworkManager.℠.parameterToChange! : text]) { (first, second) -> Any in
//                return second
//            }
//        } else {
//            pars.merge([NetworkManager.℠.parameterToChange! : [text]]) { (first, second) -> Any in
//                return second
//            }
//        }
//
//        NetworkManager.℠.updateLink(ico: DataManager.℠.currentICO!.symbol!, pars: pars as! Dictionary<String, Any>) { (data) in
//            self.showMessage("The link has been sent for the review", style: .success)
//            self.dismiss(animated: true, completion: nil)
//        }
    }
              
    func showMessage(_ text:String, style:Theme) {
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.presentationContext = .window(windowLevel: UIWindow.Level.alert)
        config.duration = .seconds(seconds: 1.5)
        config.dimMode = .gray(interactive: true)
        config.interactiveHide = false
        config.preferredStatusBarStyle = .lightContent
        config.eventListeners.append() { event in
            if case .didHide = event { print("yep") }
        }
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureTheme(style)
        view.configureDropShadow()
        let iconText = style == .success ? "🙌" : ""
        view.configureContent(title: style == .success ? "Thank you!" : "Warning!", body: text, iconImage: nil, iconText: iconText, buttonImage: nil, buttonTitle: "OK") { (button) in
            SwiftMessages.hide()
        }
        SwiftMessages.show(config: config, view: view)
    }
    
    func createNew() {
        
    }
}
