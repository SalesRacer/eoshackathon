

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import RealmSwift
import Agrume


 class StacksViewController: UIPageViewController { 
    
    static let stackBulk = 20
    var currentIndex = 0
    var pretendentIndex = 0
    var idle: Bool = true
    var staticState: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.scrollToIndex(withNotification:)),
                                               name: NSNotification.Name(rawValue: NotificationName.scrollToStack), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.menuClick(noti:)), name: NSNotification.Name(rawValue: NotificationName.menuClick), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.tweakUI(noti:)), name: NSNotification.Name(rawValue: NotificationName.reloadMode), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCurrentICO), name: NSNotification.Name(rawValue: NotificationName.updateCurrentICO), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSlides(_:)), name: NSNotification.Name(rawValue: NotificationName.showSlides),  object: nil)
        
//        self.loadInitial()
    }
    
    @objc func updateCurrentICO() {
        DataManager.℠.currentStacks = DataManager.℠.currentStacks
        if DataManager.℠.currentICO == nil, DataManager.℠.currentStacks.count > 0 {
            DataManager.℠.currentICO = DataManager.℠.currentStacks[0] 
        }
    }
    
    func loadInitial() { 
        updateCurrentICO()
        self.tweakUI(noti: nil)
    }
    
    @objc func menuClick(noti:NSNotification?) {
        currentIndex = 0
        tweakUI(noti: noti)
    }
    
    @objc func tweakUI(noti:NSNotification?) {
        if let ico = DataManager.℠.currentICO {
            self.currentIndex = DataManager.℠.currentStacks.index(of: ico) ?? 0 
        } else { 
            self.currentIndex = 0
        }
        switch AppManager.℠.currentMode {
        case .browse:
            self.staticState = false
            self.dataSource = self
            self.setViewControllers(self.createSingleStack(withIndex: self.currentIndex), direction: UIPageViewController.NavigationDirection.forward, animated: false) { (Bool) in }
        case .oneCard:
            self.staticState = true
            self.dataSource = nil
            self.setViewControllers(self.createSingleStack(withIndex: self.currentIndex), direction: UIPageViewController.NavigationDirection.forward, animated: false) { (Bool) in }
        case .favourites:
            self.currentIndex = 0
            self.staticState = false
            self.dataSource = self
            self.setViewControllers(self.createSingleStack(withIndex: self.currentIndex), direction: UIPageViewController.NavigationDirection.forward, animated: false) { (Bool) in }
        }
    }
    
    func createSingleStack(withIndex index:Int) -> Array<SingleStackViewController> {
        var bunch: Array<SingleStackViewController> = Array()
        let single = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier:"SingleStackViewController") as! SingleStackViewController
        single.currentICO = DataManager.℠.currentStacks[index]
        single.index = index
        bunch.append(single)
        return bunch
    }
    
    @objc func scrollToIndex(withNotification noti:NSNotification) {
        if DeviceType.iPhone5orSE { 
            if let index = noti.object as! Int? {
                let direction = (index > self.currentIndex) ? UIPageViewController.NavigationDirection.forward : UIPageViewController.NavigationDirection.reverse
                self.currentIndex = index
                AppManager.synchronized(self, closure: {
                    self.setViewControllers(self.createSingleStack(withIndex: self.currentIndex), direction: direction, animated: true) { (Bool) in
                        
                    }
                })
            }
        }
    }
    
    @objc func showSlides(_ noti:NSNotification) { 
        let images = noti.object as! [UIImage]
        let agrume = Agrume(images: images, startIndex: 0, background: .colored(UIColor.darkGray), dismissal:Dismissal.withPhysics)
        agrume.show(from:self.parent!)  
    }
}

extension StacksViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) { 
        if staticState {
            return 
        }
        let one = pendingViewControllers[0] as! SingleStackViewController
        self.pretendentIndex = one.index 
        self.idle = false
        DataManager.℠.currentICO = DataManager.℠.currentStacks[self.pretendentIndex]
    }
} 
 
extension StacksViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController],  transitionCompleted completed: Bool) {
        if completed {
            self.currentIndex = self.pretendentIndex
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.scrolledToIndex), object: self.currentIndex)
        }
        self.idle = true
    } 
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? { 
//        if staticState {
//            return nil
//        }
//        let nextIndex = self.currentIndex - 1
//        if nextIndex > -1 {
//            return self.createSingleStack(withIndex: nextIndex, count: 1)[0]
//        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
//        if staticState {
//            return nil
//        }
//        let nextIndex = self.currentIndex + 1
//        if nextIndex < DataManager.℠.currentStacks.count {
//            return self.createSingleStack(withIndex: nextIndex, count: 1)[0]
//        }
        return nil
    } 
}

