
import UIKit
import expanding_collection
import Alamofire
import AlamofireImage
import SwiftyJSON
import RealmSwift
import Agrume

import Mixpanel

class CardCollectionViewController: ExpandingViewController { 
    
    static let stackBulk = 20
    var pretendentIndex = 0
    var timer: Timer?
    var idle: Bool = true
    var staticState: Bool = false
    
    override func viewDidLoad() {
        if DeviceType.iPhone5orSE {
            itemSize = CGSize(width: 226, height: 336)
        } else {
            itemSize = CGSize(width: 276, height: 396)
        }
        super.viewDidLoad()
        
        let nib = UINib(nibName: "CardCollectionViewCell", bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: "CardCollectionViewCell")
        collectionView?.clipsToBounds = false 
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.scrollToIndex(withNotification:)), name: NSNotification.Name(rawValue: NotificationName.scrollToItem), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.menuClick(noti:)), name: NSNotification.Name(rawValue: NotificationName.menuClick), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.tweakUI(noti:)), name: NSNotification.Name(rawValue: NotificationName.reloadMode), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCurrentICO), name: NSNotification.Name(rawValue: NotificationName.updateCurrentICO), object: nil)
    }
    
    @objc func updateCurrentICO() {
        DataManager.℠.currentStacks = DataManager.℠.currentStacks
        if DataManager.℠.currentICO == nil, DataManager.℠.currentStacks.count > 0 {
            DataManager.℠.currentICO = DataManager.℠.currentStacks[0]
        }
    }
    
    func loadInitial() {
        updateCurrentICO()
        self.tweakUI(noti: nil)
    }
    
    @objc func menuClick(noti:NSNotification?) { 
        tweakUI(noti: noti)
    }
    
    @objc func tweakUI(noti:NSNotification?) {
        self.staticState = true
        collectionView?.reloadData()
    }
    
    @objc func scrollToIndex(withNotification noti:NSNotification) {
        if let index = noti.object as! Int?, DataManager.℠.currentStacks.count > 0 { 
            DataManager.℠.currentICO = DataManager.℠.currentStacks[index]
            self.collectionView?.scrollToItem(at: IndexPath.init(item: index, section: 0), at: .centeredHorizontally, animated: true)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.scrolledToIndex), object: index)
            loadAround(index: index)
        }
    } 
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataManager.℠.currentStacks.count
    }
    
    func parseInfo(to ico:ICO, info:Dictionary<String,String>) {
        for key in info.keys {
            if let def = info[key] {
                switch key {
                case "Pins":
                    ico.categories.append(def)
                case "Intro":
                    ico.intro = def
                case "Story":
                    ico.story = def
                case "NAICS":
                    ico.naics = def
                case "Description":
                    ico.descript = def
                case "Reason of allocation":
                    ico.reason = def
                case "Industry":
                    ico.industry = def
                default:
                    break
                }
            }
        }
    }
    
    func parseCards(to ico:ICO, cards:[Dictionary<String,Any>]) {
        var stories = Dictionary<String,String>()
        for c in cards {
            let card = Card()
            if var type = c["type"] as? Int {
                card.type = CardType(rawValue:type)!
            }
            if var arr = c["links"] as? [String] {
                if let cardOne = arr[0] as String? {
                    let ar = cardOne.components(separatedBy: "||")
                    
                    if card.type.rawValue == 7 {
                        if ar.count > 1 {
                            card.links = [ar[0]]
                            card.fake = ar[1]
                        } else {
                            card.links.append(contentsOf: arr)
                        }
                    }
                }
            }
            if let props = c["properties"] as? [Dictionary<String,Any>] {    
                for dict in props {
                    if dict["type"] as! String == "STORY_PROP" {
                        card.story = dict["value"] as? String
                        if let story = card.story {
                            stories[CardTypeText[card.type.rawValue]!] = story
                        } 
                    }
                    if dict["type"] as! String == "TAGS_PROP" {
                        card.tags = dict["value"] as? String
                    }
                    if dict["type"] as! String == "PROS_PROP" {
                        card.pros = dict["value"] as? String
                    }
                    if dict["type"] as! String == "CONS_PROP" {
                        card.cons = dict["value"] as? String
                    }
                }
            }
            card.index = c["index"] as? String
//            if ico.symbol == "ADX", card.index == "Q" {
//                card.links.removeAll()
            //                card.links = ["https://youtu.be/E4A0bcCQke0"]
            //            }
            if card.type.rawValue == 7 { 
                ico.cards.insert(card, at: 0)
            }
        }
//        for ind in CardType.profile.rawValue...CardType.market.rawValue {
//            var add = true
//            for card in ico.cards {
//                if card.type.rawValue == ind {
//                    add = false
//                }
//            }
//            if add {
//                let card = Card()
//                card.type = CardType(rawValue:ind)!
//                ico.cards.append(card)
//            }
//        }
//        ico.cards.sort { (one, two) -> Bool in
//            return one.type.rawValue < two.type.rawValue
//        }
        ico.stories = stories
    }
   
    @objc override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:("CardCollectionViewCell"), for: indexPath) as! CardCollectionViewCell
        let view = cell.frontContainerView as! OneCardView
        cell.backContainerView.layer.cornerRadius = 13 
        view.tag = 333
        view.layer.cornerRadius = 13
        let ico =  DataManager.℠.currentStacks[indexPath.item]
        let name = ico.name!
        view.name.text = name
        view.codeName.text = ico.symbol
        let tap = UITapGestureRecognizer.init(target: cell, action: #selector(cell.cellTouched)) 
        cell.addGestureRecognizer(tap)  
        
        let index = indexPath.item
        
        if !isDragging {
            loadAround(index: index)
        }
        
        cell.indexRow = indexPath.item
        cell.currentICO = ico
        view.currentICO = ico
        
        view.fill()
        return cell
    }
    
    var isDragging = false
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CardCollectionViewCell 
        cell.cellIsOpen(!cell.isOpened)
    }
    
    override func collectionView(_ view: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt index : IndexPath) {
        super.collectionView(view, willDisplay: cell, forItemAt: index)
        isDragging = true
    }
    
    func loadAround(index:Int) {
        if AppManager.℠.currentMode == .browse && NetworkManager.℠.playSession != nil {
            retrieve(index: index)
            retrieve(index: index + 1)
        }
    }
    
    func retrieve(index:Int) {
        DispatchQueue.global(qos: .userInitiated).async {
            guard DataManager.℠.currentStacks.count > index else { return }
            let ico = DataManager.℠.currentStacks[index]
            if !ico.loaded {
                ico.loaded = true
                NetworkManager.℠.loadNews(symbol:ico.symbol!)
                let symbol = DataManager.℠.currentStacks[index].symbol!
                NetworkManager.℠.retrieve(ico: symbol, completionHandler: { (response) in
                    switch response.result {
                    case .success(let data):
                        let json = JSON(data)
                        let dict = json["data"]
                        if let all = dict.dictionaryObject {
                            let realm = try! Realm()
                            if let theIco = DataManager.℠.currentStacks.first(where: { $0.symbol == symbol }) {
                                try! realm.write() {
                                    for key in all.keys {
                                        if key == "fields" {
                                            if let info = all[key] as? Dictionary<String,String> {
                                                self.parseInfo(to: theIco, info: info)
                                            }
                                        }
                                        else if key == "cards" {
                                            if let info = all[key] as? [Dictionary<String,Any>] {
                                                self.parseCards(to: theIco, cards: info)
                                            }
                                        }
                                    }
                                    if theIco.cards.count == 0 {
                                        self.parseCards(to: theIco, cards: [Dictionary<String,Any>]())
                                    }
                                }
                            }
                        } else {
                            self.parseCards(to: ico, cards: [Dictionary<String,Any>]())
                        }
//                        DispatchQueue.main.async {
//                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.additionalLoaded), object: ico) 
//                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reloadPager), object: ico)
//                        }
                        NetworkManager.℠.loadRating(ico: ico)
//                        NetworkManager.℠.loadNews(symbol:ico.symbol!)
                    case .failure(let error):
                        self.parseCards(to: ico, cards: [Dictionary<String,Any>]())
                        print("Request failed with error: \(error)")
                    }
                })
                
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        guard DeviceType.iPhone5orSE else { return }
        deal()
    }
    
    func deal() {
        isDragging = false
        let array = self.collectionView?.visibleCells
        for cell in array! {
            let c = cell as! CardCollectionViewCell
            c.cellIsOpen(false)
        }
        if let arr = array as? [CardCollectionViewCell], arr.count > 0 {
            var numbers = [Int]()
            for cell in arr {
                numbers.append(cell.indexRow!)
            }
            
            if numbers.count < 2 {
                return
            }
            numbers = numbers.sorted(by: { $0 < $1 })
            let first = numbers[0]
            let last = numbers[numbers.count - 1]
            var index = 0
            if numbers.count == 2 {
                if DataManager.℠.currentStacks.count == 2 {
                    if first == 1 {
                        index = first
                    } else {
                        index = last
                    }
                } else {
                    if first == 0 {
                        index = first
                    } else {
                        index = last
                    }
                }
            } else {
                let sec = numbers[1]
                index = sec
            }
            
            let cell = self.collectionView?.cellForItem(at: IndexPath.init(item: index, section: 0))
            let view = (cell as! CardCollectionViewCell).frontContainerView as! OneCardView
            
            loadAround(index: index)
            
            
            if DeviceType.iPhone5orSE {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.scrollToStack), object: index)
            }
            
            DataManager.℠.currentICO = DataManager.℠.currentStacks[index]
            view.fill()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.scrolledToIndex), object: index) 
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.scrollToStack), object: index)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        deal()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.closeFakeViews), object:nil) 
    }

}
 
