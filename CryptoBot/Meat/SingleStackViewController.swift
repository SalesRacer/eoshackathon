
import UIKit
import Koloda
import SwiftMessages 
import SwiftyJSON
import SwiftyJSON
import PopupDialog

class SingleStackViewController: UIViewController {

    var fakeMessage:String?
    
    var tick = 0
    
    var currentFakingIndex = 0 // ??? !!! 
    
    @IBOutlet weak var kolodaView: KolodaView!
    
    var isMovable: Bool = true
    var currentView: UIView? = nil
    var currentICO: ICO? = nil
    var index: Int = 0 
    @IBOutlet weak var close: UIButton!
    
    let like = UIImage.init(named: "like")?.withRenderingMode(.alwaysTemplate)
    let dislike = UIImage.init(named: "dislike")?.withRenderingMode(.alwaysTemplate)
    let cards = UIImage.init(named: "cards")?.withRenderingMode(.alwaysTemplate)
    let fake = UIImage.init(named: "lemon")?.withRenderingMode(.alwaysTemplate)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        kolodaView.dataSource = self
        kolodaView.delegate = self
        
//        let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.koloda(_:didSelectCardAt:)))
//        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tweakUI), name: NSNotification.Name(rawValue: NotificationName.reloadStacks), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.tweakUI), name: NSNotification.Name(rawValue: NotificationName.reloadMode), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadStack), name: NSNotification.Name(rawValue: NotificationName.playSession), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.additionalLoaded), name: NSNotification.Name(rawValue: NotificationName.additionalLoaded), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.stackOpened), name: NSNotification.Name(rawValue: NotificationName.stackOpened), object: nil)
        currentView = self.kolodaView.viewForCard(at: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.view.tag == 177 {
            self.close.alpha = 1
        }
        
//        NotificationCenter.default.addObserver(self, selector: #selector(self.koloda(_:didSelectCardAt:)), name: NSNotification.Name(rawValue: NotificationName.switchScroll), object: nil)
        
        self.tweakUI(noti: nil)
//        if AppManager.℠.currentMode == .oneCard {
//            self.reloadStack()
//        }
//        DataManager.℠.currentICO = DataManager.℠.currentStacks[self.stack.index]
        
        
    }
    
    @objc func stackOpened() {
        if currentICO!.loaded {
            if currentICO?.cards.count == 0 {
                DataManager.℠.createBountyCard(ico: (currentICO?.symbol)!)
                self.reloadStack()
                return
            }
            var ready: OneSwipeView? = nil
            let view = self.kolodaView.viewForCard(at: 0)!
            for one in view.subviews {
                if one.tag == 333 {
                    ready = one as? OneSwipeView
                }
            }
            if let red = ready {
                DataManager.℠.oneCardTop = red
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.changeBack), object: red.isFake)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func tweakUI(noti:NSNotification?) {
        if AppManager.℠.currentMode == .oneCard {
            self.isMovable = true
        }
        else {
            self.isMovable = false
        }
    }
}

extension SingleStackViewController: KolodaViewDelegate {
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        if self.view.tag == 177 {
            self.dismiss(animated: true, completion: nil) 
        }
        
        var saved = UserDefaults.standard.object(forKey: K.ratingsReadyIco) as? [String] ?? [String]()
        saved.append((self.currentICO?.symbol)!)
        UserDefaults.standard.set(saved, forKey: K.ratingsReadyIco)
        
        let ratingVC = EditStarsView(nibName: "EditStarsView", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
//        popup.completion = {
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.switchScroll), object: nil)
//        }
        ratingVC.titleLabel.text = "Review your rating"
        ratingVC.caption.text = "Change the overall rating of the " + (self.currentICO?.name)! + " ICO based on your impression. \n\nSuggest the short tagline, that represents qualities of this ICO on your opinion and receive additional points after approval"
        
        let rate = DataManager.℠.loadOveRating(ico: (self.currentICO?.name)!) ?? 0
        if rate == 1.0 {
            ratingVC.cosmosStarRating.rating = 5
        } else {
            ratingVC.cosmosStarRating.rating = 5 - (rate * 5)
        }
        // Create first button
        let buttonOne = CancelButton(title: "CANCEL", height: 50) {
            self.kolodaView.reloadData()
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.switchScroll), object: nil)
        }     
        
        // Create second button
        let buttonTwo = DefaultButton(title: "ACCEPT", height: 60) {
            self.kolodaView.reloadData()
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.switchScroll), object: nil)
            DataManager.℠.saveOveRating(rating: (ratingVC.cosmosStarRating.rating / 5.0), for: self.currentICO!)
            if ratingVC.commentTextField.text != nil, ratingVC.commentTextField.text != "" {
                
                if let toSend = ratingVC.commentTextField.text { 
//                    NetworkManager.℠.updateLink(ico: DataManager.℠.currentICO!.symbol!, pars: pars) { (data) in
//
//                    }
                    popup.dismiss({
                    })
//                    self.showMessage("The link has been sent for the review", style: .success)
                }
            }
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonTwo, buttonOne])
        
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    
    @objc func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.switchScroll), object: nil)
        self.reloadStack()
    }
    
    @objc func reloadStack() {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reloadMode), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.closeFakeViews), object: 0)
            self.kolodaView.resetCurrentCardIndex()
            self.tweakUI(noti: nil)
            self.currentView = self.kolodaView.viewForCard(at: 0)
        }
    }
}

extension UIView {
    class func fromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

extension SingleStackViewController: KolodaViewDataSource {
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return DragSpeed.moderate
    }
    
    func kolodaNumberOfCards(_ koloda:KolodaView) -> Int {
        if let ico = self.currentICO {
            return ico.cards.count
        }
        return 0
    }
 
    @objc func showDetails() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.showDetails), object: nil) 
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let base = UIView()
        
        if let ico = self.currentICO {
            let view: OneSwipeView = OneSwipeView.fromNib()
            view.cardIndex = index
            let card = ico.cards[index]
            view.tag = 333
            base.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.center = base.center
            view.underView.layer.cornerRadius = 17
            view.layer.cornerRadius = 17
            base.layer.cornerRadius = 17
            base.backgroundColor = UIColor.white
            view.underView.layer.borderColor = UIColor.lightGray.cgColor
            view.underView.layer.borderWidth = 1
            let leadingConstraint = NSLayoutConstraint(item: base, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
            let trailingConstraint = NSLayoutConstraint(item: base, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
            let topConstraint = NSLayoutConstraint(item: base, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0)
            let bottomConstraint = NSLayoutConstraint(item: base, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
            base.addConstraints([leadingConstraint, trailingConstraint, topConstraint, bottomConstraint])
            let name = ico.name!
            view.name.text = name 
            view.codeName.text = ico.symbol
            var imagePath: String?
            
            if AppManager.℠.currentMode == .oneCard {
//                switch index {
//                case 0:
                    view.currentType = card.type
                view.addAspect.tag = card.type.rawValue 
//                case 1:
//                    view.currentType = .tech
//                case 2:
//                    view.currentType = .terms
//                case 3:
//                    view.currentType = .roadmap
//                case 4:
//                    view.currentType = .market
//                case 5:
//                    if ico.coverLink != nil {
//                        view.currentType = .white
//                    } else {
//                        view.currentType = .rich
//                    }
//                default:
//                    view.currentType = .rich
//                    break
//                }
                let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.showDetails))
                view.addGestureRecognizer(tap)
            }
            
            if ico.brandLogoUrl != nil && !ico.brandLogoUrl!.isEmpty {
                imagePath = ico.brandLogoUrl
            } else if ico.coinLogoUrl != nil &&  !ico.coinLogoUrl!.isEmpty {
                imagePath = ico.coinLogoUrl
            }
            
            if imagePath == nil {
//                view.iconMissed.alpha = 1
            }
            else if let path = imagePath, NetworkManager.℠.playSession != nil {
                view.image.image = nil
                let ready = path.addingPercentEncoding(withAllowedCharacters:.urlQueryAllowed)
                NetworkManager.℠.loadImage(path: URL(string:ready!)!, view: view.image)
            }
            UIView.animate(withDuration: 0.5, animations: {
                view.image.alpha = 1
            })
            view.currentICO = ico
            
            view.fill()
        }
        return base
    }
    
    @objc func additionalLoaded(noti:NSNotification) {
        if let ico = noti.object as? ICO {
            if ico == self.currentICO, AppManager.℠.currentMode == .oneCard {
                self.reloadStack()
            }
        }
    }
 
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
        return OverlayView.init()
    }
    
    func getICO(fromViewIndex index:Int) -> ICO {
        let view = self.kolodaView.viewForCard(at: index)!
        return getICO(fromView: view)
    }
    
    func getICO(fromView view:UIView) -> ICO {
        for one in view.subviews {
            if one.tag == 10 {
                let ready = one as! OneSwipeView
                return ready.currentICO! 
            }
        }
        return ICO()
    }
    
    func koloda(_ koloda: KolodaView, didShowCardAt index: Int) {
        var ready: OneSwipeView? = nil
        let view = self.kolodaView.viewForCard(at: index)!
        for one in view.subviews {
            if one.tag == 333 {
                ready = one as? OneSwipeView
            }
        }
        DataManager.℠.oneCardTop = ready!
        DataManager.℠.oneCardTop!.dimDetails()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.changeBack), object: ready!.isFake)  
        if ready!.contributePlaceholder.alpha > 0 {
                DataManager.℠.bountyOnScreen = true
        } else {
            DataManager.℠.bountyOnScreen = false
        }
    }
    
    func koloda(_ koloda: KolodaView, shouldDragCardAt index: Int) -> Bool { 
        return self.isMovable                                                                                                       
    }
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        DataManager.℠.nextStepTutorial(toStart: 2)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.startGif), object: nil)
        if self.view.tag == 177 { 
            return
        }
        let ico = DataManager.℠.currentICO! //self.getICO(fromView: currentView)
        currentView = self.kolodaView.viewForCard(at: koloda.currentCardIndex)
//        if let aspects = ico.aspects, aspects.count > index, let quality = ico.aspects?[index] {
//            if quality == Aspect.quality.none(index) { } else {
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.scrolledToIndex), object: index + 1) 
//                return
//            }
//        }
//        if index > 4 {
//            return
//        }
        if ico.aspects == nil {
            ico.aspects = Dictionary()
        }
//        DataManager.℠.currentICO = ico
        var iconText = ""
        let view = MessageView.viewFromNib(layout: .messageView) 
        var title = ""
        var body = ""
        let name = (Aspect.names.count > index) ? Aspect.names[index] : ""
        var value: Int = 0
        var jump = false
        var card = ico.cards[index]
        
        NetworkManager.℠.sendEOS() 
        
        if direction == .up
        {
            value = 0
            jump = true 
        } else if direction == .down {
            value = 0
            jump = true
            DataManager.℠.nextStepTutorial(toStart: 3)
            
            if card.type == .rich {
                currentFakingIndex = index
                submitFake()
                NetworkManager.℠.markAsFake(link:"TestLink", author:"BadAuthor", date:"ThatDate", votesUp:27, votesDown:9)  
            }
            
        } else {
            let cardIndex = card.index
            if nil == DataManager.℠.loadRating(for: ico, with: cardIndex ?? "\(index)") && card.index != "fakeindex" {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.pulsePoints), object: PointsBag.init(points: 1, type: 1, symbol:  ico.symbol, card: card.index))
                switch direction {
                case .left:
                    iconText = "👎"
                    view.configureTheme(.error)
                    title = "Mmm..."
                    body = "The \(name) info of \(ico.name ?? "ICO") was downrated"
                    value = -1
                case .right:
                    iconText = "👍"
                    view.configureTheme(.success)
                    title = "Yeah!"
                    body = "The \(name) info of \(ico.name ?? "ICO") was uprated"
                    value = 1
                default:
                    break
                }
            
            DataManager.℠.saveRating(rating: Double(value), for: ico, with: cardIndex ?? "\(index)")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.paintIt), object: index)
            //        }
            DataManager.℠.performRealm {
                if ico.cards.count > index + 1 {
                    let card = ico.cards[index]
                    UserDefaults.standard.set(DataManager.℠.currentRating, forKey: "rating")
                    if !jump {
                        if value != 0 {
                            NetworkManager.℠.sendRating(ico: ico, name: card.index, rating: (direction == .left) ? -1 : 1)
                            //                    NetworkManager.℠.updatePoints()
                        }
                        var config = SwiftMessages.Config()
                        config.presentationStyle = .top
                        config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
                        config.duration = .seconds(seconds: 0.5)
                        config.dimMode = .gray(interactive: true)
                        config.interactiveHide = false
                        config.preferredStatusBarStyle = .lightContent
                        config.eventListeners.append() { event in
                            if case .didHide = event { print("yep") }
                        }
                        view.configureDropShadow()
                        
                        view.configureContent(title: title, body: body, iconImage: nil, iconText: iconText, buttonImage: nil, buttonTitle: "OK") { (button) in
                            SwiftMessages.hide()
                        }
                        SwiftMessages.show(config: config, view: view)
                    } else {
                        if direction == .up || direction == .down {
                            NetworkManager.℠.sendRating(ico: ico, name: card.index, rating: 0)
                        }
                    }
                }
            }
        }
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.scrolledToIndex), object: index + 1)
    }
    
    func openFakeDialog(string:String) {
        let ratingVC = EditUpdateView(nibName: "EditUpdateView", bundle: nil)
        // Prepare the popup assets
        let title = "Please explain what’s fake"
        let message = "Explain the reason of marking this article as fake to get the reward. If the community validates and agrees you will be awarded a minimum of 1000 points."
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .vertical, transitionStyle: .bounceDown, tapGestureDismissal: true)
        ratingVC.name.text = title
        ratingVC.caption.text = message
        fakeMessage = "wrong: " + string
        
        let buttonOne = DefaultButton(title: "Continue") {
            self.fakeMessage?.append("reason: " + ratingVC.commentTextField.text)
            let ratingVC2 = EditUpdateView(nibName: "EditUpdateView", bundle: nil)
            // Prepare the popup assets
            let title = "Do you have the link to the source that supports your position?"
            let message = "Add the link for reason of marking this article as fake in the box below to get the reward."
            // Create the dialog
            let popup = PopupDialog(viewController: ratingVC2, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
            ratingVC2.name.text = title
            ratingVC2.caption.text = message
            let buttonTwo = DefaultButton(title: "SUBMIT") {
                let card = self.currentICO?.cards[self.currentFakingIndex]
                DataManager.℠.saveFakeNews(ico: (self.currentICO?.symbol)!, index: card?.index ?? "notfakeatall")
                self.fakeMessage?.append("link: " + ratingVC2.commentTextField.text) 
                NetworkManager.℠.fakeLink(ico: (self.currentICO?.symbol)!, message: self.fakeMessage ?? "", completionHandler: { (data) in
                    
                })
                let view = MessageView.viewFromNib(layout: .messageView)

                var config = SwiftMessages.Config()
                config.presentationStyle = .top
                config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
                config.duration = .seconds(seconds: 0.5)
                config.dimMode = .gray(interactive: true)
                config.interactiveHide = false
                config.preferredStatusBarStyle = .lightContent
                config.eventListeners.append() { event in
                    if case .didHide = event { print("yep") }
                }
                view.configureDropShadow()
                
                view.configureContent(title: "Great!", body: "Your update was sent for the review", iconImage: nil, iconText:"👍", buttonImage: nil, buttonTitle: "OK") { (button) in
                    SwiftMessages.hide()
                }
                SwiftMessages.show(config: config, view: view) 
                
            }
            
            let buttonThree = CancelButton(title: "CANCEL", dismissOnTap: true) {
            }
            popup.addButtons([buttonTwo, buttonThree])
            self.present(popup, animated: true, completion: nil)
            ratingVC2.pendingHeight.constant = 0
            ratingVC2.updatesString.text = " "
            ratingVC2.commentTextField.text = "Input the link (optional)"
        }
        let buttonThree = CancelButton(title: "Cancel", dismissOnTap: true) {
        }
        popup.addButtons([buttonOne, buttonThree]) 
        self.present(popup, animated: true, completion: nil)
        ratingVC.pendingHeight.constant = 0
        ratingVC.updatesString.text = " "
        ratingVC.commentTextField.text = "Input the reason" 
    }
    
    func submitFake() {
        let card = self.currentICO?.cards[self.currentFakingIndex] 
        var link = card?.links[0]
        let ratingVC = SimplePopup(nibName: "SimplePopup", bundle: nil)
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .vertical, transitionStyle: .bounceDown, tapGestureDismissal: true)
        ratingVC.titleLabel.text = "Where is the fake content?"
        ratingVC.textView.text = "Please select the option for " + (link ?? "")
        let buttonTwo = DefaultButton(title: "Headline", dismissOnTap: true) {
            self.openFakeDialog(string: "Headline")
        }
        let buttonW = DefaultButton(title: "Body", dismissOnTap: true) {
            self.openFakeDialog(string: "Body")
        }
        let buttonE = DefaultButton(title: "Both", dismissOnTap: true) {
            self.openFakeDialog(string: "Both")
        }
        ratingVC.textHeight.constant = 65
        let cancel = CancelButton(title: "Cancel", dismissOnTap: true) {
            
        }
        popup.addButtons([buttonTwo,buttonW,buttonE, cancel])
        AppManager.℠.mainVC!.present(popup, animated: true, completion: nil)
    }
    
    func koloda(_ koloda: KolodaView, allowedDirectionsForIndex index: Int) -> [SwipeResultDirection] { 
        return [SwipeResultDirection.left, .right, .up, .down]
    }
    
    func koloda(_ koloda: KolodaView, draggedCardWithPercentage finishPercentage: CGFloat, in direction: SwipeResultDirection) { 
        var topCard: OneSwipeView?
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.stopGif), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.closeFakeViews), object: 0)
        
        for check in (koloda.viewForCard(at: koloda.currentCardIndex)?.subviews)! {
            if check.tag == 333 {
                topCard = check as! OneSwipeView
            }
        }
        if (topCard?.isBounty)! {
            return
        }
        topCard?.likeView.alpha = finishPercentage / 100.0 + 0.15 
        switch direction {
        case .bottomLeft, .left, .topLeft:
            topCard?.likeView.backgroundColor = UIColor.red
            topCard?.likeImage.image = dislike
            topCard?.likeImage.tintColor = UIColor.white
            topCard?.likeText.text = "Bad News"
            topCard?.likeText.textColor = UIColor.white
        case .bottomRight, .right, .topRight:
            topCard?.likeText.textColor = UIColor.white
            topCard?.likeView.backgroundColor = UIColor.green
            topCard?.likeImage.tintColor = UIColor.white
            topCard?.likeImage.image = like
            topCard?.likeText.text = "Good News"
        case .up:
            topCard?.likeText.textColor = UIColor.darkText
            topCard?.likeImage.tintColor = UIColor.darkText
            topCard?.likeView.backgroundColor = UIColor.white
            topCard?.likeText.text = "Skip/IDK"
            topCard?.likeImage.image = cards
        case .down:
            topCard?.likeText.textColor = UIColor.darkText
            topCard?.likeImage.tintColor = UIColor.darkText
            topCard?.likeView.backgroundColor = UIColor.yellow
            topCard?.likeText.text = "Fake"
            topCard?.likeImage.image = fake 
        }
    }
    
    func kolodaDidResetCard(_ koloda: KolodaView) {
        var topCard: OneSwipeView?
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.startGif), object: nil) 

        for check in (koloda.viewForCard(at: koloda.currentCardIndex)?.subviews)! {
            if check.tag == 333 {
                topCard = check as! OneSwipeView
            }
        }
        topCard?.likeText.textColor = UIColor.darkText 
        topCard?.likeView.alpha = 0
    }
}
