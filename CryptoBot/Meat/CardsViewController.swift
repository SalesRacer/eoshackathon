

import UIKit
import SwiftMessages
import FacebookShare
import FacebookCore
import FacebookLogin
import ModernSearchBar
import Floaty
import Branch
//import TwitterKit
import MessageUI
import LFLoginController
import PopupDialog
import Intercom
import SafariServices 
import FlexibleSteppedProgressBar
import MaterialShowcase

class CardsViewController: UIViewController, ModernSearchBarDelegate, MFMessageComposeViewControllerDelegate,  MFMailComposeViewControllerDelegate, LFLoginControllerDelegate, FlexibleSteppedProgressBarDelegate, MaterialShowcaseDelegate { 
    
    @IBOutlet weak var menu: UIButton!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var sliderView: UIStackView!
    @IBOutlet weak var slideControls: UIStackView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var sliderCounter: UILabel!
    @IBOutlet weak var favouriteButton: UIButton!
    var timer: Timer? 
    @IBOutlet weak var modeSwitch: UISegmentedControl!
    
    @IBOutlet weak var heightToMeat: NSLayoutConstraint!
    //    @IBOutlet weak var stars: CosmosView!
    
    @IBOutlet weak var oldContainerView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var oneCardControls: UIView!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var exitButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var toExitButtonWidth: NSLayoutConstraint! 
    @IBOutlet weak var starView: UIView!
    @IBOutlet weak var starLabel: UILabel!
    
    @IBOutlet weak var floaty: Floaty! 
    @IBOutlet weak var searchBar: ModernSearchBar!
    @IBOutlet weak var zoomIcon: UIButton!
    
    var savedExitButtonWidth: CGFloat = 36.0
    
    var searchResults: Array<ICO>! = []
    
    var stacksVC: StacksViewController?
    var icons: Array<UIImageView> = Array()
    
    var congrats:(String, PointsBag)?
    
    var initialOpened = false
    
    var copyClipboard: EditUpdateView?
    var copyClipboardText: String?
    
    @IBOutlet weak var heightToPager: NSLayoutConstraint!
    @IBOutlet weak var pageBelow: UIView!
    @IBOutlet weak var progressBar: FlexibleSteppedProgressBar!
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    @IBOutlet weak var proportionsConstraint: NSLayoutConstraint!
    @IBOutlet weak var oldPropsConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var topHeight: NSLayoutConstraint!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if DeviceType.iPhone5orSE {
            topHeight.constant = 0 
        }
//        proportionsConstraint.isActive = false
//        oldPropsConstraint.isActive = true
        AppManager.℠.mainVC = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.addCongrats(noti:)), name: NSNotification.Name(rawValue: NotificationName.addCongrats), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.dimDetails(noti:)), name: NSNotification.Name(rawValue: NotificationName.dimDetails), object: nil) 
        NotificationCenter.default.addObserver(self, selector: #selector(self.tweakUI(noti:)), name: NSNotification.Name(rawValue: NotificationName.menuClick), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.tweakUI(noti:)), name: NSNotification.Name(rawValue: NotificationName.reloadMode), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.paintIt(noti:)), name: NSNotification.Name(rawValue: NotificationName.paintIt), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.switchScroll), name: NSNotification.Name(rawValue: NotificationName.switchScroll), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openLink(noti:)), name: NSNotification.Name(rawValue: NotificationName.openLink), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeBack(noti:)), name: NSNotification.Name(rawValue: NotificationName.changeBack), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.startBeginning(noti:)), name: NSNotification.Name(rawValue: NotificationName.updateCurrentICO), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.scrolledToIndex(noti:)), name: NSNotification.Name(rawValue: NotificationName.scrolledToIndex), object: nil) 
        NotificationCenter.default.addObserver(self, selector: #selector(self.reportLink(_:)), name:  NSNotification.Name(rawValue: NotificationName.reportLink), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showDetails(_:)), name:  NSNotification.Name(rawValue: NotificationName.showDetails), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.pulsePoints(_:)), name: NSNotification.Name(rawValue: NotificationName.pulsePoints), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.copyFromClipboard), name: NSNotification.Name(rawValue: NotificationName.copyClip), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showLogin), name: NSNotification.Name(rawValue: NotificationName.showLogin), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.continueLogin), name: NSNotification.Name(rawValue: NotificationName.showEmailLogin), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.addFakeViews), name: NSNotification.Name(rawValue: NotificationName.addFakeViews), object: nil)  
        NotificationCenter.default.addObserver(self, selector: #selector(self.closeFakeViews), name: NSNotification.Name(rawValue: NotificationName.closeFakeViews),  object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.distanceMoveTo(noti:)), name: NSNotification.Name(rawValue: NotificationName.distanceMoveTo),  object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.startShowcase(noti:)), name: NSNotification.Name(rawValue: NotificationName.startShowcase),  object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.fillSuggestions), name: NSNotification.Name(rawValue: NotificationName.distanceMoveTo),  object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.skippedIntro), name: NSNotification.Name(rawValue: NotificationName.skippedIntro),  object: nil)
        
        self.tweakUI(noti: nil) 
        
        self.slideControls.layer.cornerRadius = 10
        self.slideControls.layer.borderColor = UIColor.darkGray.cgColor
        self.slideControls.layer.borderWidth = 2
        
        self.pageBelow.alpha = 0
        
        UIBarButtonItem.appearance(whenContainedInInstancesOf:[UISearchBar.self]).tintColor = UIColor.lightGray
        self.searchBar.delegateModernSearchBar = self
        
        self.tweakFloaty()
        
        colourIcons()
        
        
        progressBar.numberOfPoints = 7
        progressBar.lineHeight = 4
        progressBar.radius = 8
        progressBar.progressRadius = 10
        progressBar.progressLineHeight = 3
        progressBar.lastStateCenterColor = UIColor.green
        progressBar.currentSelectedCenterColor = UIColor.white
        progressBar.delegate = self
        
        if DeviceType.iPhoneX {
            heightToMeat.constant += 30
            heightToPager.constant += 33
        } else if DeviceType.IS_IPAD {
            heightToMeat.constant += 30
            heightToPager.constant += 33
        }
    }
    
    @objc func closeFakeViews() { 
        AppManager.℠.mainVC?.pageBelow.alpha = 0
        AppManager.℠.belowViewCube = false
    }
    
    @objc func addFakeViews() {
        if AppManager.℠.belowViewCube {
//            UIView.animate(withDuration: 0.1, animations: {
                AppManager.℠.mainVC?.pageBelow.alpha = 0
//            })
        } else {
            UIView.animate(withDuration: 0.7, animations: {
                AppManager.℠.mainVC?.pageBelow.alpha = 1 
            })
        }
        AppManager.℠.belowViewCube = !AppManager.℠.belowViewCube
    }
    
    @objc func switchScroll() {
        slideshow(play: false)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.closeFakeViews), object:nil)
        if AppManager.℠.currentMode == .browse {
            UIView.animate(withDuration: 0.5, animations: {
                AppManager.℠.currentMode = .oneCard
                if let count = DataManager.℠.currentICO?.cards.count, count >= 5 {
                    self.progressBar.numberOfPoints = count
                } else {
                    self.progressBar.numberOfPoints = 5
                }
                self.progressBar.currentIndex = 0
                self.menu.alpha = 0
                self.oldContainerView.alpha = 1
                self.containerView.alpha = 0
            }) { (complete) in
                if complete {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.stackOpened), object: nil)
                    DataManager.℠.nextStepTutorial(toStart: 3)
                }
            }
        } else if AppManager.℠.currentMode == .oneCard {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.changeBack), object: false)
            self.oldContainerView.alpha = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.menu.alpha = 1 
                self.containerView.alpha = 1
                AppManager.℠.currentMode = .browse
            }) { (completed) in
                if completed {
                    DataManager.℠.nextStepTutorial(toStart: 9)
                }
            }
        } else if AppManager.℠.currentMode == .favourites {
            self.menu.alpha = 1
            moveTo(symbol: DataManager.℠.currentICO?.symbol ?? "")
            return
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reloadMode), object: nil) 
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reloadStacks), object: nil) 
    }
    
    @objc func distanceMoveTo(noti:NSNotification) {
        if let sym = noti.object as? String {
            moveTo(symbol: sym) 
        }
    }
    
    func moveTo(symbol:String) {
        AppManager.℠.currentMode = .browse
        self.oldContainerView.alpha = 0
        self.containerView.alpha = 1
        let dm = DataManager.℠
        var ind = 0
        if let index = dm.getElementIndex(symbol:symbol, mode: 0) {
            DataManager.℠.currentStacks = DataManager.℠.dataBase
            ind = index
        } else if let index = dm.getElementIndex(symbol:symbol, mode: 1) {
            DataManager.℠.currentStacks = DataManager.℠.upcomingICO
            ind = index
        } else {
            DataManager.℠.currentStacks = DataManager.℠.favourites
        }
        self.sliderCounter.text = "\(index)/\(DataManager.℠.currentStacks.count)"
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.closeFakeViews), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reloadMode), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reloadStacks), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.scrollToStack), object: ind)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.scrollToItem), object: ind) 
    }
    
    func colourIcons() {
        var arr = [favouriteButton, menu, searchButton, exitButton, zoomIcon] as! [UIView]
        arr.append(contentsOf:slideControls.subviews)
        arr.append(contentsOf:oneCardControls.subviews)
        for button in arr {
            if let b = button as? UIButton, b is UIButton {
                let templateImage = b.imageView!.image?.withRenderingMode(.alwaysTemplate)
                b.setImage(templateImage, for: .normal)
                b.imageView?.tintColor = UIColor.white
            }
        }
    }
    
    @objc func startBeginning(noti:NSNotification) {
        tweakUI(noti: noti)
        checkTutorial()
    }
    
    @objc func skippedIntro() {
        checkTutorial()
    }
    
    func checkTutorial() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.9) {
            if AppManager.℠.scrollToToken == nil, UserDefaults.standard.object(forKey: "tutorialShown") == nil {
                let ratingVC = SimplePopup(nibName: "SimplePopup", bundle: nil)
                let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
                ratingVC.titleLabel.text = "Hello, welcome to Coinswipe"
                ratingVC.textView.text = "Would you like to go through a fast tutorial?"
                let buttonTwo = DefaultButton(title: "OK", dismissOnTap: true) {
                    AppManager.℠.tutorialStarted = true
                    UserDefaults.standard.set(true, forKey: "tutorialShown")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.distanceMoveTo), object:"EOS")
                }
                ratingVC.textHeight.constant = 45
                let cancel = CancelButton(title: "Skip", dismissOnTap: true) {
                    UserDefaults.standard.set(true, forKey: "tutorialShown")
                }
                popup.addButtons([buttonTwo, cancel])
                AppManager.℠.mainVC!.present(popup, animated: true, completion: nil)
            }
        }    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) 
        
        starView.layer.cornerRadius = 5
        //        starView.layer.borderColor = UIColor.darkGray.cgColor
        //        starView.layer.borderWidth = 1
        starView.layer.masksToBounds = false
        starView.layer.shadowOpacity = 1
        starView.layer.shadowOffset = CGSize.init(width: 1, height: 1)
        starView.layer.shadowColor = UIColor.darkGray.cgColor
        
        starLabel.text = "\(DataManager.℠.currentRating)"
        
    }
    
    @objc func copyFromClipboard() {
        copyClipboardText = UIPasteboard.general.string
        if let view = copyClipboard {
            view.commentTextField.text = copyClipboardText
        }
    }
    
    @objc func addCongrats(noti:NSNotification) {
        if let congrat = noti.object as? (String, PointsBag) {
            self.congrats = congrat
            showCongrats(text: congrat.0, points: congrat.1)
        }
    }
    
    func tweakFloaty() {
        floaty.size = 40
        floaty.openAnimationType = .fade
        floaty.backgroundColor = UIColor.clear
        floaty.buttonColor = UIColor.clear
        
        floaty.addItem(" ", icon: UIImage(named: "facebook")!, handler: { item in
            if AppManager.℠.loginFacebook(vc: self) {
                let buo = BranchUniversalObject(canonicalIdentifier: "coin")
                buo.addMetadataKey("ticker", value: DataManager.℠.currentICO!.symbol!)
                
                let lp: BranchLinkProperties = BranchLinkProperties()
                lp.channel = "facebook"
                lp.feature = "sharing"
                buo.getShortUrl(with: lp) { (url, error) in
                    let content = LinkShareContent(url: URL(string: url!)!,
                                                   title: "Check the brand new Coinswipe app! ",
                                                   description: nil,
                                                   quote: nil,
                                                   imageURL: nil)
                    let shareDialog = ShareDialog(content: content)
                    shareDialog.mode = .web
                    shareDialog.failsOnInvalidData = false
                    shareDialog.completion = { result in
                        // Handle share results
                    }
                    do {
                        try shareDialog.show()
                    }
                    catch { }
                }
            }
            self.floaty.close() 
        })
        floaty.addItem(" ", icon: UIImage(named: "email")!, handler: { item in
            let buo = BranchUniversalObject(canonicalIdentifier: "coin")
            buo.addMetadataKey("ticker", value: DataManager.℠.currentICO!.symbol!)
            
            let lp: BranchLinkProperties = BranchLinkProperties()
            lp.channel = "email"
            lp.feature = "sharing"
            buo.getShortUrl(with: lp) { (url, error) in
                if MFMailComposeViewController.canSendMail(), let u = url {
                    let composeVC = MFMailComposeViewController()
                    composeVC.mailComposeDelegate = self
                    composeVC.setToRecipients(nil)
                    composeVC.setSubject("Check the brand new Coinswipe app!")
                    composeVC.setMessageBody(u, isHTML: false)
                    self.present(composeVC, animated: true, completion: nil)
                } else {
                    //                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                    let alert = UIAlertController.init(title: "Missed credentials", message: "Please add the email account on the device Settings page",  preferredStyle: .alert)
                    alert.addAction(UIAlertAction.init(title: "OK", style: .cancel, handler: nil))
                    //                    self.floaty.close()
                    self.present(alert, animated: true, completion: nil)
                    //                    })
                }
            }
        })
        floaty.addItem(" ", icon: UIImage(named: "sms")!, handler: { item in
            let buo = BranchUniversalObject(canonicalIdentifier: "coin")
            buo.addMetadataKey("ticker", value: DataManager.℠.currentICO!.symbol!)
            let lp: BranchLinkProperties = BranchLinkProperties()
            lp.channel = "sms"
            lp.feature = "sharing"
            buo.getShortUrl(with: lp) { (url, error) in
                if let u = url {
                    let messageVC = MFMessageComposeViewController()
                    
                    messageVC.body = "Check the brand new Coinswipe app! \(u)"
                    messageVC.recipients = nil
                    messageVC.messageComposeDelegate = self
                    
                    self.present(messageVC, animated: false, completion: nil)
                }
            }
            self.floaty.close()
        })
        //        floaty.addItem(" ", icon: UIImage(named: "twitter")!, handler: { item in
        //                let buo = BranchUniversalObject(canonicalIdentifier: "coin")
        //                buo.addMetadataKey("ticker", value: DataManager.℠.currentICO!.symbolt)
        //
        //                let lp: BranchLinkProperties = BranchLinkProperties()
        //                lp.channel = "facebook"
        //                lp.feature = "sharing"
        //                buo.getShortUrl(with: lp) { (url, error) in
        //
        //                    if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
        //                        if let userID = TWTRTwitter.sharedInstance().sessionStore.session()?.userID, let res = url {
        //                            let client = TWTRAPIClient(userID: userID)
        //                            let composer = TWTRComposer()
        //                            composer.setText("Check the brand new Coinswipe app! " + res)
        //                            composer.show(from: self, completion:{ (result) in
        //
        //                            })
        //                        }
        //                    }
        //                    else {
        //                        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
        //                            if session != nil { // Log in succeeded
        //                                if let userID = TWTRTwitter.sharedInstance().sessionStore.session()?.userID, let res = url {
        //                                    let client = TWTRAPIClient(userID: userID)
        //                                    let composer = TWTRComposer()
        //                                    composer.setText("Check the brand new Coinswipe app! " + res)
        //                                    composer.show(from: self, completion:{ (result) in
        //
        //                                    })
        //                                }
        //                            }
        //                        })
        //
        //                    }
        //                }
        //                self.floaty.close()
        //            })
        floaty.addItem(" ", icon: UIImage(named: "sortNum")!, handler: { item in
            DataManager.℠.reorderDB(sort: .price)
            self.floaty.close()
        })
        floaty.addItem(" ", icon: UIImage(named: "sortFill")!, handler: { item in
            DataManager.℠.reorderDB(sort: .filled)
            self.floaty.close()
        })
        floaty.addItem(" ", icon: UIImage(named: "sortAlpha")!, handler: { item in 
            DataManager.℠.reorderDB(sort: .alpha)
            self.floaty.close()
        })
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)  
    }
    
    override func viewWillLayoutSubviews() { 
        super.viewWillLayoutSubviews()
        
        if UserDefaults.standard.object(forKey: "initialOpened") == nil //!initialOpened
        {
            UserDefaults.standard.set(true, forKey: "initialOpened")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier :"parallax") as! ParallaxController 
            vc.modalTransitionStyle = .crossDissolve
            vc.view.frame = self.view.frame
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: false, completion: {
                
            })
        }
        if !initialOpened {
            DataManager.℠.showInitialLoading(view: view)
            initialOpened = true
        }
    } 
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated) 
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) { 
            self.view.alpha = 1
        }
        if self.congrats != nil {
            showCongrats(text: self.congrats!.0, points: self.congrats!.1)
        }
        
        if DataManager.℠.currentRating > 1000, nil != UserDefaults.standard.object(forKey: "rated") {
            UserDefaults.standard.set(true, forKey: "rated")
            let al = UIAlertController.init(title: "Please review our app", message: "Your feedback is very important for us", preferredStyle: .alert)
            al.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (all) in
                UIApplication.shared.open(URL(string : "https://itunes.apple.com/us/app/coinswipe-ico-rating-platform/id1391964407")!, options: [:], completionHandler: { (status) in
                    
                })
            }))
            al.addAction(UIAlertAction.init(title: "Later", style: .cancel, handler: { (all) in
                
            }))
            self.show(al, sender: nil)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "containerSegue" {
            self.stacksVC = segue.destination as? StacksViewController
        }
    }
    
    @objc func scrolledToIndex(noti:NSNotification) {  
        if AppManager.℠.currentMode == .browse {
            let index = noti.object as! Int
            self.sliderCounter.text = "\(index + 1)/\(DataManager.℠.currentStacks.count)"
            self.slider.setValue(Float(index), animated: true)
            updateFavourites()
        } else if AppManager.℠.currentMode == .oneCard {
            self.changeName(withIndex: noti)
        }
        glorifyAspects()
        DataManager.℠.nextStepTutorial(toStart: 0) 
    }
    
    func updateFavourites() {
        if let ico = DataManager.℠.currentICO, DataManager.℠.favourites.contains(ico) {
            favouriteButton.setImage(UIImage(named: "heartFilled"), for: UIControl.State.normal)
        }
        else {
            favouriteButton.setImage(UIImage(named: "heart"), for: UIControl.State.normal)
        }
        colourIcons()
    }
    
    func glorifyAspects() {
        DataManager.℠.performRealm {
            if let ico = DataManager.℠.currentICO {
                ico.aspects = Dictionary()
                
                if let dict = UserDefaults.standard.object(forKey: "save" + ico.symbol!) as? Dictionary<String, Int> {
                   ico.aspects = dict
                }
//            self.colourImage()
            }
        }
    }
    
    @objc func dimDetails(noti:NSNotification) {
        if let dim = noti.object as? Int {
            switch dim {
            case 0:
                UIView.animate(withDuration: 0.1, animations: { 
                    self.oneCardControls.alpha = 1
                })
            case 1:
                UIView.animate(withDuration: 0.1, animations: {
                    self.oneCardControls.alpha = 0
                })
            default:
                self.oneCardControls.alpha = 0
            }
        }
    }
    
    @IBAction func menuOpened(_ sender: Any) {
        slideshow(play: false)     
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.closeFakeViews), object:nil)
        if AppManager.℠.currentMode == .oneCard { 
            eject(UIButton())
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reloadMode), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reloadStacks), object: nil)
        }
    }
    
    @objc func fillSuggestions() {
        
        var suggestionList = Array<String>()
        for ico in DataManager.℠.dataBase {
            if let symbol = ico.symbol {
                suggestionList.append(symbol)
            }
            if let name = ico.name {
                suggestionList.append(name)
            }
        }
        for ico in DataManager.℠.upcomingICO { 
            if let symbol = ico.symbol {
                suggestionList.append(symbol)
            }
            if let name = ico.name {
                suggestionList.append(name)
            }
        }
        DispatchQueue.main.async { 
            self.searchBar.setDatas(datas: suggestionList)
        }
    }
    
    @objc func tweakUI(noti:NSNotification?) {
        updateFavourites()
        glorifyAspects()
        
        fillSuggestions()
        
        if AppManager.℠.currentMode == .browse {
//            self.name.textAlignment = .left
//            self.name.text = "Browse"
            self.slider.minimumValue = 0
            self.slider.maximumValue = Float(DataManager.℠.currentStacks.count) - 1
            self.sliderCounter.text = "1/\(DataManager.℠.currentStacks.count)"
            if DataManager.℠.currentICO != nil {
                if let index = DataManager.℠.currentStacks.index(of: DataManager.℠.currentICO!) {
                    self.slider.value = Float(index)
                    self.sliderCounter.text = "\(index + 1)/\(DataManager.℠.currentStacks.count)"
                }
            } else {
                self.slider.value = 0
                self.sliderCounter.text = "1/\(DataManager.℠.currentStacks.count)"
            }
            UIView.animate(withDuration: 0.5, animations: {
                self.slideControls.alpha = 1
                self.sliderView.alpha = 1
                self.sliderCounter.alpha = 1 
                self.progressBar.alpha = 0
                self.searchBar.alpha = 0
                self.searchButton.alpha = 1
                self.oneCardControls.alpha = 0
                self.exitButtonWidth.constant = 0
                self.toExitButtonWidth.constant = 0
            })
            return
        }
//        UIView.animate(withDuration: 0.5, animations: {
//            self.slideControls.alpha = 1
//        })
        if AppManager.℠.currentMode == .oneCard {
//            self.name.textAlignment = .center
            //            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.changeTitleIndex), object: 0)
            UIView.animate(withDuration: 0.5, animations: {
                self.slideControls.alpha = 0
                self.sliderView.alpha = 0
                self.sliderCounter.alpha = 0
                self.progressBar.alpha = 1
                
                self.searchBar.alpha = 0
                self.modeSwitch.alpha = 0
                self.searchButton.alpha = 0
//                self.oneCardControls.alpha = 1
                self.exitButtonWidth.constant = self.savedExitButtonWidth
                self.toExitButtonWidth.constant = 8
            })
//            self.name.text = "\(Aspect.names[0])"
        }
        else if AppManager.℠.currentMode == .favourites {
            UIView.animate(withDuration: 0.2, animations: { 
                self.slideControls.alpha = 0
                self.sliderView.alpha = 0
                self.sliderCounter.alpha = 0
                self.progressBar.alpha = 0
                
                self.searchBar.alpha = 0
                self.modeSwitch.alpha = 0 
                self.searchButton.alpha = 0
                self.oneCardControls.alpha = 0
                self.exitButtonWidth.constant = 0
                self.toExitButtonWidth.constant = 0
            })
//            self.name.text = noti!.object as? String
        }
    }
    
    func createAspect(index:Int, quality:Int) -> Aspect.quality {
        switch quality {
        case -1:
            return Aspect.quality.bad(index)
        case 1:
            return Aspect.quality.trust(index)
        case 0:
            return Aspect.quality.none(index)
        default:
            return Aspect.quality.none(index)
        }
    }
    
    @objc func paintIt(noti:NSNotification) {
        if let index = noti.object as? Int {
            self.colourImage(index)
        }
    }
    
    func colourImage(_ index:Int) {
        if let ico = DataManager.℠.currentICO {  
            if self.progressBar.numberOfPoints > index + 1 {
                self.progressBar.currentIndex = index + 1
            }
        }
    }
    
    @IBAction func sliderMoved(_ sender: UISlider) {
        let index = lroundf(sender.value)
        self.sliderCounter.text = "\(index)/\(DataManager.℠.currentStacks.count)"
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.scrollToStack), object: index) 
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.scrollToItem), object: index)
    }
    
    @IBAction func changeSliderCounter(_ sender: UISlider) {
        let index = lroundf(sender.value)
        self.sliderCounter.text = "\(index + 1)/\(DataManager.℠.currentStacks.count)"
    }
    
    func changeName(withIndex noti:NSNotification) {
//        let index = noti.object as! Int
//        if index < Aspect.names.count {
//            self.name.text = "\(Aspect.names[index])"
//        }
    }
    
    @IBAction func addFavourite(_ sender: UIButton)  {
        let dm = DataManager.℠
        if dm.favourites.contains(dm.currentICO!) {
            favouriteButton.setImage(UIImage(named: "heart"), for: UIControl.State.normal)
            dm.favourites.remove(at: dm.favourites.index(of: dm.currentICO!)!)
            colourIcons() 
            if let sym = dm.currentICO?.symbol {
                if let ind = dm.favsSymbols.index(of: sym) {
                    dm.favsSymbols.remove(at: ind)
                }
            }
            UserDefaults.standard.set(dm.favsSymbols, forKey: "favourites")
            return
        }
        dm.favourites.append(dm.currentICO!)
        if let sym = dm.currentICO?.symbol {
            dm.favsSymbols.append(sym)
            UserDefaults.standard.set(dm.favsSymbols, forKey: "favourites")
        }
        favouriteButton.setImage(UIImage(named: "heartFilled"), for: UIControl.State.normal)
        colourIcons()
        var config = SwiftMessages.Config()
        config.presentationStyle = .bottom
        config.presentationContext = .window(windowLevel: UIWindow.Level.alert)
        config.duration = .seconds(seconds: 1.5)
        config.dimMode = .gray(interactive: true)
        config.interactiveHide = false
        config.preferredStatusBarStyle = .lightContent
        config.eventListeners.append() { event in
            if case .didHide = event { print("yep") }
        }
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureTheme(.success)
        view.configureDropShadow()
        let iconText = "😎"
        view.configureContent(title: "Awesome!", body: "The offering was added to favorites.", iconImage: nil, iconText: iconText, buttonImage: nil, buttonTitle: "OK") { (button) in
            SwiftMessages.hide()
        }
        SwiftMessages.show(config: config, view: view)
    }
    
    @IBAction func playSlide(_ sender: UIButton) {
        slideshow(play: SlideshowControls.slidePlayTag.play.rawValue == sender.tag) 
    }
    
    func slideshow(play:Bool) {
        var image: UIImage?
        var tag = SlideshowControls.slidePlayTag.play
        if play {
            image = UIImage(named: "stop")
            tag = SlideshowControls.slidePlayTag.stop
            self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: (#selector(self.nextSlide(_:))), userInfo: nil, repeats: true)
        } else {
            image = UIImage(named: "play")
            tag = SlideshowControls.slidePlayTag.play
            self.timer?.invalidate()
        }
        playButton.tag = tag.rawValue
        playButton.setImage(image, for: UIControl.State.normal)
        colourIcons()
    }
    
    @IBAction func backSlide(_ sender: Any) {
        let currentIndex = DataManager.℠.currentStacks.index(of:DataManager.℠.currentICO!)!
        if currentIndex > 0 {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.scrollToItem), object: currentIndex - 1)
        }
    }
    
    @IBAction func nextSlide(_ sender: Any) {
        let currentIndex = DataManager.℠.currentStacks.index(of:DataManager.℠.currentICO!)!
        if currentIndex < DataManager.℠.currentStacks.count - 1 {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.scrollToItem), object: currentIndex + 1)
        } else {
            slideshow(play: false)
            playButton.setImage(UIImage(named: "play"), for: UIControl.State.normal)
            colourIcons() 
        }
    }
    
    @IBAction func searchPressed(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.closeFakeViews), object:nil)
        let textFieldInsideSearchBar = self.searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.lightText
        textFieldInsideSearchBar?.font = UIFont.init(name: "Futura", size: 14)
        self.searchBar.searchLabel_textColor = UIColor.darkText 
        self.searchBar.searchLabel_font = UIFont.init(name: "Futura", size: 14)
        self.searchBar.text = "Enter a name or ticker"
        UIView.animate(withDuration: 0.2, animations: {
            if self.searchBar.alpha == 0 {
                self.searchBar.alpha = 1
                self.modeSwitch.alpha = 0
                self.menu.alpha = 0
                self.searchButton.alpha = 0
//                self.name.alpha = 0
                self.addDimView()
            }
        })
    }
    
    func addDimView() {
        let view = UIView.init(frame: self.view.frame) 
        view.backgroundColor = UIColor.black
        view.center = self.view.center
        view.alpha = 0
        view.tag = 123
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.dismissTap))
        view.addGestureRecognizer(tap)
        self.view.insertSubview(view, belowSubview: self.searchBar)
        UIView.animate(withDuration: 0.3) {
            view.alpha = 0.4
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        dismissTap()
    }
    
    func onClickShadowView(shadowView:UIView) { 
        dismissTap()
    }
    
    @objc func dismissTap() {
        for sub in self.view.subviews {
            if sub.tag == 123 {
                sub.removeFromSuperview()
            }
        }
        searchBar.resignFirstResponder()
        UIView.animate(withDuration: 0.2, animations: {
            self.searchBar.alpha = 0
//            self.name.alpha = 1
            self.modeSwitch.alpha = 0
            self.menu.alpha = 1
            self.searchButton.alpha = 1
        })
    }
    
    func onClickItemSuggestionsView(item: String) {
        var ready:ICO?
        for ico in DataManager.℠.dataBase {
            if ico.symbol == item || ico.name == item {
                ready = ico
            }
        }
        if ready == nil {
            for ico in DataManager.℠.upcomingICO {
                if ico.symbol == item || ico.name == item {
                    ready = ico
                }
            }
        }
        self.dismissTap()
        self.searchBar.searchBarCancelButtonClicked(self.searchBar)
        moveTo(symbol: ready!.symbol!)
    }
    
    @IBAction func eject(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.switchScroll), object: nil)
    }
    
    @IBAction func showDetails(_ sender: UIButton) {
        if let images = DataManager.℠.oneCardTop?.images {
            if images.count > 0 {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.showSlides), object: DataManager.℠.oneCardTop?.images)
            }
        }
    }
    
    @objc func reportLink(_ noti: NSNotification) {
        var text = ""
        var parameterToChange = UpdateType.profile
        var checkNum = 0
        let name =  "\(DataManager.℠.currentICO!.name!) (\(DataManager.℠.currentICO!.symbol ?? ""))"
        
        var currentCard:Card?
        var cardText:String? = ""
        if AppManager.℠.currentMode == .oneCard {
            if let top = DataManager.℠.oneCardTop {
                if DataManager.℠.currentICO!.cards.count > top.cardIndex ?? 0 {
                    let currentCard = DataManager.℠.currentICO!.cards[top.cardIndex!]    
                    cardText = CardTypeText[currentCard.type.rawValue]
                }
            }
        }
        var showBounty = false
        let ico = DataManager.℠.currentICO
        if let num = noti.object as? Int {
            checkNum = num
            switch num {
            case 111: 
                text = "\(name) icon"
                parameterToChange = .icon
            case 333:
                text = "\(name) cover"
                parameterToChange = .cover
            case 444:
                if ico is UpcomingICO {
                    text = "\(name) info about the release"
                    parameterToChange = .rich
                } else {
                    text = " \(name) link for initial price data"
                    parameterToChange = .initialPrice
                }
            case 5556:
                text = "\(name) ICO news" 
                parameterToChange = .rich
            case UpdateType.pins.rawValue:
                text = "\(name) tags"
                parameterToChange = .pins
                showBounty = !checkExistence(param:ico?.categories)
            case UpdateType.industry.rawValue:
                text = "\(name) link for industry info"
                parameterToChange = .industry
                showBounty = !checkExistence(param:ico?.industry)
            case UpdateType.story.rawValue:
                text = "\(name) story/tagline"
                parameterToChange = .story
                showBounty = !checkExistence(param:ico?.story)
            case UpdateType.naics.rawValue:
                text = "\(name) NAICS"
                parameterToChange = .naics
                showBounty = !checkExistence(param:ico?.naics)
                
            case UpdateType.cardStory.rawValue:
                text = "\(cardText ?? "") story/tagline for \(name)"
                parameterToChange = .cardStory
                showBounty = !checkExistence(param:currentCard?.story)
            case UpdateType.cardTags.rawValue:
                text = "\(cardText ?? "") tags for \(name)"
                parameterToChange = .cardTags
                showBounty = !checkExistence(param:currentCard?.tags)
            case UpdateType.cardPros.rawValue:
                text = "\(cardText ?? "") pros for \(name)"
                parameterToChange = .cardPros
                showBounty = !checkExistence(param:currentCard?.pros)
            case UpdateType.cardCons.rawValue:
                text = "\(cardText ?? "") cons for \(name)"
                parameterToChange = .cardCons
                showBounty = !checkExistence(param:currentCard?.cons)
            case UpdateType.news.rawValue:
                text = "\(name) News"
                parameterToChange = UpdateType(rawValue:num) ?? .profile 
            default:
                text = "\(name) news link" //text = "\(name) \(Aspect.names[num]) info"
                parameterToChange = UpdateType(rawValue:num) ?? .profile
            }
        } 
        NetworkManager.℠.parameterToChange = parameterToChange
        
        if checkNum == 444 || checkNum == 333 || checkNum == 111 || checkNum == 5556 {
            self.showBounty(text:text, param: parameterToChange, header: "")
            return
        }
        
        if DataManager.℠.bountyOnScreen || showBounty {
            if nil != UserDefaults.standard.object(forKey: "loginned") as? String { 
                self.showBounty(text:text, param: parameterToChange, header: "")
            } else {
                let ratingVC = SimplePopup(nibName: "SimplePopup", bundle: nil)
                let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
                ratingVC.titleLabel.text = "Login is required to participate"
                ratingVC.textView.text = "Please select an option"
                let buttonTwo = DefaultButton(title: "Facebook", dismissOnTap: true) {
                    AppManager.℠.loginFacebook(vc: AppManager.℠.mainVC!)
                }
                let button4 = DefaultButton(title: "Email", dismissOnTap: true) {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.showEmailLogin), object:nil)
                }
                ratingVC.textHeight.constant = 45
                let cancel = CancelButton(title: "Later", dismissOnTap: true) {
                    
                }
                popup.addButtons([buttonTwo, button4, cancel])
                AppManager.℠.mainVC!.present(popup, animated: true, completion: nil)
            }
            return
        } 
        
        let ratingVC = EditUpdateView(nibName: "EditUpdateView", bundle: nil) 
        // Prepare the popup assets
        let title = "Edit or delete info for \(name)"
        let message = "If the community validates and agrees you will be awarded a minimum of 100 points. \n\nIf the community invalidates you will lose a minimum of 1,000 points" 
        
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
        ratingVC.name.text = title
        ratingVC.caption.text = message
        
        let buttonE = DefaultButton(title: "EDIT") {
            self.showAdd(text:text, param: parameterToChange, header: "") 
        }
        let buttonTwo = DefaultButton(title: "DELETE") {
            self.showDelete(text:text, param: parameterToChange)
        }
        let buttonThree = CancelButton(title: "CANCEL", dismissOnTap: true) {
        }
        popup.addButtons([buttonE, buttonTwo, buttonThree])
        self.present(popup, animated: true, completion: nil)
    }
    
    func checkExistence(param:String?) -> Bool {
        return (nil != param && param! != "")
    }
    
    func showBounty(text:String, param parameterToChange:UpdateType, header:String) {
        UIPasteboard.general.string = ""
        
        let ratingVC = EditUpdateView(nibName: "EditUpdateView", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
        copyClipboard = ratingVC
        ratingVC.name.text = "Bounty hunt"
        ratingVC.caption.text = "Suggest " + text + " to get the reward.\n\nType in box below or paste a link \n\nIf the community validates and agrees you will be awarded a minimum of 100 points. \nIf the community invalidates you will lose a minimum of 1,000 points"
        
        // Create first button
        let buttonOne = CancelButton(title: "CANCEL", height: 50) {
            
        }
        
        // Create second button
        let buttonTwo = DefaultButton(title: "SEND", height: 60) {
            if ratingVC.commentTextField.text != nil, ratingVC.commentTextField.text != "" {
                if let toSend = ratingVC.commentTextField.text {
                    let pars = ["updateType" : UpdateType.rich.rawValue, "actionType" : UpdateActionType.add.rawValue, "value" : toSend] as [String : Any]
                    NetworkManager.℠.updateLink(ico: DataManager.℠.currentICO!.symbol!, pars: pars) { (data) in 
                        
                    }
                    self.copyClipboard = nil
                    popup.dismiss({
                    })
                    self.showMessage("The link has been sent for the review", style: .success) 
                }
            }
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonTwo, buttonOne])
        
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    
    func showAdd(text:String, param parameterToChange:UpdateType, header:String) {
        UIPasteboard.general.string = ""
        
        let ratingVC = EditUpdateView(nibName: "EditUpdateView", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
        copyClipboard = ratingVC
        ratingVC.name.text = "Apply for a reward"
        ratingVC.caption.text = "Suggest your link for " + text + " to get the reward. Type in box below or paste a link"
        
        // Create first button
        let buttonOne = CancelButton(title: "CANCEL", height: 50) {
        }
        
        // Create second button
        let buttonTwo = DefaultButton(title: "SEND", height: 60) {
            if ratingVC.commentTextField.text != nil, ratingVC.commentTextField.text != "" {
                if let toSend = ratingVC.commentTextField.text {
                    let pars = ["updateType" : parameterToChange.rawValue, "actionType" : UpdateActionType.edit.rawValue, "value" : toSend] as [String : Any]
                    NetworkManager.℠.updateLink(ico: DataManager.℠.currentICO!.symbol!, pars: pars) { (data) in
                        
                    }
                    self.copyClipboard = nil
                    popup.dismiss({
                        self.showMessage("The link has been sent for the review", style: .success)
                    })
                }
            }
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonTwo, buttonOne])
        
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    
    func showDelete(text:String, param parameterToChange:UpdateType) {
        let name = DataManager.℠.currentICO!.name!
        let sym = DataManager.℠.currentICO!.symbol ?? ""
        let title = "Delete bad \(name)(\(sym)) info"
        let message = "Please select a reason. \n\nIf the community validates and agrees you will be awarded a minimum of 100 points. \n\nIf the community invalidates you will lose a minimum of 1,000 points"
        
        let popup = PopupDialog(title: title, message: message, image: nil)
        let buttonOne = DefaultButton(title: "Offensive content") {
            NetworkManager.℠.report(ico: name, text: text + "Offensive content") { (data) in
                let pars = ["updateType" : parameterToChange.rawValue, "actionType" : UpdateActionType.delete.rawValue, "value" : text] as [String : Any]
                NetworkManager.℠.updateLink(ico: DataManager.℠.currentICO!.symbol!, pars: pars) { (data) in
                    
                }
                self.copyClipboard = nil
                popup.dismiss({
                    self.showMessage("The link has been sent for the review", style: .success)
                })
                self.showMessage("Thank you, the report has been sent", style: .success)
            }
        }
        let buttonTwo = DefaultButton(title: "Wholly inaccurate") {
            NetworkManager.℠.report(ico: name, text: text + "Wholly inaccurate") { (data) in
                let pars = ["updateType" : parameterToChange.rawValue, "actionType" : UpdateActionType.delete.rawValue, "value" : text] as [String : Any]
                NetworkManager.℠.updateLink(ico: DataManager.℠.currentICO!.symbol!, pars: pars) { (data) in
                    
                }
                self.copyClipboard = nil
                popup.dismiss({
                    self.showMessage("The link has been sent for the review", style: .success)
                })
                self.showMessage("Thank you, the report has been sent", style: .success)
            }
        }
        let buttonThree = CancelButton(title: "CANCEL", dismissOnTap: true) {
        }
        popup.addButtons([buttonOne, buttonTwo, buttonThree])
        self.present(popup, animated: true, completion: nil)
    }

    
    func showMessage(_ text:String, style:Theme) {
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.presentationContext = .window(windowLevel: UIWindow.Level.alert)
        config.duration = .seconds(seconds: 1.5)
        config.dimMode = .gray(interactive: true)
        config.interactiveHide = false
        config.preferredStatusBarStyle = .lightContent
        config.eventListeners.append() { event in
            if case .didHide = event { print("yep") }
        }
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureTheme(style)
        view.configureDropShadow()
        let iconText = style == .success ? "🙌" : ""
        view.configureContent(title: style == .success ? "Thank you!" : "Warning!", body: text, iconImage: nil, iconText: iconText, buttonImage: nil, buttonTitle: "OK") { (button) in
            SwiftMessages.hide()
        }
        SwiftMessages.show(config: config, view: view)
    }
    
    @objc func pulsePoints(_ noti:NSNotification) { 
        UIView.animate(withDuration: 0.2, delay: 0, options: .allowAnimatedContent, animations:   {
            self.starView.transform = CGAffineTransform.init(scaleX: 1.2, y: 1.2)
        }, completion: { (finished) in
            UIView.animate(withDuration: 0.2, delay: 0, options: .allowAnimatedContent, animations:   {
                self.starView.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            }, completion: { (finished) in
                let bag = noti.object as! PointsBag
                DataManager.℠.currentRating += bag.points
                AppManager.℠.postBranchTry()
                self.starLabel.text = "\(DataManager.℠.currentRating)"
                UserDefaults.standard.set(DataManager.℠.currentRating, forKey: "rating")
                DataManager.℠.loginEnoughPoints += 1
                if bag.type == 1 { 
                    self.checkLogin()
                }
                NetworkManager.℠.updatePoints(card: bag.card, type: bag.type, ticker: bag.symbol)
                UIView.animate(withDuration: 0.2, delay: 0, options: .allowAnimatedContent, animations:   {
                    self.starView.transform = CGAffineTransform.init(scaleX: 1.2, y: 1.2)
                }, completion: { (finished) in
                    UIView.animate(withDuration: 0.2, delay: 0, options: .allowAnimatedContent, animations:   {
                        self.starView.transform = CGAffineTransform.init(scaleX: 1, y: 1)
                    }, completion: { (finished) in })
                })
            })
        })
    }
    
    @IBAction func loginSuggest(_ sender: Any) {
        
        let ratingVC = ProfileView(nibName: "ProfileView", bundle: nil)
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .vertical, transitionStyle: .bounceDown, tapGestureDismissal: true)
        let buttonThree = CancelButton(title: "Close", dismissOnTap: true) { }
        popup.addButtons([buttonThree]) 
        self.present(popup, animated: true, completion: nil)
        
        
//        if NetworkManager.℠.historyToKeep.count > 0 {
//            let ratingVC = ProfileView(nibName: "ProfileView", bundle: nil)
//            ratingVC.mode = .history
//
//            // Create the dialog
//            let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
//            //        ratingVC.currentIco =
//            //            ratingVC.titleLabel.text = "Review your rating"
//            //            ratingVC.caption.text = "Change the overall rating of the ICO based on your impression. \n\nSuggest the short tagline, that represents qualities of this ICO on your opinion and receive additional points after the approval"
//
//            // Create first button
//            let buttonOne = CancelButton(title: "CLOSE", height: 50) {
//
//            }
//
//            // Add buttons to dialog
//            popup.addButtons([buttonOne])
//
//            // Present dialog
//            AppManager.℠.mainVC!.present(popup, animated: true, completion: nil) 
//            return
//        }
//        let title = "What can I do with the points?"
//        let message = "The plan is for Coinswipe points to be exchanged periodically for tokens designed to \"unlock\" based on the usefulness of your ratings to the community.\n\nDuring the beta the system is generous with points due to the need to test features and onboard teams (referrals).\n\nFriends you refer that install and earn points will earn additional points for the Coinswiper that refers them."
//        let popup = PopupDialog(title: title, message: message, image: nil)
//        let buttonTwo = DefaultButton(title: "OK", dismissOnTap: true) {
//
//        }
//        popup.addButtons([buttonTwo])
//        self.present(popup, animated: true, completion: nil)
        //        Intercom.mess
        //        Intercom.presentMessageComposer(withInitialMessage: "Message about how points work")
        //        AppManager.℠.loginFacebook(vc: self)
    }
    
    @IBOutlet weak var backgroundImage: UIImageView!
    var lipstickOn = false
    
    @objc func changeBack(noti:Notification) {
        var image = UIImage.init(named: "main-bg")
        if let fake = noti.object as? Bool {
            if fake {
                image = UIImage.init(named: "lipstick")
                lipstickOn = true
            } else {
                if !lipstickOn {
                    return
                }
                lipstickOn = false
            }
        }
        UIView.transition(with: self.backgroundImage,
                          duration: 0.75,
                          options: .transitionCrossDissolve,
                          animations: { self.backgroundImage.image = image },
                          completion: nil) 
    }
    
    func showCongrats(text:String, points:PointsBag) {
        var config = SwiftMessages.Config()
        config.presentationStyle = .center
        config.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        config.duration = .forever
        config.dimMode = .gray(interactive: true)
        config.interactiveHide = false
        config.preferredStatusBarStyle = .lightContent
        config.eventListeners.append() { event in
            if case .didHide = event { print("yep") }
        }
        let view = MessageView.viewFromNib(layout: .messageView)
        view.configureTheme(.success)
        view.configureDropShadow()
        let iconText = "😻"
        
        view.configureContent(title: "Congratulations!", body: text, iconImage: nil, iconText: iconText, buttonImage: nil, buttonTitle: "OK") { (button) in
            SwiftMessages.hide()
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.pulsePoints), object: points)
        SwiftMessages.show(config: config, view: view)
        self.congrats = nil
    }
    
    
    
    func checkLogin() {
//        if DataManager.℠.loginEnoughPoints > 2, UserDefaults.standard.object(forKey: "loginned") == nil, UserDefaults.standard.object(forKey: "loginnedOnce") == nil {
//            UserDefaults.standard.set(true, forKey: "loginnedOnce")
//            
//            let title = "Save your points!"
//            let message = "Don't forget to login - your score will be synchronized with the cloud"
//            let popup = PopupDialog(title: title, message: message, image: nil)
//            let buttonTwo = DefaultButton(title: "OK", dismissOnTap: true) {
//                self.showLogin()
//            }
//            let buttonThree = CancelButton(title: "Remind me later", height: 60) {
//                
//            }
//            popup.addButtons([buttonTwo, buttonThree]) 
//            self.present(popup, animated: true, completion: nil) 
//        }
    }
    
    @objc func showLogin() {
        let alert = UIAlertController.init(title: "Login options", message: "Please select the login option to keep your points saved online", preferredStyle: .actionSheet)
        let fb = UIAlertAction.init(title: "Facebook", style: .default, handler: { (action) in
            AppManager.℠.loginFacebook(vc: self)
        })
        let email = UIAlertAction.init(title: "Email", style: .default, handler: { (action) in
            self.continueLogin()
        })
        let cancel = UIAlertAction.init(title: "Remind me later", style: .cancel, handler: { (action) in
            
        }) 
        alert.addAction(fb)
        alert.addAction(email)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: { 
            
        })
    }
    
    @objc func continueLogin() {
        self.dismiss(animated: true) {
            let loginController = LFLoginController()
            loginController.logo = UIImage.init(named: "logo")
            loginController.backgroundColor = kBlueColor
            loginController.delegate = self
            self.present(loginController, animated: true) {
                
            }
        }
    }
    
    func loginDidFinish(email: String, password: String, type: LFLoginController.SendType) { 
        UserDefaults.standard.set(email, forKey: "loginned")
        AppManager.℠.loginIntercom()
        switch type {
        case .Login:
            NetworkManager.℠.login(name: email, pass: password)
        case .Signup:
            NetworkManager.℠.login(name: email, pass: password)
        }
        self.dismiss(animated: true) {
            
        }
    }
    
    func forgotPasswordTapped(email: String) {
        
    }
    
    @objc func openLink(noti:NSNotification) {
        if let link = noti.object as? String {
            let svc = SFSafariViewController(url: URL(string:link)!)
            present(svc, animated: true, completion: nil)
        }
    }
    
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar, 
                     didSelectItemAtIndex index: Int) {
        print("Index selected!")
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     willSelectItemAtIndex index: Int) {
        print("Index selected!")
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     canSelectItemAtIndex index: Int) -> Bool {
        return true
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     textAtIndex index: Int, position: FlexibleSteppedProgressBarTextLocation) -> String {
        if position == FlexibleSteppedProgressBarTextLocation.bottom {
            switch index { 
            default: return ""
                
            }
        }
        return ""
    }
    
    @objc func startShowcase(noti:NSNotification) {
        startShowcase(num: noti.object as! Int)
    }
    
    var currentShowcaseIndex = 0
    
    func startShowcase(num:Int) {
        let showcase = MaterialShowcase()
        showcase.delegate = self
        
//        showcase.aniRippleAlpha = 0
        showcase.targetHolderColor = UIColor.clear
        showcase.aniComeInDuration = 0.5
        showcase.targetHolderRadius = 44
        showcase.backgroundViewType = .circle
        switch num {
        case 0:
            showcase.setTargetView(view: DataManager.℠.oneCardBaseTop!.botImage)
            showcase.primaryText = "Let's start with EOS"
            showcase.isTapRecognizerForTargetView = true
            //            showcase.backgroundViewType = .full
            showcase.targetHolderRadius = 80
            showcase.secondaryText = "Click on the card to see the news"
        case 1:
            showcase.setTargetView(view: DataManager.℠.oneCardBaseTop!)
            showcase.primaryText = "Great!"
            //            showcase.backgroundViewType = .full
            //            showcase.isTapRecognizerForTagretView = true
            showcase.targetHolderRadius = 180
            showcase.secondaryText = "Swipe right or left to rate the current article for this project, swipe right for good, left for bad"
        case 2:
            showcase.setTargetView(view: DataManager.℠.oneCardBaseTop!)
            showcase.primaryText = "Swipe down to mark an article as fake news"
            showcase.backgroundViewType = .full
            showcase.isTapRecognizerForTargetView = true
            showcase.targetHolderRadius = 180
            showcase.secondaryText = "Send us the reason and get the points!"
        case 3:
            showcase.setTargetView(view: DataManager.℠.oneCardBaseTop!)
            showcase.primaryText = "Select the options in this dialog flow to submit"
            showcase.backgroundViewType = .full
            showcase.isTapRecognizerForTargetView = true
            showcase.targetHolderRadius = 140
            showcase.secondaryText = "Include the link with the details if it helps"
        case 4:
            showcase.setTargetView(view: DataManager.℠.oneCardBaseTop!) 
            showcase.primaryText = "Seems that you are ready 🙌"
            showcase.isTapRecognizerForTargetView = true
            //            showcase.backgroundViewType = .full
            showcase.targetHolderRadius = 180
            showcase.secondaryText = "Find a coin you like or know and start exploring!"
//        case 4:
//            showcase.setTargetView(view: DataManager.℠.oneCardTop!.expandButton!)
//            showcase.primaryText = "Need to know more before you rate?"
//            showcase.isTapRecognizerForTagretView = true
//            showcase.secondaryText = "Expand the card to get the story on this aspect of the project"
//        case 5:
//            showcase.setTargetView(view: self.pageBelow)
//            showcase.primaryText = "Awesome!"
////            showcase.isTapRecognizerForTagretView = true
////            showcase.backgroundViewType = .full
////            showcase.targetHolderRadius = 115
//            showcase.secondaryText = "Rotate the box that appears to see Pros, Cons and Tags contributed by the community"
//        case 6:
//            showcase.setTargetView(view: DataManager.℠.oneCardTop!.profileOne)
//            showcase.primaryText = "Great job!"
//            showcase.isTapRecognizerForTagretView = true
////            showcase.targetHolderRadius = 140
//            showcase.secondaryText = "If you need more information to rate this element, click one of the images on the card"
//        case 7:
//            showcase.setTargetView(view: DataManager.℠.oneCardTop!)
//            showcase.primaryText = "Rate the cards by swiping them until you see stars appear."
//            showcase.isTapRecognizerForTagretView = true
//                        showcase.targetHolderRadius = 180
//            showcase.secondaryText = " "
//        case 8:
//            showcase.setTargetView(view: DataManager.℠.oneCardBaseTop!)
//            showcase.primaryText = "Great job! Coinswipe derived a potential star rating based on your swipes"
//            showcase.isTapRecognizerForTagretView = true
////            showcase.backgroundViewType = .full
//            showcase.targetHolderRadius = 10
//            showcase.secondaryText = "You can edit this rating by clicking the stars"
//        case 9:
//            showcase.setTargetView(view: DataManager.℠.oneCardBaseTop!.checkAspectsAll)
//            showcase.primaryText = "Fantastic!"
//            showcase.isTapRecognizerForTagretView = true
//            showcase.secondaryText = "Send proposed updates for any card or other aspect of a coin to win bigger rewards"
////            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 4) {
////                showcase.completeShowcase(animated: false)
////                DataManager.℠.nextStepTutorial(toStart: 10)
////            }
//        case 10:
//            showcase.setTargetView(view: DataManager.℠.oneCardBaseTop!.botImage)
//            showcase.primaryText = "Thanks for sharing this small journey!"
//            showcase.isTapRecognizerForTagretView = true
////            showcase.backgroundViewType = .full
//            showcase.targetHolderRadius = 80
//            showcase.secondaryText = "Your bigger voyage across thousands of coins starts now"
//        case 11:
//            showcase.setTargetView(view: DataManager.℠.oneCardBaseTop!.botImage)
//            showcase.primaryText = "Explore, learn, review and help the crypto community become stronger"
//            showcase.isTapRecognizerForTagretView = true
//            //            showcase.backgroundViewType = .full
//            showcase.targetHolderRadius = 80
//            showcase.secondaryText = "Tap to start!"
 
        default:
            return
        }
        showcase.backgroundPromptColor = kOrangeColor
        showcase.targetTintColor = kOrangeColor
        currentShowcaseIndex = num
        showcase.show {
            
        }
    }
    func showCaseWillDismiss(showcase: MaterialShowcase, didTapTarget: Bool) {
        print("Showcase \(showcase.primaryText) will dismiss.")
    }
    func showCaseDidDismiss(showcase: MaterialShowcase) {
        if self.currentShowcaseIndex == 0 {
            self.switchScroll()
            DataManager.℠.nextStepTutorial(toStart: 1)
        }
        if self.currentShowcaseIndex == 1 {
            
        }
        if self.currentShowcaseIndex == 2 {
            
        }
        if self.currentShowcaseIndex == 3 {
            self.switchScroll()
            DataManager.℠.nextStepTutorial(toStart: 4)
        }
        if self.currentShowcaseIndex == 4 {
            
        }
    }
}

class BackSubView : UIView {
    
}
